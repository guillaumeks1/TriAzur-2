var validation = require('./../../../models/validation/validation');

class section{
    constructor(Name, MaterialId, Area, I2, I3, J, XG, YG, Xext, Yext, Xint, Yint){
        this.Name = String(Name);
        this.MaterialId = MaterialId;
        this.Area = Number(Area);
        this.I2 = Number(I2);
        this.I3 = Number(I3);
        this.J = Number(J);
        this.XG = Number(XG);
        this.YG = Number(YG);
        this.Xext = Xext;
        this.Yext = Yext;
        this.Xint = Xint;
        this.Yint = Yint;
    }

    isvalid(){
        if(validation.isNotEmpty(this.Name) && validation.isNotEmpty(this.MaterialId) 
        && validation.isStrictPositive(this.Area) && validation.isStrictPositive(this.I2) 
        && validation.isStrictPositive(this.I3) && validation.isPositive(this.J)
        && validation.isNumeric(this.XG) && validation.isNumeric(this.YG)){
            return true;
        }else{
            return false;
        }
    }
}

module.exports = section;
