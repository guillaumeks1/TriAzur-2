var validation = require('./../../../models/validation/validation');

class support{
    constructor(Name, DispX, DispY, DispZ, RotX, RotY, RotZ){
        this.Name = String(Name);
        this.DispX = Number(DispX);
        this.DispY = Number(DispY);
        this.DispZ = Number(DispZ);
        this.RotX = Number(RotX);
        this.RotY = Number(RotY);
        this.RotZ = Number(RotZ);
    }

    isvalid(){
        if(validation.isNotEmpty(this.Name)
        && validation.isNumeric(this.DispX) && validation.isNumeric(this.DispY)
        && validation.isNumeric(this.DispZ) && validation.isNumeric(this.RotX)
        && validation.isNumeric(this.RotY) && validation.isNumeric(this.RotZ)){
            return true;
        }else{
            return false;
        }
    }
}

module.exports = support;
