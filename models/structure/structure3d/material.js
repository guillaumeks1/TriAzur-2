var validation = require('./../../../models/validation/validation');

class material{
    constructor(Name, Density, Eyoung, Poisson, Thermal, Gmodulus){
        this.Name = String(Name);
        this.Density = Number(Density);
        this.Eyoung = Number(Eyoung);
        this.Poisson = Number(Poisson);
        this.Thermal = Number(Thermal);
        this.Gmodulus = Number(Gmodulus);
    }

    isvalid(){
        if(validation.isNotEmpty(this.Name) && validation.isStrictPositive(this.Density)
        && validation.isStrictPositive(this.Eyoung) && validation.isStrictPositive(this.Poisson)
        && validation.isStrictPositive(this.Thermal) && validation.isStrictPositive(this.Gmodulus)){
            return true;
        }else{
            return false;
        }
    }
}

module.exports = material;
