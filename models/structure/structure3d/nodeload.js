var validation = require('./../../../models/validation/validation');

class nodeload{
    constructor(Name, NodeId, ForceX, ForceY, ForceZ,MomentX,MomentY,MomentZ){
        this.Name = String(Name);
        this.NodeId = NodeId;
        this.ForceX = Number(ForceX);
        this.ForceY = Number(ForceY);
        this.ForceZ = Number(ForceZ);
        this.MomentX = Number(MomentX);
        this.MomentY = Number(MomentY);
        this.MomentZ = Number(MomentZ);
    }

    isvalid(){
        if(validation.isNotEmpty(this.Name) && validation.isNotEmpty(this.NodeId) 
        && validation.isNumeric(this.ForceX) && validation.isNumeric(this.ForceY)
        && validation.isNumeric(this.ForceZ) && validation.isNumeric(this.MomentX)
        && validation.isNumeric(this.MomentY) && validation.isNumeric(this.MomentZ)){
            return true;
        }else{
            return false;
        }
    }
}

module.exports = nodeload;