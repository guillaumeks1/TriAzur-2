var validation = require('./../../../models/validation/validation');

class member{
    constructor(Name, NodeiId, NodejId, SectionId){
        this.Name = String(Name);
        this.NodeiId = String(NodeiId);
        this.NodejId = String(NodejId);
        this.SectionId = String(SectionId);
    }

    isvalid(){
        if(validation.isNotEmpty(this.Name) && validation.isNotEmpty(this.NodeiId) 
        && validation.isNotEmpty(this.NodejId) && validation.isNotEmpty(this.SectionId)){
            return true;
        }else{
            return false;
        }
    }
}

module.exports = member;