var validation = require('./../../../models/validation/validation');

class node{
    constructor(Name, SupportId, X, Y, Z){
        this.Name = String(Name);
        this.SupportId = SupportId;
        this.X = Number(X);
        this.Y = Number(Y);
        this.Z = Number(Z);
    }

    isvalid(){
        if(validation.isNotEmpty(this.Name) && validation.isNotEmpty(this.SupportId)
        && validation.isNumeric(this.X) && validation.isNumeric(this.Y)
        && validation.isNumeric(this.Z)){
            return true;
        }else{
            return false;
        }
    }
}

module.exports = node;
