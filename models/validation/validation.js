var validation = {
    isNotEmpty: function(Input){
        if(Input === undefined || Input === '' || Input === null){
            return false;
        }else{
            return true;
        }

        // if(Input=='' || Input==null){
        //     return false;
        // }else{
        //     return true;
        // }
    },

    isNumeric: function(Input){
        if(!isNaN(Input) && isFinite(Input) && Input!==''){
            if(typeof(Input)=='number'){
                return true;
            }else{
                return false;
            }
            return true;
        }else{
            return false;
        }
    },

    isPositive:function(Input){
        if(validation.isNumeric(Input)){
            if(Input >= 0){
                return true
            }else{
                return false;
            }
        }else{
            return false;
        }
    },

    isStrictPositive:function(Input){
        if(validation.isNumeric(Input)){
            if(Input > 0){
                return true
            }else{
                return false;
            }
        }else{
            return false;
        }
    },

    isArrayNaN:function(Input){
        check = false;
        for(var i = 0; i < Input.length; i++){
            if(isNaN(Input[i])){
                check = true;
            }
        }
        return check;
    }
};



module.exports = validation;