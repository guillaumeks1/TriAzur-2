// var Xext = [0, 3, 3, 0, 0], Yext = [0, 0, 2, 2, 0], Xint = [0, 0, 0, 0, 0], Yint = [0, 0, 0, 0, 0];

function sectionanalysis(Xext, Yext, Xint, Yint){
    var Xextmin = 0, Xextmax = 0, Yextmin = 0, Yextmax = 0,
    Xintmin = 0, Xintmax = 0, Yintmin = 0, Yintmax = 0;
    
    var nbpc = 0;
    var nbpe = 0;
   
    Xextmax = Math.max(...Xext);
    Xextmin = Math.min(...Xext);
    Yextmax = Math.max(...Yext);
    Yextmin = Math.min(...Yext);
    Xintmax = Math.max(...Xint);
    Xintmin = Math.min(...Xint);
    Yintmax = Math.max(...Yint);
    Yintmin = Math.min(...Yint);

    nbpc = Xext.length;
    nbpe = Xint.length;

    var Bext = Xextmax - Xextmin;
    var Hext = Yextmax - Yextmin;
    var Bint = Xintmax - Xintmin;
    var Hint = Yintmax - Yintmin;

    var Xkext =  [];
    var Xkint =  [];
    var Xk =  [];
    var Yk =  [];
    var Xkextmin =  [];
    var Xkextmax =  [];

    var Xbet =  [];
    var Ybet =  [];
    var bbet =  [];

    var sbet =  [];
    var ixxb =  [];
    var iyyb =  [];
    var ixyb =  [];
    var sxbe =  [];
    var sybe =  [];

    var S =  [];

    var nxbe = 0;
    var nybe = 0;
    var nbbe = 0;
    var nbel = 0;

    if (nbpc < 3 || nbpe == 1 || nbpe == 2){
        for (var i = 0; i < nbpe; i++){
            S[i] = -1;
        }
    }
           
    var deltaY = Hext / 100;
    var deltaX = Bext / 100;

    var larg = 0;
    var lc = 0;
    var le = 0;
    var kc = 0;
    var ke = 0;
    var rang = 0;
    var nega = 0;
    var poin = 0;
    var surf = 0;
    var ixx = 0;
    var iyy = 0;
    var ixy = 0;
    var surx = 0;
    var sury = 0;
    var xgra = 0;
    var ygra = 0;
    var brou;
    var dlar;
    
    var i = 0, j=0;
    do{
        Yk[j] = Yextmin + deltaY * (j + 0.5);
  
        kc = 0;
        ke = 0;

        if (nbpc > 2){
            i = 0;
            do{
                if (Yext[i] <= Yk[j] && Yext[i + 1] > Yk[j]){
                    Xkext[kc] = Xext[i] + (Xext[i + 1] - Xext[i]) / (Yext[i + 1] - Yext[i]) * (Yk[j] - Yext[i]);
                    kc++;
                }

                if (Yext[i] > Yk[j] && Yext[i + 1] <= Yk[j]){
                    Xkext[kc] = Xext[i] + (Xext[i + 1] - Xext[i]) / (Yext[i + 1] - Yext[i]) * (Yk[j] - Yext[i]);
                    kc++;
                }
                i++;
            } 
            while (i < nbpc - 1);
        }

        if (nbpe > 2){
            i = 0;
            do{
                if (Yint[i] <= Yk[j] && Yint[i + 1] > Yk[j]){
                    Xkint[ke] = Xint[i] + (Xint[i + 1] - Xint[i]) / (Yint[i + 1] - Yint[i]) * (Yk[j] - Yint[i]);
                    ke++;
                }

                if (Yint[i] > Yk[j] && Yint[i + 1] <= Yk[j]){
                    Xkint[ke] = Xint[i] + (Xint[i + 1] - Xint[i]) / (Yint[i + 1] - Yint[i]) * (Yk[j] - Yint[i]);
                    ke++;
                }
                i++;
            }
            while (i < nbpe - 1);
        }


        lc = kc;
        le = ke;
        kc = 0;
        ke = 0;

        //Algorithme de tri croissant des Xkext[0 à lc] avec Xkext[lc] = max et Xkext[0]= min
        do{
            i = 0;
            rang = 0;
            do{
                if (Xkext[kc] > Xkext[i]){
                    rang++;
                }
                i++;
            } 
            while (i < lc);
            if (rang != kc){
                brou = Xkext[rang];
                Xkext[rang] = Xkext[kc];
                Xkext[kc] = brou;
            }else{
                kc++;
            }
        } 
        while (kc < lc);

        //Algorithme de tri croissant des Xkint[0 à le] avec Xkint[le] = max et Xkint[0]=min
        if (Yk[j] > Yintmin && Yk[j] < Yintmax){
            do{
                i = 0;
                rang = 0;
                do{
                    if (Xkint[ke] > Xkint[i]){
                        rang++;
                    }
                    i++;
                } 
                while (i < le);
                if (rang != ke){
                    brou = Xkint[rang];
                    Xkint[rang] = Xkint[ke];
                    Xkint[ke] = brou;
                }else{
                    ke++;
                }
            } 
            while (ke < le);
        }

        kc = 0;
        ke = 0;
        larg = 0;

        //Calcul de la largeur de béton/acier à l'ordonnée Yk[j]
        do{
            larg = larg + Math.abs(Xkext[kc + 1] - Xkext[kc]);
            kc = kc + 2;
        } 
        while (kc < lc);
        ke = 0;

        //Calcul de la largeur de l'évidemment à l'ordonnée Yk[j]
        if (Yk[j] > Yintmin && Yk[j] < Yintmax){
            do{
                larg = larg - Math.abs(Xkint[ke + 1] - Xkint[ke]);
                ke = ke + 2;
            } 
            while (ke < le);
        }

        if (larg < 0){
            nega = 1;
        }

        nbel = 0;
        poin = 0;
        i = 0;

        do{
            Xk[i] = Xextmin + deltaX * (i + 0.5);
            kc = 0;
            ke = 0;
            poin = 0;
            do{
                //détermine si le point de coordonnées (Xk[i],Yk[j]) est situé dans la section pleine si oui: poin = 1
                if (Xkext[kc + 1] > Xk[i] && Xkext[kc] < Xk[i]){
                    poin = 1;
                }
                kc = kc + 2;
            } 
            while (kc < lc);

            if (Yk[j] > Yintmin && Yk[j] < Yintmax){
                do{
                    //détermine si le point de coordonnées (Xk[i],Yk[j]) est situé dans la section vide si oui: poin = 0
                    if (Xkint[ke + 1] > Xk[i] && Xkint[ke] < Xk[i]){
                        poin = 0;
                    }
                    ke = ke + 2;
                } 
                while (ke < le);
            }
            if (poin == 1){
                nbel++;
                Xbet[nxbe] = Xk[i];
                nxbe++;
            }
            i++;
        } 
        while (i < 100);

        if (nbel != 0){
            dlar = larg / nbel;
            i = 0;
            do{
                bbet[nbbe + i] = dlar;
                Ybet[nybe + i] = Yk[j];
                i++;
            } 
            while (i < nbel);
            nbbe = nbbe + nbel;
            nybe = nybe + nbel;
        }

        j++;

    }
    while(j<100);

    nbel = nybe;

    if (nega == 1){
        for (i = 0; i < nbpe; i++){
            S[i] = -1;
        }
    }

    i = 0;

    surf = 0;
    ixx = 0;
    iyy = 0;
    ixy = 0;
    surx = 0;
    sury = 0;

    do{
        sbet[i] = deltaY * bbet[i];
        surf = surf + sbet[i];
        surx = surx + (Xbet[i] * bbet[i] * deltaY);
        sury = sury + (Ybet[i] * bbet[i] * deltaY);
        i++;
    } 
    while (i < nbel);

    xgra = surx / surf;
    ygra = sury / surf;

    i = 0;
    
    do{
        Xbet[i] = Xbet[i] - xgra;
        Ybet[i] = Ybet[i] - ygra;
        i++;
    } 
    while (i < nbel);

    i = 0;
    do{
        iyyb[i] = (Math.pow(deltaX, 3) * bbet[i] / 12) + (deltaY * bbet[i] * Math.pow(Ybet[i], 2));
        ixxb[i] = (Math.pow(bbet[i], 3) * deltaY / 12) + (deltaY * bbet[i] * Math.pow(Xbet[i], 2));
        ixyb[i] = (Xbet[i]) * (Ybet[i]) * bbet[i] * deltaY;
        sxbe[i] = (Xbet[i] * bbet[i] * deltaY);
        sybe[i] = (Ybet[i] * bbet[i] * deltaY);

        ixx = ixx + ixxb[i];
        iyy = iyy + iyyb[i];
        ixy = ixy + ixyb[i];
        i++;
    } 
    while (i < nbel);

    S[0] = Hext;
    S[1] = surf;
    S[2] = ixx;
    S[3] = iyy;
    S[4] = ixy;
    S[5] = xgra;
    S[6] = ygra;
    
    return S;
}

module.exports = sectionanalysis;