//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++             FORMULAIRE DE CALCUL RDM POUR LES BARRES BI-ARTICULEE/BI-ENCASTREE
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//SOMMAIRE

//1 - Charges ponctuelles pour Poutre Bi-Articulé

//2 - Charges uniformes pour Poutre Bi-Articulé

//3 - Charges ponctuelles pour Poutre Bi-Encastré

//4 - Charges uniformes pour Poutre Bi-Encastré


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//1 - Charges ponctuelles pour Poutre Bi-Articulé
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function MP(t, a, c, l, x){
    var b;
    var result;
    b = l - a;
    if (0 <= x && x <= a){
        result = t * b * x / l;
        return result;
    }else{
        result = t * a * (1 - x) / l;
        return result;
    }
}

module.exports = MP;

function VP(t, a, c, l, x){
    var b;
    var result;
    b = l - a;
    if (0 <= x && x <= a){
        result = t * b / l;
        return result;
    }else{
        result = -t * a / l;
        return result;
    }
}

module.exports = VP;

function NP(t, a, c, l, x){
    var b;
    var result;
    b = l - a;
    if (0 <= x && x <= a){
        result = t * (l - a) / l;
        return result;
    }else{
        result = t * a / l;
        return result;
    }
}

module.exports = NP;

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//2 - Charges uniformes pour Poutre Bi-Articulé
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function MD(p, a, c, l, x){
    var b;
    var result;
    b = l - a - c;
    if (0 <= x && x <= a){
        result = p * b * (b + 2 * c) * x / (2 * l);
        return result;
    }else{
        if (a < x && x <= a + b){
            result = p * b * (b + 2 * c) * x / (2 * l) - p * Math.pow((x - a), 2) / 2;
            return result;
        }else{
            result = p * b * (2 * a + b) * (l - x) / (2 * l);
            return result;
        }
    }
}

module.exports = MD;

function VD(p, a, c, l, x){
    var b;
    var result;
    b = l - a - c;
    if (0 <= x && x <= a){
        result = p * b * (b + 2 * c) / (2 * l);
        return result;
    }else{
        if (a < x && x <= a + b){
            result = p * b * (b + 2 * c) / (2 * l) - p * (x - a);
            return result;
        }else{
            result = p * b * (2 * a + b) * (-1) / (2 * l);
            return result;
        }
    }
}

module.exports = VD;

function ND(p, a, c, l, x){
    var b;
    var result;
    b = l - a - c;
    if (0 <= x && x <= a){
        result = -p * b * (l - a) / (l);
        return result;
    }else{
        if (a < x && x <= a + b){
            result = p * x + (-p * b * (l - a) - p * a * l) / l;
            return result;
        }else{
            result = p * b * a / l;
            return result;
        }
    }
}

module.exports = ND;

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//3 - Pour les charges ponctuelles pour Poutre Bi-Encastré
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

function MPe(t, a, c, l, x){
    var b, Ma, Mb;
    var result;
    b = l - a;
    Ma = -t * a * Math.pow(b / l, 2);
    Mb = -t * b * Math.pow(a / l, 2);

    result = MP(t, a, c, l, x) + Ma * (1 - x / l) + Mb * x / l;
    return result;
}

module.exports = MPe;

function VPe(t, a, c, l, x){
    var b, Ma, Mb;
    var result;
    b = l - a;
    Ma = -t * a * Math.pow(b / l, 2);
    Mb = -t * b * Math.pow(a / l, 2);

    result = VP(t, a, c, l, x) + (Ma - Mb) / l;
    return result;
}

module.exports = VPe;

function NPe(t, a, c, l, x){
    var b;
    var result;
    b = l - a;

    if (0 <= x && x <= a){
        result = -t;
        return result;
    }else{
        result = 0;
        return result;
    }
}

module.exports = NPe;

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//4 - Pour les charges uniformes pour Poutre Bi-Encastré
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function MDe(p, a, c, l, x){
    var b, Ma, Mb;
    var result;
    b = l - a - c;
    Ma = -p * b / (24 * Math.pow(l, 2)) * ((b + 4 * c - 2 * a) * (4 * Math.pow(l, 2) - Math.pow(b, 2)) - 2 * Math.pow(b + 2 * c, 3) + Math.pow(b + 2 * a, 3));
    Mb = -p * b / (24 * Math.pow(l, 2)) * ((b + 4 * a - 2 * c) * (4 * Math.pow(l, 2) - Math.pow(b, 2)) - 2 * Math.pow(b + 2 * a, 3) + Math.pow(b + 2 * c, 3));

    result = MD(p, a, c, l, x) + Ma * (1 - x / l) + Mb * x / l;
    return result;
}

module.exports = MDe;

function VDe(p, a, c, l, x){
    var b, Ma, Mb;
    var result;
    b = l - a - c;
    Ma = -p * b / (24 * Math.pow(l, 2)) * ((b + 4 * c - 2 * a) * (4 * Math.pow(l, 2) - Math.pow(b, 2)) - 2 * Math.pow(b + 2 * c, 3) + Math.pow(b + 2 * a, 3));
    Mb = -p * b / (24 * Math.pow(l, 2)) * ((b + 4 * a - 2 * c) * (4 * Math.pow(l, 2) - Math.pow(b, 2)) - 2 * Math.pow(b + 2 * a, 3) + Math.pow(b + 2 * c, 3));

    result = VD(p, a, c, l, x) + (Ma - Mb) / l;
    return result;
}

module.exports = VDe;

function NDe(p, a, c, l, x){
    var b;
    var result;
    b = l - a - c;

    if (0 <= x && x <= a){
        result = -p * b;
        return result;
    }else{
        if (a < x && x <= a + b){
            result = p * x - p * (a + b);
            return result;
        }else{
            result = 0;
            return result;
        }
    }
}

module.exports = NDe;