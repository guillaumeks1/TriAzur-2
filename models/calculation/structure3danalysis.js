function structure3danalysis(listmaterials, listsections, listsupports, listnodes, listmembers, listnodeloads){
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++                                       INITIALISATION DES INPUTS                                                 ++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    var structure3danalysismessage = [];
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //Récupération des inputs relatifs aux:
    //1- Caractéristiques des matériaux (MATERIALS)
    //2- Caractéristiques des sections (SECTIONS)
    //3- Caractéristiques des appuis (SUPPORTS)
    //4- Caractéristiques des noeuds (NODES)
    //5- Carcatéristiques des barres (MEMBERS)
    //6- Carcatéristiques des charges appliquées aux noeuds (NODES LOADS)

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //1- Caractéristiques des matériaux (MATERIALS)
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    var imat = 0;
    var kmat = listmaterials.length;
    var Materials = [];
    var ro = [];
    var Eyoung = [];
    var Poisson = [];
    var Temp = [];
    var Gcisaill = [];

    for(imat = 0; imat < kmat; imat++){
        Materials[imat] = listmaterials[imat].Name;
        ro[imat] = listmaterials[imat].Density;
        Eyoung[imat] = listmaterials[imat].Eyoung;
        Poisson[imat] = listmaterials[imat].Poisson;
        Temp[imat] = listmaterials[imat].Thermal;
        Gcisaill[imat] = listmaterials[imat].Gmodulus;
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //2- Caractéristiques des sections (SECTIONS)
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    var isec = 0;
    var ksec = listsections.length;
    var Sections = [];
    var Mat = [];
    var Area = [];
    var Inertia1 = [];
    var Inertia2 = [];
    var Inertia3 = [];

    for(isec = 0; isec < ksec; isec++){
        Sections[isec] = listsections[isec].Name;
        for(imat = 0; imat < kmat; imat++){
            if(listsections[isec].MaterialId === listmaterials[imat]._id){
                Mat[isec] = listmaterials[imat].Name;
            }
        }
        if(Mat[isec]===""){
            structure3danalysismessage[0] = Sections[isec] + " with not defined material";
            return structure3danalysismessage;
        }
        Area[isec] = listsections[isec].Area;
        Inertia1[isec] = listsections[isec].J;
        Inertia2[isec] = listsections[isec].I2;
        Inertia3[isec] = listsections[isec].I3;
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //3- Caractéristiques des appuis (SUPPORTS)
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    var isupp = 0;
    var ksupp = listsupports.length;
    var Supports = [];
    var DispX = [];
    var DispY = [];
    var DispZ = [];
    var RotX = [];
    var RotY = [];
    var RotZ = [];

    for(isupp = 0; isupp < ksupp; isupp++){
        Supports[isupp] = listsupports[isupp].Name;
        DispX[isupp] = listsupports[isupp].DispX;
        DispY[isupp] = listsupports[isupp].DispY;
        DispZ[isupp] = listsupports[isupp].DispZ;
        RotX[isupp] = listsupports[isupp].RotX;
        RotY[isupp] = listsupports[isupp].RotY;
        RotZ[isupp] = listsupports[isupp].RotZ;
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //4- Caractéristiques des noeuds (NODES)
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    var inodes = 0;
    var knodes = listnodes.length;
    var Nodes = [];
    var Supp = [];
    var Xi = [];
    var Yi = [];
    var Zi = [];

    for(inodes = 0; inodes < knodes; inodes++){
        Nodes[inodes] = listnodes[inodes].Name;
        for(isupp = 0; isupp < ksupp; isupp++){
            if(listnodes[inodes].SupportId === listsupports[isupp]._id){
                Supp[inodes] = listsupports[isupp].Name;
            }
        }
        if(Supp[inodes]===""){
            structure3danalysismessage[1] = Nodes[inodes] + " with not defined support";
            return structure3danalysismessage;
        }
        Xi[inodes] = listnodes[inodes].X;
        Yi[inodes] = listnodes[inodes].Y;
        Zi[inodes] = listnodes[inodes].Z;
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //5- Carcatéristiques des barres (MEMBERS)
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    var imembers = 0;
    var kmembers = listmembers.length;
    var Members = [];
    var Nodesi = [];
    var Nodesj = [];
    var Sec = [];

    for(imembers = 0; imembers < kmembers; imembers++){
    Members[imembers] = listmembers[imembers].Name;
        for(inodes = 0; inodes < knodes; inodes++){
            if(listmembers[imembers].NodeiId === listnodes[inodes]._id){
                Nodesi[imembers] = listnodes[inodes].Name;
            }
            if(listmembers[imembers].NodejId === listnodes[inodes]._id){
                Nodesj[imembers] = listnodes[inodes].Name;
            }
        }
        if(Nodesi[imembers]===""){
            structure3danalysismessage[2] = Members[imembers] + " with not defined node";
            return structure3danalysismessage;
        }
        if(Nodesj[imembers]===""){
            structure3danalysismessage[3] = Members[imembers] + " with not defined node";
            return structure3danalysismessage;
        }
        
        for(isec = 0; isec < ksec; isec++){
            if(listmembers[imembers].SectionId === listsections[isec]._id){
                Sec[imembers] = listsections[isec].Name;
            }
        }

        if(Sec[imembers]===""){
            structure3danalysismessage[4] = Members[imembers] + " with not defined section";
            return structure3danalysismessage;
        }
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //6- Carcatéristiques des charges appliquées aux noeuds (NODES LOADS)
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    var inloads = 0;
    var knloads = listnodeloads.length;
    var NodesLoadsName = [];
    var NodesLoads = [];
    var FXNodesLoads =[];
    var FYNodesLoads =[];
    var FZNodesLoads =[];
    var MXNodesLoads =[];
    var MYNodesLoads =[];
    var MZNodesLoads =[];

    for(inloads = 0; inloads < knloads; inloads++){
        NodesLoadsName[inloads] = listnodeloads[inloads].Name; 
        for(inodes = 0; inodes < knodes; inodes++){
            if(listnodeloads[inloads].NodeId === listnodes[inodes]._id){
                NodesLoads[inloads] = listnodes[inodes].Name;
            }
        }
        if(NodesLoads[inloads]===""){
            structure3danalysismessage[5] = NodesLoadsName[inloads] + " applied on a not defined node";
            return structure3danalysismessage;
        }
        FXNodesLoads[inloads] = listnodeloads[inloads].ForceX;
        FYNodesLoads[inloads] = listnodeloads[inloads].ForceY;
        FZNodesLoads[inloads] = listnodeloads[inloads].ForceZ;
        MXNodesLoads[inloads] = listnodeloads[inloads].MomentX;
        MYNodesLoads[inloads] = listnodeloads[inloads].MomentY;
        MZNodesLoads[inloads] = listnodeloads[inloads].MomentZ;
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //7- Carcatéristiques des charges appliquées aux barres (MEMBERS LOADS)
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // var imloads = 0;
    // var kmloads = listmemberloads.length;
    // var MembersLoadsName = [];
    // var MembersLoads = [];
    // var TypeLoads = [];
    // var LoadsDirection = [];
    // var aLoads = [];
    // var PaLoads = [];
    // var bLoads = [];
    // var PbLoads = [];

    // for(inloads = 0; inloads < knloads; inloads++){
    //     MembersLoadsName[imloads] = listmemberloads[imloads].Name; 
    //     for(imembers = 0; imembers < knodes; imembers++){
    //         if(listmemberloads[imloads].MemberId === listmembers[imembers]._id){
    //             MembersLoads[imloads] = listmembers[imembers].Name;
    //         }
    //     }
    //     if(MembersLoads[imloads]===""){
    //         structure3danalysismessage[6] = MembersLoadsName[inloads] + " applied on a not defined member";
    //         return structure3danalysismessage;
    //     }
    //     TypeLoads[imloads] = listmemberloads[imloads].Type;
    //     LoadsDirection[imloads] = listmemberloads[imloads].Direction;
    //     aLoads[imloads] = listmemberloads[imloads].DistanceA;
    //     PaLoads[imloads] = listmemberloads[imloads].LoadPA;
    //     bLoads[imloads] = listmemberloads[imloads].DistanceB;
    //     PbLoads[imloads] = listmemberloads[imloads].LoadPB;
    // }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //8- Caractéristiques de la pushover analysis (PUSHOVER)
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //var OptionPushover = 0;
    //object NodePushover, DirectionPushover, DisplacementMaxPushover;

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++                          CONSTRUCTION DE LA MATRICE DE RIGIDITE DE LA STRUCTURE                                 ++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //0- Algorithme de tri servant à vérifier que les noeuds définis correspondent bien aux noeuds d'extrémités des barres
    //00- Boucle permettant un calcul de pushover analysis
    //Récupération des caractéristiques de chaque Barre (MEMBERS) et construction de sa matrice de rigidité [K]:
    //1- Détermination des caractéristiques de la section associée à la barre
    //2- Determination des caractéristiques du matériaux de la section associée à la barre
    //3- Détermination des coordonnées des noeuds d'extrémité de la barre
    //4- Construction de la matrice de rigidité de la barre [K] dans le repère locale
    //   4.1- Impact de la nonlinearité des matériaux sur les matrices de rigidité dans le cas d'une pushover analysis
    //   4.2- Construction de la matrice [Kii]
    //   4.3- Construction de la matrice [Kji]
    //   4.4- Construction de la matrice [Kij]
    //   4.5- Construction de la matrice [Kjj]
    //   4.6- Construction de la matrice [KMemb] représentant la matrice 12x12 de la barre
    //5- Construction de la matrice de changement de base [P] du repère locale de la barre dans le repère globale
    //   5.1- Construction de la matrice [Pii]
    //   5.2- Construction de la matrice [Pji]
    //   5.3- Construction de la matrice [Pij]
    //   5.4- Construction de la matrice [Pjj]
    //   5.5- Construction de la matrice [PMemb] représentant la matrice 12x12 de changement de base de la barre dans le repère globale
    //6- Construction de la matrice de changement de base [InvP] du repère globale dans le repère locale de la barre
    //7- Construction de la matrice de rigidité de la barre [Kg]=[InvP][K][P] dans le repère globale
    //   7.1- Construction de la matrice [Kgii]=[InvPii][Kii][Pii]
    //  7.2- Construction de la matrice [Kgji]=[InvPji][Kji][Pji]
    //   7.3- Construction de la matrice [Kgij]=[InvPij][Kij][Pij]
    //   7.4- Construction de la matrice [Kgjj]=[InvPjj][Kjj][Pjj]
    //8- Remplissage de la matrice total de la structure [Ktotal]
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    //Caractéristiques de section associées aux barres
    var MatMemb = [];
    var AreaMemb = [];
    var Inertia2Memb = [];
    var Inertia3Memb = [];
    var Inertia1Memb = [];

    //Caractéristiques du matériaux associé aux sections des barres
    var roMemb = [];
    var EyoungMemb = [];
    var PoissonMemb = [];
    var TempMemb = [];
    var GcisaillMemb = [];

    //Caractéristiques géométriques des Noeuds d'extrémité de la barre
    var XiMemb = [];
    var YiMemb = [];
    var ZiMemb = [];
    var XjMemb = [];
    var YjMemb = [];
    var ZjMemb = [];
    var LijMemb = [];

    //Caractéristiques matricielles de raideur et de changement de base
    var KiiMemb = [];
    var PiiMemb = [];
    var InvPiiMemb = [];
    var KjiMemb = [];
    var PjiMemb = [];
    var KijMemb = [];
    var PijMemb = [];
    var KjjMemb = [];
    var PjjMemb = [];
    var KMemb = [];
    var PMemb = [];
    var InvPMemb = [];
    var KgMemb = [];
    var KgMemb1 = [];

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //0- Algorithme de tri servant à vérifier que les noeuds définis correspondent bien aux noeuds d'extrémités des barres
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //      0.1- Construction de l'ensemble des noeuds d'extrémités des barres [NodesMemb]
    var kNodesMemb, Counter, MembNodei, MembNodej, Counteri, Counterj, iNodesMemb, t;
    var Phii, Phij, Mi, Mj, Myi, Mpl, f1, f2, f3, f4, f5, f6, f7, c0, c1, c2, c3, c4, c5, c6;

    kNodesMemb = 2;
    var NodesMemb = [];

    if (knodes == 0){
        structure3danalysismessage[0] = "No Node define";
        return structure3danalysismessage;
    }
    else{
        structure3danalysismessage[0] = knodes + " Nodes define";
        NodesMemb[0] = Nodesi[0];
        NodesMemb[1] = Nodesj[0];
    }

    for (var i = 0; i < kmembers; i++){
        Counteri = 0;
        Counterj = 0;
        t = kNodesMemb;
        for (var j = 0; j < t; j++){
            if (NodesMemb[j] != Nodesi[i]){
                Counteri++;
            }
            if (Counteri == t){
                kNodesMemb++;
                //Array.Resize(ref NodesMemb, kNodesMemb);
                NodesMemb[kNodesMemb - 1] = Nodesi[i];
            }
            if (NodesMemb[j] != Nodesj[i]){
                Counterj++;
            }{
                kNodesMemb++;
                //Array.Resize(ref NodesMemb, kNodesMemb);
                NodesMemb[kNodesMemb - 1] = Nodesj[i];
            }

        }
    }
    //      0.2- Vérification que Dim[NodesMemb] = Dim[Nodes] et création de [iNodesToMembs] fonction permettant de relié les noeuds définis aux noeuds des barres
    var iNodesToMembs = [];

    structure3danalysismessage[1] = "";
    inodes = 0;
    do{
        Counter = 0;
        for (iNodesMemb = 0; iNodesMemb < kNodesMemb; iNodesMemb++){
            if (Nodes[inodes] != NodesMemb[iNodesMemb]){
                Counter++;
            }
            else{
                iNodesToMembs[iNodesMemb] = inodes;
            }
        }
        if (Counter == kNodesMemb){
            structure3danalysismessage[1] = structure3danalysismessage[1] + "Noeud " + Nodes[inodes] + " non attribué à une barre";
            return structure3danalysismessage;
        }
        inodes++;
    } while (inodes < knodes);
    if (structure3danalysismessage[1] == ""){
        structure3danalysismessage[1] = "Tous les noeuds ont été associés à des barres";
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //Définitions des vecteurs forces et déplacements ainsi que les matrices de rigidité

    //Déplacements
    var Ui = [];
    var UimembersG = [];
    var Uimembers = [];
    var Uinconnu = [];
    var Uconnu = [];
    //Efforts
    var Fi = [];
    var Fiext = [];
    var PaMembLoads = [];
    var PaMembLoadsG = [];
    var PbMembLoads = [];
    var PbMembLoadsG = [];
    var FMembNodeiLoads = [];
    var FMembNodeiLoadsG = [];
    var FMembNodejLoads = [];
    var FMembNodejLoadsG = [];
    var FimembersG = [];
    var Fimembers = [];
    var Finconnu = [];
    var Fconnu = [];
    //Sollicitations
    var FMembSoll = [];
    //Rigidité
    var Ktotal = [];

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    for (inodes = 0; inodes < 6 * kNodesMemb; inodes++){
        Fi[inodes] = 0;
        Fiext[inodes] = 0;
    }
    structure3danalysismessage[2] = "";
    structure3danalysismessage[3] = "";
    structure3danalysismessage[4] = "";

    for (imembers = 0; imembers < kmembers; imembers++){
        PiiMemb[imembers] = [];
        PijMemb[imembers] = [];
        PjiMemb[imembers] = [];
        PjjMemb[imembers] = [];
        PMemb[imembers] = [];
        InvPMemb[imembers] = [];
        KgMemb[imembers] = [];
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //1- Détermination des caractéristiques de la section associée à la barre
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
        isec = 0;
        imat = 0;
        MembNodei = 0;
        MembNodej = 0;
        do{
            if (Sec[imembers] === Sections[isec]){
                MatMemb[imembers] = Mat[isec];
                AreaMemb[imembers] = Area[isec];
                Inertia2Memb[imembers] = Inertia2[isec];
                Inertia3Memb[imembers] = Inertia3[isec];
                Inertia1Memb[imembers] = Inertia1[isec];
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //2- Determination des caractéristiques du matériaux de la section associée à la barre
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                do{
                    if (Materials[imat] === MatMemb[imembers]){
                        roMemb[imembers] = ro[imat];
                        EyoungMemb[imembers] = Eyoung[imat];
                        PoissonMemb[imembers] = Poisson[imat];
                        TempMemb[imembers] = Temp[imat];
                        GcisaillMemb[imembers] = Gcisaill[imat];
                        break;
                    }
                    else{
                        if (imat === kmat){
                            structure3danalysismessage[2] = structure3danalysismessage[2] + "Barre " + Members[imembers] + " sans caractéristique de matériaux";
                            return structure3danalysismessage;
                        }
                    }
                    imat++;
                } while (imat < kmat);
                break;
            }
            else{
                if (isec === ksec){
                    structure3danalysismessage[3] = structure3danalysismessage[3] + "Barre " + Members[imembers] + " sans caractéristique de section";
                    return structure3danalysismessage;
                }
            }
            isec++;
        } while (isec < ksec);
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //3- Détermination des coordonnées des noeuds d'extrémité de la barre
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        inodes = 0;
        Counter = 0;
        do{
            if (Nodesi[imembers] === Nodes[inodes]){
                Counter++;
                MembNodei = inodes;
                XiMemb[imembers] = Xi[inodes];
                YiMemb[imembers] = Yi[inodes];
                ZiMemb[imembers] = Zi[inodes];
            }
            if (Nodesj[imembers] == Nodes[inodes]){
                Counter++;
                MembNodej = inodes;
                XjMemb[imembers] = Xi[inodes];
                YjMemb[imembers] = Yi[inodes];
                ZjMemb[imembers] = Zi[inodes];
            }
            if (Counter == 2){
                break;
            }
            inodes++;
        } while (inodes < knodes);

        if (Counter != 2){
            structure3danalysismessage[4] = structure3danalysismessage[4] + "Barre " + Members[imembers] + " sans noeud associé";
            return structure3danalysismessage;
        }
        LijMemb[imembers] = Math.pow(Math.pow((XiMemb[imembers] - XjMemb[imembers]), 2) + Math.pow((YiMemb[imembers] - YjMemb[imembers]), 2) + Math.pow((ZiMemb[imembers] - ZjMemb[imembers]), 2), 0.5);

        if (LijMemb[imembers] === 0){
            structure3danalysismessage[4] = structure3danalysismessage[4] + "Barre " + Members[imembers] + " de longueur nulle";
            return structure3danalysismessage;
        }
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //4- Construction de la matrice de rigidité de la barre [K]
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //4.1- Impact de la nonlinearité des matériaux sur les matrices de rigidité dans le cas d'une pushover analysis
        //if(OptionPushover==0){
        Phii = 0;
        Phij = 0;
        //suite du pushover à rajouter
        f1 = 16 - 8 * Phii - 8 * Phij + 3 * Phii * Phij;
        f2 = 2 * (8 - 7 * Phii - 7 * Phij + 6 * Phii * Phij);
        f3 = 2 * (8 - 8 * Phii - 6 * Phij + 6 * Phii * Phij);
        f4 = 2 * (8 - 6 * Phii - 8 * Phij + 6 * Phii * Phij);
        f5 = 2 * (8 - 8 * Phii - 5 * Phij + 5 * Phii * Phij);
        f6 = 2 * (8 - 5 * Phii - 8 * Phij + 5 * Phii * Phij);
        f7 = 16 * (1 - Phii - Phij - Phii * Phij);

        c0 = Math.pow((1 - Phii - Phij + Phii * Phij), 0.5);
        c1 = f2 / f1;
        c2 = f3 / f1;
        c3 = f4 / f1;
        c4 = f5 / f1;
        c5 = f6 / f1;
        c6 = f7 / f1;
        //4.2- Construction de la matrice [Kii]
        for (var i = 0; i < 6; i++){
            KiiMemb[i] = [];
            for (var j = 0; j < 6; j++){
                KiiMemb[i][j] = 0;
            }
        }
        KiiMemb[0][0] = EyoungMemb[imembers] * AreaMemb[imembers] / LijMemb[imembers] * c0;
        KiiMemb[1][1] = 12 * EyoungMemb[imembers] * Inertia3Memb[imembers] / Math.pow(LijMemb[imembers], 3) * c1;
        KiiMemb[2][2] = 12 * EyoungMemb[imembers] * Inertia2Memb[imembers] / Math.pow(LijMemb[imembers], 3) * c1;
        KiiMemb[3][3] = GcisaillMemb[imembers] * Inertia1Memb[imembers] / LijMemb[imembers];
        KiiMemb[4][4] = 4 * EyoungMemb[imembers] * Inertia2Memb[imembers] / LijMemb[imembers] * c4;
        KiiMemb[5][5] = 4 * EyoungMemb[imembers] * Inertia3Memb[imembers] / LijMemb[imembers] * c4;
        KiiMemb[4][2] = -6 * EyoungMemb[imembers] * Inertia2Memb[imembers] / Math.pow(LijMemb[imembers], 2) * c2;
        KiiMemb[2][4] = -6 * EyoungMemb[imembers] * Inertia2Memb[imembers] / Math.pow(LijMemb[imembers], 2) * c2;
        KiiMemb[5][1] = 6 * EyoungMemb[imembers] * Inertia3Memb[imembers] / Math.pow(LijMemb[imembers], 2) * c2;
        KiiMemb[1][5] = 6 * EyoungMemb[imembers] * Inertia3Memb[imembers] / Math.pow(LijMemb[imembers], 2) * c2;
        //4.3- Construction de la matrice [Kji]
        for (var i = 0; i < 6; i++){
            KjiMemb[i] = [];
            for (var j = 0; j < 6; j++){
                KjiMemb[i][j] = 0;
            }
        }
        KjiMemb[0][0] = -EyoungMemb[imembers] * AreaMemb[imembers] / LijMemb[imembers] * c0;
        KjiMemb[1][1] = -12 * EyoungMemb[imembers] * Inertia3Memb[imembers] / Math.pow(LijMemb[imembers], 3) * c1;
        KjiMemb[2][2] = -12 * EyoungMemb[imembers] * Inertia2Memb[imembers] / Math.pow(LijMemb[imembers], 3) * c1;
        KjiMemb[3][3] = -GcisaillMemb[imembers] * Inertia1Memb[imembers] / LijMemb[imembers];
        KjiMemb[4][4] = 2 * EyoungMemb[imembers] * Inertia2Memb[imembers] / LijMemb[imembers] * c6;
        KjiMemb[5][5] = 2 * EyoungMemb[imembers] * Inertia3Memb[imembers] / LijMemb[imembers] * c6;
        KjiMemb[4][2] = -6 * EyoungMemb[imembers] * Inertia2Memb[imembers] / Math.pow(LijMemb[imembers], 2) * c3;
        KjiMemb[2][4] = 6 * EyoungMemb[imembers] * Inertia2Memb[imembers] / Math.pow(LijMemb[imembers], 2) * c2;
        KjiMemb[5][1] = 6 * EyoungMemb[imembers] * Inertia3Memb[imembers] / Math.pow(LijMemb[imembers], 2) * c3;
        KjiMemb[1][5] = -6 * EyoungMemb[imembers] * Inertia3Memb[imembers] / Math.pow(LijMemb[imembers], 2) * c2;
        //4.4- Construction de la matrice [Kij]
        for (var i = 0; i < 6; i++){
            KijMemb[i] = [];
            for (var j = 0; j < 6; j++){
                KijMemb[i][j] = 0;
            }
        }
        KijMemb[0][0] = -EyoungMemb[imembers] * AreaMemb[imembers] / LijMemb[imembers] * c0;
        KijMemb[1][1] = -12 * EyoungMemb[imembers] * Inertia3Memb[imembers] / Math.pow(LijMemb[imembers], 3) * c1;
        KijMemb[2][2] = -12 * EyoungMemb[imembers] * Inertia2Memb[imembers] / Math.pow(LijMemb[imembers], 3) * c1;
        KijMemb[3][3] = -GcisaillMemb[imembers] * Inertia1Memb[imembers] / LijMemb[imembers];
        KijMemb[4][4] = 2 * EyoungMemb[imembers] * Inertia2Memb[imembers] / LijMemb[imembers] * c6;
        KijMemb[5][5] = 2 * EyoungMemb[imembers] * Inertia3Memb[imembers] / LijMemb[imembers] * c6;
        KijMemb[4][2] = 6 * EyoungMemb[imembers] * Inertia2Memb[imembers] / Math.pow(LijMemb[imembers], 2) * c2;
        KijMemb[2][4] = -6 * EyoungMemb[imembers] * Inertia2Memb[imembers] / Math.pow(LijMemb[imembers], 2) * c3;
        KijMemb[5][1] = -6 * EyoungMemb[imembers] * Inertia3Memb[imembers] / Math.pow(LijMemb[imembers], 2) * c2;
        KijMemb[1][5] = 6 * EyoungMemb[imembers] * Inertia3Memb[imembers] / Math.pow(LijMemb[imembers], 2) * c3;
        //4.5- Construction de la matrice [Kjj]
        for (var i = 0; i < 6; i++){
            KjjMemb[i] = [];
            for (var j = 0; j < 6; j++){
                KjjMemb[i, j] = 0;
            }
        }
        KjjMemb[0][0] = EyoungMemb[imembers] * AreaMemb[imembers] / LijMemb[imembers] * c0;
        KjjMemb[1][1] = 12 * EyoungMemb[imembers] * Inertia3Memb[imembers] / Math.pow(LijMemb[imembers], 3) * c1;
        KjjMemb[2][2] = 12 * EyoungMemb[imembers] * Inertia2Memb[imembers] / Math.pow(LijMemb[imembers], 3) * c1;
        KjjMemb[3][3] = GcisaillMemb[imembers] * Inertia1Memb[imembers] / LijMemb[imembers];
        KjjMemb[4][4] = 4 * EyoungMemb[imembers] * Inertia2Memb[imembers] / LijMemb[imembers] * c5;
        KjjMemb[5][5] = 4 * EyoungMemb[imembers] * Inertia3Memb[imembers] / LijMemb[imembers] * c5;
        KjjMemb[4][2] = 6 * EyoungMemb[imembers] * Inertia2Memb[imembers] / Math.pow(LijMemb[imembers], 2) * c3;
        KjjMemb[2][4] = 6 * EyoungMemb[imembers] * Inertia2Memb[imembers] / Math.pow(LijMemb[imembers], 2) * c3;
        KjjMemb[5][1] = -6 * EyoungMemb[imembers] * Inertia3Memb[imembers] / Math.pow(LijMemb[imembers], 2) * c3;
        KjjMemb[1][5] = -6 * EyoungMemb[imembers] * Inertia3Memb[imembers] / Math.pow(LijMemb[imembers], 2) * c3;
        //4.6- Construction de la matrice [KMemb] représentant la matrice 12x12 de la barre
        for (var i = 0; i < 6; i++){
            KMemb[i] = [];
            KMemb[i+6] = [];
            for (var j = 0; j < 6; j++){
                KMemb[i][j] = KiiMemb[i][j];
                KMemb[i+6][j] = KjiMemb[i][j];
                KMemb[i][j+6] = KijMemb[i][j];
                KMemb[i+6][j+6] = KjjMemb[i][j];
            }
        }
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //5- Construction de la matrice de changement de base [P] du repère locale de la barre dans le repère globale
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //Rg = {uX,uY,uZ} = Repère globale
        //Rl = {u1,u2,u3} = Repère locale de la barre avec ux colinéaire à l'axe de la barre
        //- Avec Vecteur(u1) = Vecteur(AiAj)/Norme(AiAj)
        //- Avec Vecteur(u2) = (Vecteur(uZ) ^ Vecteur(u1))/Norme(Vecteur(uZ) ^ Vecteur(u1))
        //- Avec Vecteur(u3) = (Vecteur(u1) ^ Vecteur(u2))/Norme(Vecteur(u1) ^ Vecteur(u2))
        //5.1- Construction de la matrice [Pii]
        for (var i = 0; i < 6; i++) {
            PiiMemb[imembers][i] = [];
            for (var j = 0; j < 6; j++){
                PiiMemb[imembers][i][j] = 0;
            }
        }
        //vecteur u1
        PiiMemb[imembers][0][0] = (XjMemb[imembers] - XiMemb[imembers]) / LijMemb[imembers];
        PiiMemb[imembers][0][1] = (YjMemb[imembers] - YiMemb[imembers]) / LijMemb[imembers];
        PiiMemb[imembers][0][2] = (ZjMemb[imembers] - ZiMemb[imembers]) / LijMemb[imembers];
        //vecteur u2
        if (Math.abs(PiiMemb[imembers][0][2]) == 1){
            PiiMemb[imembers][1][0] = 0;
            PiiMemb[imembers][1][1] = 1 * PiiMemb[imembers][0][2];
            PiiMemb[imembers][1][2] = 0;
        }
        else{
            PiiMemb[imembers][1][0] = -PiiMemb[imembers][0][1] / Math.pow((Math.pow(PiiMemb[imembers][0][0], 2) + Math.pow(PiiMemb[imembers][0][1], 2)), 0.5);
            PiiMemb[imembers][1][1] = PiiMemb[imembers][0][0] / Math.pow((Math.pow(PiiMemb[imembers][0][0], 2) + Math.pow(PiiMemb[imembers][0][1], 2)), 0.5);
            PiiMemb[imembers][1][2] = 0;
        }
        //vecteur u3
        var ProdVect = PiiMemb[imembers][0][0] * PiiMemb[imembers][1][0] + PiiMemb[imembers][0][1] * PiiMemb[imembers][1][1] + PiiMemb[imembers][0][2] * PiiMemb[imembers][1][2];
        PiiMemb[imembers][2][0] = (PiiMemb[imembers][0][1] * PiiMemb[imembers][1][2] - PiiMemb[imembers][0][2] * PiiMemb[imembers][1][1]) / Math.pow(1 - Math.pow(ProdVect, 2), 0.5);
        PiiMemb[imembers][2][1] = (PiiMemb[imembers][0][2] * PiiMemb[imembers][1][0] - PiiMemb[imembers][0][0] * PiiMemb[imembers][1][2]) / Math.pow(1 - Math.pow(ProdVect, 2), 0.5);
        PiiMemb[imembers][2][2] = (PiiMemb[imembers][0][0] * PiiMemb[imembers][1][1] - PiiMemb[imembers][0][1] * PiiMemb[imembers][1][0]) / Math.pow(1 - Math.pow(ProdVect, 2), 0.5);
        for (var i = 0; i < 3; i++){
            for (var j = 0; j < 3; j++){
                PiiMemb[imembers][i + 3][j + 3] = PiiMemb[imembers][i][j];
            }
        }
        //5.2- Construction de la matrice [Pji]
        for (var i = 0; i < 6; i++){
            PjiMemb[imembers][i] = [];
            for (var j = 0; j < 6; j++){
                PjiMemb[imembers][i][j] = 0;
            }
        }
        //5.3- Construction de la matrice [Pij]
        for (var i = 0; i < 6; i++){
            PijMemb[imembers][i] = [];
            for (var j = 0; j < 6; j++){
                PijMemb[imembers][i][j] = 0;
            }
        }
        //5.4- Construction de la matrice [Pjj]
        for (var i = 0; i < 6; i++){
            PjjMemb[imembers][i] = [];
            for (var j = 0; j < 6; j++){
                PjjMemb[imembers][i][j] = PiiMemb[imembers][i][j];
            }
        }
        //5.5- Construction de la matrice [PMemb] représentant la matrice 12x12 de changement de base de la barre dans le repère globale
        for (var i = 0; i < 6; i++){
            PMemb[imembers][i] = [];
            PMemb[imembers][i+6] = [];
            for (var j = 0; j < 6; j++){
                PMemb[imembers][i][j] = PiiMemb[imembers][i][j];
                PMemb[imembers][i + 6][j] = PjiMemb[imembers][i][j];
                PMemb[imembers][i][j + 6] = PijMemb[imembers][i][j];
                PMemb[imembers][i + 6][j + 6] = PjjMemb[imembers][i][j];
            }
        }
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //6- Construction de la matrice de changement de base [InvP] du repère globale dans le repère locale de la barre
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        for (var i = 0; i < 12; i++){
            InvPMemb[imembers][i] = [];
            for (var j = 0; j < 12; j++){
                InvPMemb[imembers][i][j] = PMemb[imembers][j][i];
            }
        }
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //7- Construction de la matrice de rigidité de la barre [Kg]=[InvP][K][P] dans le repère globale
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        for (var i = 0; i < 12; i++){
            KgMemb1[i] = [];
            KgMemb[imembers][i] = [];
            for (var j = 0; j < 12; j++){
                KgMemb1[i][j] = 0;
                KgMemb[imembers][i][j] = 0;
            }
        }
        for (var i = 0; i < 12; i++){
            for (var j = 0; j < 12; j++){
                for (var k = 0; k < 12; k++){
                    KgMemb1[i][j] = KgMemb1[i][j] + KMemb[i][k] * PMemb[imembers][k][j];
                }
            }
        }
        for (var i = 0; i < 12; i++){
            for (var j = 0; j < 12; j++){
                for (var k = 0; k < 12; k++){
                    KgMemb[imembers][i][j] = KgMemb[imembers][i][j] + InvPMemb[imembers][i][k] * KgMemb1[k][j];
                }
            }
        }
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //7- Construction de la matrice de rigidité de la barre [Kg]=[InvP][K][P] dans le repère globale
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        for(var i = 0; i < 6 * kNodesMemb; i++){
            Ktotal[i] = [];
        }
        
        for (var i = 0; i < 6; i++){
            for (var j = 0; j < 6; j++){
                Ktotal[MembNodei * 6 + i][MembNodei * 6 + j] = Ktotal[MembNodei * 6 + i][MembNodei * 6 + j] + KgMemb[imembers][i][j];
            }
        }
        for (var i = 0; i < 6; i++){
            for (var j = 0; j < 6; j++){
                Ktotal[MembNodej * 6 + i][MembNodei * 6 + j] = Ktotal[MembNodej * 6 + i][MembNodei * 6 + j] + KgMemb[imembers][i + 6][j];
            }
        }
        for (var i = 0; i < 6; i++){
            for (var j = 0; j < 6; j++){
                Ktotal[MembNodei * 6 + i][MembNodej * 6 + j] = Ktotal[MembNodei * 6 + i][MembNodej * 6 + j] + KgMemb[imembers][i][j + 6];
            }
        }
        for (var i = 0; i < 6; i++){
            for (var j = 0; j < 6; j++){
                Ktotal[MembNodej * 6 + i][MembNodej * 6 + j] = Ktotal[MembNodej * 6 + i][MembNodej * 6 + j] + KgMemb[imembers][i + 6][j + 6];
            }
        }

    }
    if (structure3danalysismessage[2] == ""){
        structure3danalysismessage[2] = "Toutes les barres ont des caractéristiques de matériaux associés";
    }
    if (structure3danalysismessage[3] == ""){
        structure3danalysismessage[3] = "Toutes les barres ont des caractéristiques de sections associés";
    }
    if (structure3danalysismessage[4] == ""){
        structure3danalysismessage[4] = "Toutes les barres ont des noeuds associés";
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++               REAMENAGEMENT DE LA MATRICE DE RIGIDITE SUIVANT LES CONDITIONS LIMITES AUX NOEUDS                 ++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //Réarrangement de la matrice de rigidité en fonction des efforts et des déplacements connus aux noeuds
    //Résolution du sytème suivant:
    //[Fconnu]=[A][Uinconnu]+[B][Uconnu] => on en déduit [Uinconnu]
    //[Finconnu]=[tB][Uinconnu]+[C][Uconnu] => on en déduit [Finconnu]

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //Rérrangement des vecteurs force [F] et déplacement [U] en fonction des inconnues:
    //1- Création du vecteur force [F] dans le repère globale
    //   1.1- Charges appliquées aux noeuds
    //   1.2- Charges appliquées aux barres
    //       1.2.1- Création de la matrice de changement de base [PiiMemb]entre le repère globale et le repère locale
    //       1.2.2- Création des vecteur colonne de chargement [PaMembLoadsG],[PbMembLoadsG] dans le repère globale
    //       1.2.3- Création des vecteur colonne de chargement [PaMembLoads],[PbMembLoads] dans le repère locale
    //       1.2.4- Calcul des efforts ramenés aux noeuds [FMembNodeiLoads] et [FMembNodejLoads] dans le repère locale
    //       1.2.5- Calcul des efforts ramenés aux noeuds [FMembNodeiLoadsG] et [FMembNodejLoadsG] dans le repère globale
    //2- Création du vecteur déplacement [U] dans le repère globale
    //3- Réarrangement dans [Uinconnu], [Uconnu] et dans [Fconnu]
    //4- Création de [A],[B],[tB],[C]
    //5- Inversion de [A]=>[InvA]
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //1- Création du vecteur force [F]
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    for (inodes = 0; inodes < 6 * kNodesMemb; inodes++){
        Fi[inodes] = 0;
        Fiext[inodes] = 0;
    }
    //  1.1- Charges appliquées aux noeuds
    structure3danalysismessage[5] = "";
    for (inloads = 0; inloads < knloads; inloads++){
        inodes = 0;
        do{
            if (NodesLoads[inloads] === Nodes[inodes]){
                Fi[inodes * 6] = Fi[inodes * 6] + FXNodesLoads[inloads];
                Fi[inodes * 6 + 1] = Fi[inodes * 6 + 1] + FYNodesLoads[inloads];
                Fi[inodes * 6 + 2] = Fi[inodes * 6 + 2] + FZNodesLoads[inloads];
                Fi[inodes * 6 + 3] = Fi[inodes * 6 + 3] + MXNodesLoads[inloads];
                Fi[inodes * 6 + 4] = Fi[inodes * 6 + 4] + MYNodesLoads[inloads];
                Fi[inodes * 6 + 5] = Fi[inodes * 6 + 5] + MZNodesLoads[inloads];
                break;
            }
            else{
                if (inodes === knodes - 1){
                    structure3danalysismessage[5] = "Efforts appliqués sur un noeud non défini";
                    return structure3danalysismessage;
                }
            }
            inodes++;
        } while (inodes < knodes);
    }
    if (structure3danalysismessage[5] == ""){
        structure3danalysismessage[5] = "Tous les efforts sont appliqués à des noeuds définis";
    }
























    structure3danalysismessage[7] = "OK";

    return(structure3danalysismessage);
}

module.exports = structure3danalysis;