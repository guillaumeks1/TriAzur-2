﻿var MemberLoadsDirectives = angular.module('MemberLoadsDirectives', ['MemberLoadsServices'])

.directive('trMemberloadslist', function (MemberLoadsService) {
    return {
        restrict: 'A',
        templateUrl: '/Scripts/AngularJs/Structure/Structure3D/Templates/MemberLoads/ListMemberLoadTemplate.html',
        controller: ['$scope', function trMemberLoadslistCtrl($scope) {
            //memberloadloads window control
            $('#windowmemberloads').click(function () {
                $('#inputsectioncard').toggleClass("col-md-12");
                $('#outputsectioncard').toggle();
                $('#windownodes').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
                $('#windowmembers').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
                $('#windownodeloads').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
                $('#windowmemberloads').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
                $('#windownodedisplacements').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
            });

            //In order to control the visibility of tr-memberloadslist
            $scope.memberloadslistvisibility = true;

            //List of LoadType
            $scope.listloadtypes = [
                { Name: 'Ponctual', Type: 'Ponctual' },
                { Name: 'Distributed', Type: 'Distributed' }
            ];

            //List of LoadDirection
            $scope.listloaddirections = [
                { Name: 'X', Direction: 'X' },
                { Name: 'Y', Direction: 'Y' },
                { Name: 'Z', Direction: 'Z' }
            ];

            //List of memberloads
            $scope.listmemberloads = [];

            $scope.Listmemberload = function (id) {
                MemberLoadsService.GetListMemberLoads().then(function (d) {
                    $scope.listmemberloads = d.data;
                    $scope.MemberLoad = $scope.listmemberloads[id];
                }, function (error) {
                    $scope.ListMemberLoadsmessage = "Error";
                });
            }
            $scope.Listmemberload(0);

            //Edit memberload
            $scope.posteditmemberload = function (ddata) {
                $scope.ListMemberLoadsmessage = "";
                MemberLoadsService.PostEditMemberLoads(ddata).then(function (d) {
                    $scope.ListMemberLoadsmessage = d;
                    if (d == "Success") {
                        $scope.Listmemberload(0);
                        $scope.ListMemberLoadsmessage = d;
                    }
                }, function (error) {
                    $scope.ListMemberLoadsmessage = 'Error MemberLoad!';
                });
            };

            //Create memberload
            $scope.postaddmemberload = function () {
                $scope.ListMemberLoadsmessage = "";
                $scope.Listmemberload(0);
                var n;
                if ($scope.listmemberloads.length <= 0) {
                    n = 1;
                } else {
                    n = $scope.listmemberloads[$scope.listmemberloads.length-1].MemberLoadId + 1;
                }
                $scope.newmemberload = {
                    MemberId: 6,
                    Type: 'Ponctual',
                    Direction: 'X',
                    DistanceA: 0,
                    LoadPA: 1000,
                    DistanceB: 0,
                    LoadPB: 1000
                };
                MemberLoadsService.PostAddMemberLoads($scope.newmemberload).then(function (d) {
                    $scope.ListMemberLoadsmessage = d;
                    if (d === "Success") {
                        $scope.Listmemberload(0);
                        $scope.ListMemberLoadsmessage = "";
                    }
                }, function (error) {
                    $scope.ListMemberLoadsmessage = "Error MemberLoad!";
                });
            };


            //Delete memberload
            $scope.postdeletememberload = function (data) {
                MemberLoadsService.PostDeleteMemberLoads(data).then(function (d) {
                    if (d === "Success") {
                        $scope.ListMemberLoadsmessage = "";
                        $scope.Listmemberload(0);
                    } else {
                        $scope.ListMemberLoadsmessage = d;
                    }   
                }, function (error) {
                    $scope.ListMemberLoadsmessage = "Error";
                });
            };

        }]
    };
})

