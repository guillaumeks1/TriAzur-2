﻿var MemberLoadsServices = angular.module('MemberLoadsServices', [])

.factory('MemberLoadsService', function ($http, $q) {
    var fac = {};
    fac.GetListMemberLoads = function () {
        return $http.get('/MemberLoadsData/ListMemberLoads');
    }
    fac.GetDetailMemberLoads = function (data) {
        return $http.get('/MemberLoadsData/DetailsMemberLoads', { params: data });
    }
    fac.PostEditMemberLoads = function (data) {
        var defer = $q.defer();
        $http({
            url: '/MemberLoadsData/EditMemberLoads',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        }).success(function (d) {
            defer.resolve(d);
        }).error(function (e) {
            alert('Error!');
            defer.reject(e);
        });
        return defer.promise;
    }
    fac.PostAddMemberLoads = function (data) {
        var defer = $q.defer();
        $http({
            url: '/MemberLoadsData/AddMemberLoads',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        }).success(function (d) {
            defer.resolve(d);
        }).error(function (e) {
            alert('Error!');
            defer.reject(e);
        });
        return defer.promise;
    }
    fac.PostDeleteMemberLoads = function (data) {
        var defer = $q.defer();
        $http({
            url: '/MemberLoadsData/DeleteMemberLoads',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        }).success(function (d) {
            defer.resolve(d);
        }).error(function (e) {
            alert('Error!');
            defer.reject(e);
        });
        return defer.promise;
    }
    return fac;

})

