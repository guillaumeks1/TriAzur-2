﻿var MembersDirectives = angular.module('MembersDirectives', ['MembersServices'])

.directive('trMemberslist', function (MembersService) {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/structure3D/templates/members/ListMemberTemplate.html',
        controller: ['$scope', function trMemberslistCtrl($scope) {
            //members window control
            $('#windowmembers').click(function () {
                $('#inputsectioncard').toggleClass("col-md-12");
                $('#outputsectioncard').toggle();
                $('#windownodes').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
                $('#windowmembers').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
                $('#windownodeloads').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
                $('#windownmemberloads').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
                $('#windownodedisplacements').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
            });

            //In order to control the visibility of tr-memberslist
            $scope.memberslistvisibility = true;

            //List of members
            $scope.listmembers = [];

            $scope.Listmember = function (id) {
                MembersService.GetListMembers().then(function (d) {
                    $scope.listmembers = d.data;
                    $scope.Member = $scope.listmembers[id];
                }, function (error) {
                    $scope.ListMembersmessage = "Error";
                });
            }
            $scope.Listmember(0);

            //Edit member
            $scope.posteditmember = function (ddata) {
                $scope.ListMembersmessage = "";
                MembersService.PostEditMembers(ddata).then(function (d) {
                    $scope.ListMembersmessage = d.data;
                    if (d.data == "Success") {
                        $scope.Listmember(0);
                        $scope.ListMembersmessage = d.data;
                    }
                }, function (error) {
                    $scope.ListMembersmessage = 'Error Member!';
                });
            };

            //Edit list of members
            $scope.posteditlistmembers = function(){
                $scope.ListMembersmessage = "";
                MembersService.PostEditListMembers($scope.listmembers).then(function (d) {
                    $scope.ListMembersmessage = d.data;
                    if (d.data == "Success") {
                        $scope.Listmember(0);
                        $scope.ListMembersmessage = d.data;
                    }
                }, function (error) {
                    $scope.ListMembersmessage = 'Error Member!';
                });
            }
            
            //Create member
            $scope.postaddmember = function () {
                $scope.ListMembersmessage = "";
                $scope.Listmember(0);
                $scope.Listnode(0);
                $scope.Listsection(0);
                var n;
                if ($scope.listmembers.length <= 1) {
                    n = 1;
                } else {
                    n = $scope.listmembers[$scope.listmembers.length-1].MemberId + 1;
                }
                $scope.newmember = {
                    Name: "Member " + n,
                    NodeiId: $scope.listnodes[0].NodeId,
                    NodejId: $scope.listnodes[0].NodeId,
                    SectionId: $scope.listsections[0].SectionId
                };
                MembersService.PostAddMembers($scope.newmember).then(function (d) {
                    $scope.ListMembersmessage = d.data;
                    if (d.data === "Success") {
                        $scope.Listmember(0);
                        $scope.ListMembersmessage = "";
                    }
                }, function (error) {
                    $scope.ListMembersmessage = "Error Member!";
                });
            };


            //Delete member
            $scope.postdeletemember = function (memdata) {
                MembersService.PostDeleteMembers(memdata).then(function (d) {
                    if (d.data === "Success") {
                        $scope.ListMembersmessage = "";
                        $scope.Listmember(0);
                    } else {
                        $scope.ListMembersmessage = d.data;
                    }   
                }, function (error) {
                    $scope.ListMembersmessage = "Error";
                });
            };

        }]
    };
})

