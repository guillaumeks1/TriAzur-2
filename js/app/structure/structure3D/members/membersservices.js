﻿var MembersServices = angular.module('MembersServices', [])

.factory('MembersService', function ($http, $q) {
    var fac = {};
    fac.GetListMembers = function () {
        return $http.get('/membersdata/listmembers');
    }
    fac.GetDetailMembers = function (data) {
        return $http.get('/membersdata/detailsmembers', { params: data });
    }

    fac.PostEditMembers = function (data) {
        return $http({
            url: '/membersdata/editmembers',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.PostEditListMembers = function (data) {
        return $http({
            url: '/membersdata/editlistmembers',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.PostAddMembers = function (data) {
        return $http({
            url: '/membersdata/addmembers',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.PostDeleteMembers = function (data) {
        return $http({
            url: '/membersdata/deletemembers',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        });
    }




    // fac.PostEditMembers = function (data) {
    //     var defer = $q.defer();
    //     $http({
    //         url: '/MembersData/EditMembers',
    //         method: 'POST',
    //         data: JSON.stringify(data),
    //         headers: { 'content-type': 'application/json' }
    //     }).success(function (d) {
    //         defer.resolve(d);
    //     }).error(function (e) {
    //         alert('Error!');
    //         defer.reject(e);
    //     });
    //     return defer.promise;
    // }
    // fac.PostAddMembers = function (data) {
    //     var defer = $q.defer();
    //     $http({
    //         url: '/MembersData/AddMembers',
    //         method: 'POST',
    //         data: JSON.stringify(data),
    //         headers: { 'content-type': 'application/json' }
    //     }).success(function (d) {
    //         defer.resolve(d);
    //     }).error(function (e) {
    //         alert('Error!');
    //         defer.reject(e);
    //     });
    //     return defer.promise;
    // }
    // fac.PostDeleteMembers = function (data) {
    //     var defer = $q.defer();
    //     $http({
    //         url: '/MembersData/DeleteMembers',
    //         method: 'POST',
    //         data: JSON.stringify(data),
    //         headers: { 'content-type': 'application/json' }
    //     }).success(function (d) {
    //         defer.resolve(d);
    //     }).error(function (e) {
    //         alert('Error!');
    //         defer.reject(e);
    //     });
    //     return defer.promise;
    // }
    return fac;

})

