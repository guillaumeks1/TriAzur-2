﻿var NodesDirectives = angular.module('NodesDirectives', ['NodesServices'])

.directive('trNodeslist', function (NodesService) {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/structure3D/templates/nodes/ListNodeTemplate.html',
        controller: ['$scope', function trNodeslistCtrl($scope) {
            //nodes window control
            $('#windownodes').click(function () {
                $('#inputsectioncard').toggleClass("col-md-12");
                $('#outputsectioncard').toggle();
                $('#windownodes').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
                $('#windowmembers').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
                $('#windownodeloads').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
                $('#windownmemberloads').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
                $('#windownodedisplacements').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
            });

            //In order to control the visibility of tr-nodeslist
            $scope.nodeslistvisibility = true;

            //List of nodes
            $scope.listnodes = [];

            $scope.Listnode = function (id) {
                NodesService.GetListNodes().then(function (d) {
                    $scope.listnodes = d.data;
                    $scope.Node = $scope.listnodes[id];
                }, function (error) {
                    $scope.ListNodesmessage = "Error";
                });
            }
            $scope.Listnode(0);

            //Edit node
            $scope.posteditnode = function (ddata) {
                $scope.ListNodesmessage = "";
                NodesService.PostEditNodes(ddata).then(function (d) {
                    $scope.ListNodesmessage = d.data;
                    if (d.data == "Success") {
                        $scope.Listnode(0);
                        $scope.ListNodesmessage = d.data;
                    }
                }, function (error) {
                    $scope.ListNodesmessage = 'Error Node!';
                });
            };

            //Edit listnodes
            $scope.posteditlistnodes = function(){
                $scope.ListNodesmessage = "";
                NodesService.PostEditListNodes($scope.listnodes).then(function (d) {
                    $scope.ListNodesmessage = d.data;
                    //$scope.Listnode(0);
                    if (d.data == "Success") {
                        $scope.Listnode(0);
                        $scope.ListNodesmessage = d.data;
                    }
                }, function (error) {
                    $scope.ListNodesmessage = 'Error Node!';
                });
            };

            //Create node
            $scope.postaddnode = function () {
                $scope.ListNodesmessage = "";
                $scope.Listnode(0);
                NodesService.PostAddNodes().then(function (d) {
                    $scope.ListNodesmessage = d.data;
                    if (d.data === "Success") {
                        $scope.Listnode(0);
                        $scope.ListNodesmessage = "";
                    }
                }, function (error) {
                    $scope.ListNodesmessage = "Error Node!";
                });
            };


            //Delete node
            $scope.postdeletenode = function (noddata) {
                NodesService.PostDeleteNodes(noddata).then(function (d) {
                    if (d.data === "Success") {
                        $scope.ListNodesmessage = "";
                        $scope.Listnode(0);
                    } else {
                        $scope.ListNodesmessage = d.data;
                    }   
                }, function (error) {
                    $scope.ListNodesmessage = "Error";
                });
            };

        }]
    };
})

