﻿var NodesServices = angular.module('NodesServices', [])

.factory('NodesService', function ($http, $q) {
    var fac = {};
    fac.GetListNodes = function () {
        return $http.get('/nodesdata/listnodes');
    }
    fac.GetDetailNodes = function (data) {
        return $http.get('/nodesdata/detailsnodes', { params: data });
    }
    fac.PostEditNodes = function (data) {
        return $http({
            url: '/nodesdata/editnodes',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.PostEditListNodes = function (data) {
        return $http({
            url: '/nodesdata/editlistnodes',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        });
    }

    fac.PostAddNodes = function (data) {
        return $http({
            url: '/nodesdata/addnodes',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.PostDeleteNodes = function (data) {
        return $http({
            url: '/nodesdata/deletenodes',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        });
    }






    // fac.PostEditNodes = function (data) {
    //     var defer = $q.defer();
    //     $http({
    //         url: '/NodesData/EditNodes',
    //         method: 'POST',
    //         data: JSON.stringify(data),
    //         headers: { 'content-type': 'application/json' }
    //     }).success(function (d) {
    //         defer.resolve(d);
    //     }).error(function (e) {
    //         alert('Error!');
    //         defer.reject(e);
    //     });
    //     return defer.promise;
    // }
    // fac.PostEditListNodes = function (data) {
    //     var defer = $q.defer();
    //     $http({
    //         url: '/NodesData/EditListNodes',
    //         method: 'POST',
    //         data: JSON.stringify(data),
    //         headers: { 'content-type': 'application/json' }
    //     }).success(function (d) {
    //         defer.resolve(d);
    //     }).error(function (e) {
    //         alert('Error!');
    //         defer.reject(e);
    //     });
    //     return defer.promise;
    // }
    // fac.PostAddNodes = function (data) {
    //     var defer = $q.defer();
    //     $http({
    //         url: '/NodesData/AddNodes',
    //         method: 'POST',
    //         data: JSON.stringify(data),
    //         headers: { 'content-type': 'application/json' }
    //     }).success(function (d) {
    //         defer.resolve(d);
    //     }).error(function (e) {
    //         alert('Error!');
    //         defer.reject(e);
    //     });
    //     return defer.promise;
    // }
    // fac.PostDeleteNodes = function (data) {
    //     var defer = $q.defer();
    //     $http({
    //         url: '/NodesData/DeleteNodes',
    //         method: 'POST',
    //         data: JSON.stringify(data),
    //         headers: { 'content-type': 'application/json' }
    //     }).success(function (d) {
    //         defer.resolve(d);
    //     }).error(function (e) {
    //         alert('Error!');
    //         defer.reject(e);
    //     });
    //     return defer.promise;
    // }
    return fac;

})

