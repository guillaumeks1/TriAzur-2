﻿var SectionsServices = angular.module('SectionsServices', [])

.factory('SectionsService', function ($http, $q) {
    var fac = {};
    fac.GetListSections = function () {
        return $http.get('/sectionsdata/listsections');
    }
    fac.GetDataSections = function (data) {
        return $http.get('/sectionsdata/datasections', { params: data });
    }
    fac.PostEditSections = function (data) {
        return $http({
            url: '/sectionsdata/editsections',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.PostAddSections = function (data) {
        return $http({
            url: '/sectionsdata/addsections',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.PostDeleteSections = function (data) {
        return $http({
            url: '/sectionsdata/deletesections',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        });
    }
    //-------------------------------------------------------------
    //Section Geometry
    //-------------------------------------------------------------
    fac.RectangularSection = function(b, h, unitb, unith){
        return ({
            type: 'scatter',
            data: {
                datasets: [{
                    label: 'Rectangular Section',
                    data: [
                        {x: 0,y: 0}, 
                        {x: b,y: 0},
                        {x: b,y: h},
                        {x: 0,y: h},
                        {x: 0,y: 0}
                    ],
                    backgroundColor: [
                        'rgba(112,112,112, 0.2)'
                    ],
                    borderColor: [
                        'rgba(112,112,112,1)'
                    ],
                    borderWidth: 3,
                    lineTension: 0
                }]
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        type: 'linear',
                        position: 'bottom',
                        ticks:{
                            min:0,
                            max: Math.max(b,h),
                            stepSize : b
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'x(' + unitb + ')'
                        }
                    }],
                    yAxes: [{
                        type: 'linear',
                        position: 'bottom',
                        ticks:{
                            min:0,
                            max: Math.max(b,h), 
                            stepSize : h
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'y(' + unith + ')'
                        }
                    }],
                }
            }
        }); 
    }
    fac.TSection = function(b, h, b0, h0, unitb, unith, unitb0, unith0){
        return ({
            type: 'scatter',
            data: {
                datasets: [{
                    label: 'T Section',
                    data: [
                        {x: 0,y: h}, 
                        {x: 0,y: h-h0},
                        {x: (b-b0)/2,y: h-h0},
                        {x: (b-b0)/2,y: 0},
                        {x: (b+b0)/2,y: 0},
                        {x: (b+b0)/2,y: h-h0},
                        {x: b,y: h-h0},
                        {x: b,y: h},
                        {x: 0,y: h},
                    ],
                    backgroundColor: [
                        'rgba(112,112,112, 0.2)'
                    ],
                    borderColor: [
                        'rgba(112,112,112,1)'
                    ],
                    borderWidth: 3,
                    lineTension: 0
                }]
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        type: 'linear',
                        position: 'bottom',
                        ticks:{
                            min:0,
                            max: Math.max(b,h),
                            stepSize : b
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'x(' + unitb + ')'
                        }
                    }],
                    yAxes: [{
                        type: 'linear',
                        position: 'bottom',
                        ticks:{
                            min:0,
                            max: Math.max(b,h),
                            stepSize : h
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'y(' + unith + ')'
                        }
                    }],
                }
            }
        }); 
    }
    return fac;

})

