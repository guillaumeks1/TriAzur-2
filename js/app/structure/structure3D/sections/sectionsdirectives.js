﻿var SectionsDirectives = angular.module('SectionsDirectives', ['SectionsServices'])

.directive('trSectionslist', function (SectionsService) {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/structure3D/templates/sections/ListSectionTemplate.html',
        controller: ['$scope', function trSectionslistCtrl($scope) {
            //In order to control the visibility of tr-sectionslist
            $scope.sectionslistvisibility = true;

            //List of sections
            $scope.listsections = [];

            $scope.Listsection = function (id) {
                SectionsService.GetListSections().then(function (d) {
                    $scope.listsections = d.data;
                    $scope.Section = $scope.listsections[id];
                    $scope.ListSectionsmessage = 'Ok';
                }, function (error) {
                    $scope.ListSectionsmessage = "Error";
                });
            }
            $scope.Listsection(0);

            // //Detail section
            // $scope.Detailsection = function (secdata) {
            //     SectionsService.GetDetailSections(secdata).then(function (d) {
            //         $scope.Section = d.data;
            //     }, function (error) {
            //         $scope.ListSectionsmessage = "Error";
            //     });
            // }

            //Edit section
            $scope.gotoeditsection = function (secdata) {
                //$scope.Detailsection(secdata);
                $scope.EditSectionsmessage = "";
                $scope.sectionslistvisibility = false;
                $scope.sectionseditvisibility = true;
            };

            //Create section
            $scope.gotoaddsection = function (secdata) {
                //$scope.Detailsection(secdata);
                $scope.AddSectionsmessage = "";
                $scope.sectionslistvisibility = false;
                $scope.sectionsaddvisibility = true;
            };

            //Delete section
            $scope.postdeletesection = function (data) {
                SectionsService.PostDeleteSections(data).then(function (d) {
                    if (d.data == "Success") {
                        $scope.ListSectionsmessage = "";
                        $scope.Listsection(0);
                    } else {
                        $scope.ListSectionsmessage = d.data;
                    }   
                }, function (error) {
                    $scope.ListSectionsmessage = "Error";
                });
            };
        }]
    };
})

.directive('trSectionsedit', function (SectionsService) {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/structure3D/templates/sections/EditSectionTemplate.html',
        controller: ['$scope', function trSectionseditCtrl($scope) {

            //Back to List of section
            $scope.editgotolistsection = function () {
                $scope.Listsection(0);
                $scope.sectionseditvisibility = false;
                $scope.sectionslistvisibility = true;
                $scope.EditSectionsButtonText = "Edit";
                $scope.EditSectionsmessage = "";
            };

            //Edit section
            $scope.EditSectionsButtonText = "Edit";
            $scope.EditSectionsmessage = "";

            $scope.editsection = function (data) {
                $scope.EditSectionsButtonText = "Save..";
                SectionsService.PostEditSections(data).then(function (d) {
                    $scope.EditSectionsmessage = d.data;
                    $scope.EditSectionsButtonText = "Edit";
                    if (d.data == "Success") {
                        $scope.Listsection(0);
                        $scope.sectionseditvisibility = false;
                        $scope.sectionslistvisibility = true;
                        $scope.EditSectionsmessage = "";
                    }
                }, function (error) {
                    $scope.EditSectionsmessage = 'Error Section!';
                });
            };
        }]
    };
})

.directive('trSectionsadd', function (SectionsService) {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/structure3D/templates/sections/AddSectionTemplate.html',
        controller: ['$scope', function trSectionsaddCtrl($scope) {
            //Back to List of section
            $scope.addgotolistsection = function () {
                $scope.Listsection(0);
                $scope.sectionsaddvisibility = false;
                $scope.sectionslistvisibility = true
                $scope.AddSectionsButtonText = "Add";
                $scope.AddSectionsmessage = "";
            };

            $scope.srcimageconcretesection = [
                "/Img/Structure/ConcreteStructure/ReinforcedStructure/rectangularconcretesection.svg",
                "/Img/Structure/ConcreteStructure/ReinforcedStructure/Tconcretesection.svg",
                "/Img/Structure/ConcreteStructure/ReinforcedStructure/Iconcretesection.svg",
                "/Img/Structure/ConcreteStructure/ReinforcedStructure/circleconcretesection.svg",
                "/Img/Structure/ConcreteStructure/ReinforcedStructure/hollowcircleconcretesection.svg",
                "/Img/Structure/ConcreteStructure/ReinforcedStructure/randomconcretesection.svg"
            ];

            $scope.rectangularsectiongeometry = function () {
                $scope.sectionsaddvisibility = false;
                $scope.rectangularsectionvisibility = true;
            };

            $scope.tsectiongeometry = function () {
                $scope.sectionsaddvisibility = false;
                $scope.tsectionvisibility = true;
            };
        }]
    };
}) 

.directive('trDatasection', function(SectionsService){
    return{
        restrict: 'A',
        templateUrl: '/js/app/structure/structure3D/templates/sections/DataSectionTemplate.html',
        controller: ['$scope', function trDatasectionCtrl($scope) {
            $scope.backsectionsaddvisibility = function () {
                $scope.sectionsdatavisibility = false;
                $scope.sectionslistvisibility = true;
            };

            //Add section
            $scope.AddSectionsButtonText = "Add";
            $scope.AddSectionsmessage = "";

            $scope.addsection = function (data) {
                $scope.AddSectionsButtonText = "Save..";
                SectionsService.PostAddSections(data).then(function (d) {
                    $scope.AddSectionsmessage = d.data;
                    $scope.AddSectionsButtonText = "Add";
                    if (d.data == "Success") {
                        $scope.Listsection(0);
                        $scope.sectionsdatavisibility = false;
                        $scope.sectionslistvisibility = true;
                    }

                }, function (error) {
                    $scope.AddSectionsmessage = "Error Section!";
                });
            };

        }]
    }
})

.directive('trRectangularsection', function(SectionsService){
    return{
        restrict: 'A',
        templateUrl: '/js/app/structure/structure3D/templates/sections/RecSectionTemplate.html',
        controller: ['$scope', function trRectangularsectionCtrl($scope) {

            //---------------------------------------------------------------------------------------------
            //Commands Control
            //---------------------------------------------------------------------------------------------
            $scope.rectangularsectionback = function () {
                $scope.rectangularsectionvisibility = false;
                $scope.sectionsaddvisibility = true;

            };

            $scope.RecDatasection = function () {
                SectionsService.GetDataSections($scope.RectSection).then(function (d) {
                    $scope.Recvalidationmessage = d.data.message;
                    if(d.data.message==false){
                        $scope.Section.Xext = $scope.RectSection.Xext.map(Number);
                        $scope.Section.Yext = $scope.RectSection.Yext.map(Number);
                        $scope.Section.Xint = $scope.RectSection.Xint.map(Number);
                        $scope.Section.Yint = $scope.RectSection.Yint.map(Number);
                        $scope.Section.Area = Math.round(d.data.section[1]*1000)/1000;
                        $scope.Section.I2 = Math.round(d.data.section[3]*1000)/1000;
                        $scope.Section.I3 = Math.round(d.data.section[2]*1000)/1000;
                        $scope.Section.J = Math.round(d.data.section[4]*1000)/1000;
                        $scope.Section.XG = Math.round(d.data.section[5]*1000)/1000;
                        $scope.Section.YG = Math.round(d.data.section[6]*1000)/1000;

                        $scope.rectangularsectionvisibility = false;
                        $scope.sectionsdatavisibility = true;
                    }
                }, function (error) {
                    $scope.ListSectionsmessage = "Error";
                });
            };

            //---------------------------------------------------------------------------------------------
            //Units Control
            //---------------------------------------------------------------------------------------------
            $scope.bwUnit = $scope.UnitLengthOptions[0];
            $scope.hUnit = $scope.UnitLengthOptions[0];
            
            //---------------------------------------------------------------------------------------------
            //Rectangular Section Width
            //---------------------------------------------------------------------------------------------
            $scope.bwAff = 1;
         
            $scope.bw = function () {
                return ($scope.bwAff * $scope.conversionUnit[$scope.bwUnit.value]['m']);
            };
            $scope.$watch('bwUnit', function (newValue, oldValue) {
                $scope.bwAff = Math.round($scope.bwAff * $scope.conversionUnit[oldValue.value][newValue.value]*1000)/1000;
            });

            //---------------------------------------------------------------------------------------------
            //Rectangular Section Height
            //---------------------------------------------------------------------------------------------
            $scope.hAff = 1;

            $scope.h = function () {
                return ($scope.hAff * $scope.conversionUnit[$scope.hUnit.value]['m']);
            };
            $scope.$watch('hUnit', function (newValue, oldValue) {
                $scope.hAff = Math.round($scope.hAff * $scope.conversionUnit[oldValue.value][newValue.value] * 1000) / 1000;
            });

            $scope.RectSection = {
                Xext : [0,  $scope.bw(),  $scope.bw(), 0, 0], 
                Yext : [0, 0, $scope.h(), $scope.h(), 0], 
                Xint : [0, 0, 0, 0, 0], 
                Yint : [0, 0, 0, 0, 0]
            };

            $scope.$watch('bwAff + hAff', function (newvalue, oldvalue) {
                //if ($scope.isStrictPositive($scope.bwAff) && $scope.isStrictPositive($scope.hAff)) {
                    $scope.RectSection = {
                        Xext : [0,  $scope.bw(),  $scope.bw(), 0, 0], 
                        Yext : [0, 0, $scope.h(), $scope.h(), 0], 
                        Xint : [0, 0, 0, 0, 0], 
                        Yint : [0, 0, 0, 0, 0]
                    };
                //}
            });
        }]
    }
})

.directive('trRecsectionchart', function (SectionsService) {
    return {
        restrict: 'A',
        scope: {
            sectionwidth: '=',
            sectionheight: '=',
            unitx: '=',
            unity: '='
        },
        link: function link(scope, element, attrs) {
            scope.$watch('sectionwidth + sectionheight + unitx + unity', function () {
                var myChart = new Chart(element[0],SectionsService.RectangularSection(scope.sectionwidth, scope.sectionheight, scope.unitx, scope.unity));
            });    
        }
    };
})

.directive('trTsection', function(SectionsService){
    return{
        restrict: 'A',
        templateUrl: '/js/app/structure/structure3D/templates/sections/TSectionTemplate.html',
        controller: ['$scope', function trTsectionCtrl($scope) {

            //---------------------------------------------------------------------------------------------
            //Commands Control
            //---------------------------------------------------------------------------------------------
            $scope.tsectionback = function () {
                $scope.tsectionvisibility = false;
                $scope.sectionsaddvisibility = true;

            };

            $scope.TDatasection = function () {
                SectionsService.GetDataSections($scope.TSection).then(function (d) {
                   $scope.Section.Xext = $scope.TSection.Xext.map(Number);
                   $scope.Section.Yext = $scope.TSection.Yext.map(Number);
                   $scope.Section.Xint = $scope.TSection.Xint.map(Number);
                   $scope.Section.Yint = $scope.TSection.Yint.map(Number);
                   $scope.Section.Area = Math.round(d.data[1]*1000)/1000;
                   $scope.Section.I2 = Math.round(d.data[3]*1000)/1000;
                   $scope.Section.I3 = Math.round(d.data[2]*1000)/1000;
                   $scope.Section.J = Math.round(d.data[4]*1000)/1000;
                   $scope.Section.XG = Math.round(d.data[5]*1000)/1000;
                   $scope.Section.YG = Math.round(d.data[6]*1000)/1000;                                           
                }, function (error) {
                    $scope.ListSectionsmessage = "Error";
                });
            }

            $scope.tsectiondata = function () {
                $scope.TDatasection();
                $scope.tsectionvisibility = false;
                $scope.sectionsdatavisibility = true;
            };
            //---------------------------------------------------------------------------------------------
            //Units Control
            //---------------------------------------------------------------------------------------------
            $scope.tbUnit = $scope.UnitLengthOptions[0];
            $scope.thUnit = $scope.UnitLengthOptions[0];
            $scope.tb0Unit = $scope.UnitLengthOptions[0];
            $scope.th0Unit = $scope.UnitLengthOptions[0];
            //---------------------------------------------------------------------------------------------
            //T Section Width
            //---------------------------------------------------------------------------------------------
            $scope.tbAff = 1;
         
            $scope.tb = function () {
                return ($scope.tbAff * $scope.conversionUnit[$scope.tbUnit.value]['m']);
            };
            $scope.$watch('tbUnit', function (newValue, oldValue) {
                $scope.tbAff = Math.round($scope.tbAff * $scope.conversionUnit[oldValue.value][newValue.value]*1000)/1000;
            });
            //---------------------------------------------------------------------------------------------
            //T Section Height
            //---------------------------------------------------------------------------------------------
            $scope.thAff = 1;

            $scope.th = function () {
                return ($scope.thAff * $scope.conversionUnit[$scope.thUnit.value]['m']);
            };
            $scope.$watch('thUnit', function (newValue, oldValue) {
                $scope.thAff = Math.round($scope.thAff * $scope.conversionUnit[oldValue.value][newValue.value] * 1000) / 1000;
            });
            //---------------------------------------------------------------------------------------------
            //T Section Small Width
            //---------------------------------------------------------------------------------------------
            $scope.tb0Aff = 0.5;
         
            $scope.tb0 = function () {
                return ($scope.tb0Aff * $scope.conversionUnit[$scope.tb0Unit.value]['m']);
            };
            $scope.$watch('tb0Unit', function (newValue, oldValue) {
                $scope.tb0Aff = Math.round($scope.tb0Aff * $scope.conversionUnit[oldValue.value][newValue.value]*1000)/1000;
            });
            //---------------------------------------------------------------------------------------------
            //T Section Small Height
            //---------------------------------------------------------------------------------------------
            $scope.th0Aff = 0.5;

            $scope.th0 = function () {
                return ($scope.th0Aff * $scope.conversionUnit[$scope.th0Unit.value]['m']);
            };
            $scope.$watch('th0Unit', function (newValue, oldValue) {
                $scope.th0Aff = Math.round($scope.th0Aff * $scope.conversionUnit[oldValue.value][newValue.value] * 1000) / 1000;
            });

            $scope.TSection = {
                Xext : [
                    0, 
                    0, 
                    ($scope.tb()-$scope.tb0())/2,
                    ($scope.tb()-$scope.tb0())/2,
                    ($scope.tb()+$scope.tb0())/2, 
                    ($scope.tb()+$scope.tb0())/2,   
                    $scope.tb(),
                    $scope.tb(),  
                    0
                    ], 
                Yext : [
                    $scope.th(), 
                    $scope.th()-$scope.th0(),  
                    $scope.th()-$scope.th0(),
                    0,
                    0, 
                    $scope.th()-$scope.th0(),   
                    $scope.th()-$scope.th0(),
                    $scope.th(),  
                    $scope.th()
                    ],
                Xint : [0, 0, 0, 0, 0, 0, 0, 0, 0], 
                Yint : [0, 0, 0, 0, 0, 0, 0, 0, 0]
            };

            $scope.$watch('tbAff + thAff + tb0Aff + th0Aff', function (newvalue, oldvalue) {
                //if ($scope.isStrictPositive($scope.bwAff) && $scope.isStrictPositive($scope.hAff)) {
                    $scope.TSection = {
                        Xext : [
                            0, 
                            0, 
                            ($scope.tb()-$scope.tb0())/2,
                            ($scope.tb()-$scope.tb0())/2,
                            ($scope.tb()+$scope.tb0())/2, 
                            ($scope.tb()+$scope.tb0())/2,   
                            $scope.tb(),
                            $scope.tb(),  
                            0
                            ], 
                        Yext : [
                            $scope.th(), 
                            $scope.th()-$scope.th0(),  
                            $scope.th()-$scope.th0(),
                            0,
                            0, 
                            $scope.th()-$scope.th0(),   
                            $scope.th()-$scope.th0(),
                            $scope.th(),  
                            $scope.th()
                            ],
                        Xint : [0, 0, 0, 0, 0, 0, 0, 0, 0], 
                        Yint : [0, 0, 0, 0, 0, 0, 0, 0, 0]
                    };
                //}
            });
        }]
    }
})

.directive('trTsectionchart', function (SectionsService) {
    return {
        restrict: 'A',
        scope: {
            b: '=',
            h: '=',
            b0: '=',
            h0:  '=',
            unitb: '=',
            unith: '=',
            unitb0: '=',
            unith0: '='
        },
        link: function link(scope, element, attrs) {
            scope.$watch('b + h + b0 + h0 + unitb + unith + unitb0 + unith0', function () {
                var TChart = new Chart(element[0],SectionsService.TSection(scope.b, scope.h, scope.b0, scope.h0, scope.unith, scope.unitb, scope.unitb0, scope.unith0));
            });    
        }
    };
})

