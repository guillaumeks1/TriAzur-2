var MaterialsServices = angular.module('MaterialsServices', [])

.factory('MaterialsService', function ($http, $q) {
    var fac = {};
    fac.GetListMaterials = function () {
        return $http.get('/materialsdata/listmaterials');
    }
    fac.GetDetailMaterials = function (data) {
        return $http.get('/materialsdata/detailsmaterials', { params: data });
    }
    fac.PostEditMaterials = function (data) {
        return $http({
            url: '/materialsdata/editmaterials',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.PostAddMaterials = function (data) {
        return $http({
            url: '/materialsdata/addmaterials',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.PostDeleteMaterials = function (data) {
        return $http({
            url: '/materialsdata/deletematerials',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        });
    }
    // fac.PostDeleteMaterials = function (data) {
    //     var defer = $q.defer();
    //     $http({
    //         url: '/materialsdata/deletematerials',
    //         method: 'POST',
    //         data: JSON.stringify(data),
    //         headers: { 'content-type': 'application/json' }
    //     }).success(function (d) {
    //         defer.resolve(d);
    //     }).error(function (e) {
    //         alert('Error!');
    //         defer.reject(e);
    //     });
    //     return defer.promise;
    // }
    return fac;

})

