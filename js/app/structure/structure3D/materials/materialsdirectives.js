var MaterialsDirectives = angular.module('MaterialsDirectives', ['MaterialsServices'])

.directive('trMaterialslist', function (MaterialsService) {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/structure3D/templates/materials/ListMaterialTemplate.html',
        controller: ['$scope', function trMaterialslistCtrl($scope) {
            //materials window control
            $('#windowmaterials').click(function () {
                $('#inputsectioncard').toggleClass("col-md-12");
                $('#outputsectioncard').toggle();
                $('#windowmaterials').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
            });
            //In order to control the visibility of tr-materialslist
            $scope.materialslistvisibility = true;

            //List of materials
            $scope.listmaterials = [];

            $scope.Listmaterial = function (id) {
                MaterialsService.GetListMaterials().then(function (d) {
                    $scope.listmaterials = d.data;
                    $scope.Material = $scope.listmaterials[id];
                    $scope.ListMaterialsmessage = 'Ok';
                }, function (error) {
                    $scope.ListMaterialsmessage = "Error";
                });
            }
            $scope.Listmaterial(0);

            //Detail material
            $scope.Detailmaterial = function (matdata) {
                MaterialsService.GetDetailMaterials(matdata).then(function (d) {
                    $scope.Material = d.data;
                }, function (error) {
                    $scope.ListMaterialsmessage = "Error";
                });
            }

            //Edit material
            $scope.gotoeditmaterial = function (matdata) {
                $scope.ListMaterialsmessage = matdata;
                //$scope.Detailmaterial(matdata);
                $scope.EditMaterialsmessage = "";
                $scope.materialslistvisibility = false;
                $scope.materialseditvisibility = true;
            };

            //Create material
            $scope.gotoaddmaterial = function (matdata) {
                //$scope.Detailmaterial(matdata);
                $scope.AddMaterialsmessage = "";
                $scope.materialslistvisibility = false;
                $scope.materialsaddvisibility = true;
            };

            //Delete material
            $scope.postdeletematerial = function (data) {
                MaterialsService.PostDeleteMaterials(data).then(function (d) {
                    if (d.data == "Success") {
                        $scope.ListMaterialsmessage = "";
                        $scope.Listmaterial(0);
                    } else {
                        $scope.ListMaterialsmessage = d.data;
                    }
                }, function (error) {
                    $scope.ListMaterialsmessage = "Error";
                });
            };

        }]
    };
})

.directive('trMaterialsedit', function (MaterialsService) {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/structure3D/templates/materials/EditMaterialTemplate.html',
        controller: ['$scope', function trMaterialseditCtrl($scope) {
            //materials window control
            $('#windowmaterials').click(function () {
                $('#inputsectioncard').toggleClass("col-md-12");
                $('#outputsectioncard').toggle();
                $('#windowmaterials').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
            });
            //Back to List of material
            $scope.editgotolistmaterial = function () {
                $scope.Listmaterial(0);
                $scope.materialseditvisibility = false;
                $scope.materialslistvisibility = true;
                $scope.EditMaterialsButtonText = "Edit";
                $scope.EditMaterialsmessage = "";
            };

            //Edit material
            $scope.EditMaterialsButtonText = "Edit";
            $scope.EditMaterialsmessage ="";

            $scope.editmaterial = function (data) {
                $scope.EditMaterialsButtonText = "Save..";
                MaterialsService.PostEditMaterials(data).then(function (d) {
                    $scope.EditMaterialsmessage = d.data;
                    $scope.EditMaterialsButtonText = "Edit";
                    if (d.data == "Success") {
                        $scope.Listmaterial(0);
                        $scope.materialseditvisibility = false;
                        $scope.materialslistvisibility = true;
                        $scope.EditMaterialsmessage = "";
                    }
                }, function (error) {
                    $scope.EditMaterialsmessage='Error Material!';
                });
            };
        }]
    };
})

.directive('trMaterialsadd', function (MaterialsService) {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/structure3D/templates/materials/AddMaterialTemplate.html',
        controller: ['$scope', function trMaterialsaddCtrl($scope) {
            //materials window control
            $('#windowmaterials').click(function () {
                $('#inputsectioncard').toggleClass("col-md-12");
                $('#outputsectioncard').toggle();
                $('#windowmaterials').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
            });
            //Back to List of material
            $scope.addgotolistmaterial = function () {
                $scope.Listmaterial(0);
                $scope.materialsaddvisibility = false;
                $scope.materialslistvisibility = true;
                $scope.AddMaterialsButtonText = "Add";
                $scope.AddMaterialsmessage = "";
            };

            //Add material
            $scope.AddMaterialsButtonText = "Add";
            $scope.AddMaterialsmessage = "";

            $scope.addmaterial = function (data) {
                $scope.AddMaterialsButtonText = "Save..";
                MaterialsService.PostAddMaterials(data).then(function (d) {
                    $scope.AddMaterialsmessage = d.data;
                    $scope.AddMaterialsButtonText = "Add";
                    if (d.data == "Success")
                    {
                        $scope.Listmaterial(0);
                        $scope.materialsaddvisibility = false;
                        $scope.materialslistvisibility = true;
                    }

                }, function (error) {
                    $scope.AddMaterialsmessage = "Error Material!";
                });
            };

        }]
    };
})


