var Structure3DServices = angular.module('Structure3DServices', [])

.factory('Structure3DService', function ($http) {
    var fac = {};
    fac.GetListSections = function () {
        return $http.get('/sectionsdata/listsections');
    }
    fac.GetListNodes = function () {
        return $http.get('/nodesdata/listnodes');
    }
    fac.GetDataStructures3d = function (data) {
        return $http.get('/structure3danalysisdata/datastructure3d', { params: data });
    }
    fac.nodesdrawing = function (nodes, listnodes) {
        //Deleting all nodes
        for (var i = nodes.length - 1; i >= 0; i--) {
            scene.remove(nodes[i])
        }
        nodes.length = 0;
        //Drawing all nodes "Fixed" = SupportId = 1 = Box geometry, "Free" = Sphere geometry
        var spherege = new THREE.SphereGeometry(0.5, 32, 32);
        var boxge = new THREE.BoxGeometry(1, 1, 1);
        var materialsphere = new THREE.MeshBasicMaterial({ color: 'blue' });
        var materialbox = new THREE.MeshBasicMaterial({ color: 'skyblue' });
        for (var i = 0; i < listnodes.length; i++) {
            if (listnodes[i].SupportId === 1) {
                nodes[i] = new THREE.Mesh(boxge, materialbox);
                nodes[i].position.set(listnodes[i].X, listnodes[i].Y, listnodes[i].Z);
                scene.add(nodes[i]);
            } else {
                nodes[i] = new THREE.Mesh(spherege, materialsphere);
                nodes[i].position.set(listnodes[i].X, listnodes[i].Y, listnodes[i].Z);
                scene.add(nodes[i]);
            }
        }
    };
    return fac;

});


