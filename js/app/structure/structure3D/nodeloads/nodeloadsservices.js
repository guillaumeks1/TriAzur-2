﻿var NodeLoadsServices = angular.module('NodeLoadsServices', [])

.factory('NodeLoadsService', function ($http, $q) {
    var fac = {};
    fac.GetListNodeLoads = function () {
        return $http.get('/nodeloadsdata/listnodeloads');
    }
    fac.GetDetailNodeLoads = function (data) {
        return $http.get('/nodeloadsdata/detailsnodeloads', { params: data });
    }
    fac.PostEditNodeLoads = function (data) {
        return $http({
            url: '/nodeloadsdata/editnodeloads',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.PostEditListNodeLoads = function (data) {
        return $http({
            url: '/nodeloadsdata/editlistnodeloads',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.PostAddNodeLoads = function (data) {
        return $http({
            url: '/nodeloadsdata/addnodeloads',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.PostDeleteNodeLoads = function (data) {
        return $http({
            url: '/nodeloadsdata/deletenodeloads',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        });
    }
    return fac;

})

