﻿var NodeLoadsDirectives = angular.module('NodeLoadsDirectives', ['NodeLoadsServices'])

.directive('trNodeloadslist', function (NodeLoadsService) {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/structure3D/templates/nodeloads/ListNodeLoadTemplate.html',
        controller: ['$scope', function trNodeLoadslistCtrl($scope) {
            //nodeloadloads window control
            $('#windownodeloads').click(function () {
                $('#inputsectioncard').toggleClass("col-md-12");
                $('#outputsectioncard').toggle();
                $('#windownodes').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
                $('#windowmembers').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
                $('#windownodeloads').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
                $('#windownmemberloads').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
                $('#windownodedisplacements').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
            });

            //In order to control the visibility of tr-nodeloadslist
            $scope.nodeloadslistvisibility = true;

            //List of nodeloads
            $scope.listnodeloads = [];

            $scope.Listnodeload = function (id) {
                NodeLoadsService.GetListNodeLoads().then(function (d) {
                    $scope.listnodeloads = d.data;
                    $scope.NodeLoad = $scope.listnodeloads[id];
                }, function (error) {
                    $scope.ListNodeLoadsmessage = "Error";
                });
            }
            $scope.Listnodeload(0);

            //Edit nodeload
            $scope.posteditnodeload = function (ddata) {
                $scope.ListNodeLoadsmessage = "";
                NodeLoadsService.PostEditNodeLoads(ddata).then(function (d) {
                    $scope.ListNodeLoadsmessage = d.data;
                    if (d.data == "Success") {
                        $scope.Listnodeload(0);
                        $scope.ListNodeLoadsmessage = d.data;
                    }
                }, function (error) {
                    $scope.ListNodeLoadsmessage = 'Error NodeLoad!';
                });
            };

            //Edit listnodeloads
            $scope.posteditlistnodeloads = function(){
                $scope.ListNodeLoadsmessage = "";
                NodeLoadsService.PostEditListNodeLoads($scope.listnodeloads).then(function (d) {
                    $scope.ListNodeLoadsmessage = d.data;
                    if (d.data == "Success") {
                        $scope.Listnodeload(0);
                        $scope.ListNodeLoadsmessage = d.data;
                    }
                }, function (error) {
                    $scope.ListNodeLoadsmessage = 'Error Node Load!';
                });
            };

            //Create nodeload
            $scope.postaddnodeload = function () {
                $scope.ListNodeLoadsmessage = "";
                $scope.Listnodeload(0);
                NodeLoadsService.PostAddNodeLoads().then(function (d) {
                    $scope.ListNodeLoadsmessage = d.data;
                    if (d.data === "Success") {
                        $scope.Listnodeload(0);
                        $scope.ListNodeLoadsmessage = "";
                    }
                }, function (error) {
                    $scope.ListNodeLoadsmessage = "Error NodeLoad!";
                });
            };


            //Delete nodeload
            $scope.postdeletenodeload = function (data) {
                NodeLoadsService.PostDeleteNodeLoads(data).then(function (d) {
                    if (d.data === "Success") {
                        $scope.ListNodeLoadsmessage = "";
                        $scope.Listnodeload(0);
                    } else {
                        $scope.ListNodeLoadsmessage = d.data;
                    }   
                }, function (error) {
                    $scope.ListNodeLoadsmessage = "Error";
                });
            };

        }]
    };
})

