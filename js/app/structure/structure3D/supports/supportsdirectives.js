﻿var SupportsDirectives = angular.module('SupportsDirectives', ['SupportsServices'])

.directive('trSupportslist', function (SupportsService) {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/structure3D/templates/supports/ListSupportTemplate.html',
        controller: ['$scope', function trSupportslistCtrl($scope) {
            //In order to control the visibility of tr-supportslist
            $scope.supportslistvisibility = true;

            //List of supports
            $scope.listsupports = [];

            $scope.Listsupport = function (id) {
                SupportsService.GetListSupports().then(function (d) {
                    $scope.listsupports = d.data;
                    $scope.Support = $scope.listsupports[id];
                }, function (error) {
                    $scope.ListSupportsmessage = "Error";
                });
            }
            $scope.Listsupport(0);

            //Detail support
            $scope.Detailsupport = function (ddata) {
                SupportsService.GetDetailSupports(ddata).then(function (d) {
                    $scope.Support = d.data;
                }, function (error) {
                    $scope.ListSupportsmessage = "Error";
                });
            }

            //Edit support
            $scope.gotoeditsupport = function (ddata) {
                //$scope.Detailsupport(ddata);
                $scope.EditSupportsmessage = "";
                $scope.supportslistvisibility = false;
                $scope.supportseditvisibility = true;
            };

            //Create support
            $scope.gotoaddsupport = function (ddata) {
                //$scope.Detailsupport(ddata);
                $scope.AddSupportsmessage = "";
                $scope.supportslistvisibility = false;
                $scope.supportsaddvisibility = true;
            };

            //Delete support
            $scope.postdeletesupport = function (data) {
                SupportsService.PostDeleteSupports(data).then(function (d) {
                    if (d.data == "Success") {
                        $scope.ListSupportsmessage = "";
                        $scope.Listsupport(0);
                    } else {
                        $scope.ListSupportsmessage = d.data;
                    }   
                }, function (error) {
                    $scope.ListSupportsmessage = "Error";
                });
            };

        }]
    };
})

.directive('trSupportsedit', function (SupportsService) {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/structure3D/templates/supports/EditSupportTemplate.html',
        controller: ['$scope', function trSupportseditCtrl($scope) {

            //Back to List of support
            $scope.editgotolistsupport = function () {
                $scope.Listsupport(0);
                $scope.supportseditvisibility = false;
                $scope.supportslistvisibility = true;
                $scope.EditSupportsButtonText = "Edit";
                $scope.EditSupportsmessage = "";
                $scope.ListSupportsmessage = "";
            };

            //Edit support
            $scope.EditSupportsButtonText = "Edit";
            $scope.EditSupportsmessage = "";

            $scope.editsupport = function (data) {
                $scope.EditSupportsButtonText = "Save..";
                $scope.ListSupportsmessage = "";
                SupportsService.PostEditSupports(data).then(function (d) {
                    $scope.EditSupportsmessage = d.data;
                    $scope.EditSupportsButtonText = "Edit";
                    if (d.data == "Success") {
                        $scope.Listsupport(0);
                        $scope.supportseditvisibility = false;
                        $scope.supportslistvisibility = true;
                        $scope.EditSupportsmessage = "";
                    }
                }, function (error) {
                    $scope.EditSupportsmessage = 'Error Support!';
                });
            };
        }]
    };
})

.directive('trSupportsadd', function (SupportsService) {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/structure3D/templates/supports/AddSupportTemplate.html',
        controller: ['$scope', function trSupportsaddCtrl($scope) {

            //Back to List of support
            $scope.addgotolistsupport = function () {
                $scope.Listsupport(0);
                $scope.supportsaddvisibility = false;
                $scope.supportslistvisibility = true;
                $scope.AddSupportsButtonText = "Add";
                $scope.AddSupportsmessage = "";
                $scope.ListSupportsmessage = "";
            };

            //Add support
            $scope.AddSupportsButtonText = "Add";
            $scope.AddSupportsmessage = "";

            $scope.addsupport = function (data) {
                $scope.AddSupportsButtonText = "Save..";
                $scope.ListSupportsmessage = "";
                SupportsService.PostAddSupports(data).then(function (d) {
                    $scope.AddSupportsmessage = d.data;
                    $scope.AddSupportsButtonText = "Add";
                    if (d.data == "Success") {
                        $scope.Listsupport(0);
                        $scope.supportsaddvisibility = false;
                        $scope.supportslistvisibility = true;
                    }

                }, function (error) {
                    $scope.AddSupportsmessage = "Error Support!";
                });
            };

        }]
    };
})


