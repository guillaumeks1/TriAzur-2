﻿var SupportsServices = angular.module('SupportsServices', [])

.factory('SupportsService', function ($http) {
    var fac = {};
    fac.GetListSupports = function () {
        return $http.get('/supportsdata/listsupports');
    }
    fac.GetDetailSupports = function (data) {
        return $http.get('/supportsdata/detailssupports', { params: data });
    }
    fac.PostEditSupports = function (data) {
        return $http({
            url: '/supportsdata/editsupports',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.PostAddSupports = function (data) {
        return $http({
            url: '/supportsdata/addsupports',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.PostDeleteSupports = function (data) {
        return $http({
            url: '/supportsdata/deletesupports',
            method: 'POST',
            data: JSON.stringify(data),
            headers: { 'content-type': 'application/json' }
        });
    }
    return fac;

})

