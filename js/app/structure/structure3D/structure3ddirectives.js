var Structure3DDirectives = angular.module('Structure3DDirectives', ['MaterialsDirectives','SectionsDirectives',
'SupportsDirectives','NodesDirectives','MembersDirectives','NodeLoadsDirectives'])

.directive('trMaterials', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/structure3D/templates/MaterialsTemplate.html',
        //scope:{},
        controller: ['$scope', function trMaterialsCtrl($scope) {

            $scope.TextMaterials = "Materials";

            $scope.TextMaterialDensity = "Material Density";

            $scope.InfoMaterialDensity = "Material Density";

            $scope.MaterialDensity = 12;

            $(document).ready(function () {
                $('[data-toggle="Depthhelp"]').tooltip();
            });
        }]
    };
})

.directive('trSections', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/structure3D/templates/SectionsTemplate.html'
    };
})

.directive('trSupports', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/structure3D/templates/SupportsTemplate.html'
    };
})

.directive('trNodes', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/structure3D/templates/NodesTemplate.html'
    };
})

.directive('trMembers', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/structure3D/templates/MembersTemplate.html'
    };
})

.directive('trNodeloads', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/structure3D/templates/NodeLoadsTemplate.html'
    };
})

// .directive('trMemberloads', function () {
//     return {
//         restrict: 'A',
//         templateUrl: '/js/app/structure/structure3D/templates/MemberLoadsTemplate.html'
//     };
// })

// .directive('trNodedisplacements', function () {
//     return {
//         restrict: 'A',
//         templateUrl: '/js/app/structure/structure3D/templates/NodeDisplacementsTemplate.html'
//     };
// })


.directive('trStructure3d', function (Structure3DService) {
    return {
        restrict: 'A',
        scope: {
            'width': '=',
            'height': '=',
            'fillcontainer': '=',
            'scale': '=',
            'materialType': '=',
            'centerposition': '=',
            'listsections':'=',
            'listsupports':'=',
            'listnodes': '=',
            'listmembers': '=',
            'listnodeloads' : '=',
            'message' : '=',
            'coord2dlabel' : '=',
            'threedimensionsstructure':'='
        },
        link: function postLink(scope, element, attrs) {
            var camera, scene, renderer,
              shadowMesh, icosahedron, box, light, line,
              mouseX = 0, mouseY = 0,
              contW = (scope.fillcontainer) ?
                element[0].clientWidth : scope.width,
              contH = scope.height,
              windowHalfX = contW / 2,
              windowHalfY = contH / 2,
              materials = {};

            // var raycaster;
            // var mouse = new THREE.Vector2();

            var objects = []; 
            scope.init = function () {
                // Camera
                camera = new THREE.PerspectiveCamera(10, contW / contH, 1, 2000);
     
                // Place camera on y axis
                camera.position.set(0,-200,0);
                camera.up = new THREE.Vector3(0,0,1);
                camera.lookAt(new THREE.Vector3(0,0,0));
                

                // Scene
                scene = new THREE.Scene();
                //scene.add(camera);
                // // Ligthing
                light = new THREE.DirectionalLight(0xffffff);
                light.position.set(0, 0,1);
                scene.add(light);

                 //Axes X, Y, Z
                var axisHelper = new THREE.AxisHelper(1);
                scene.add(axisHelper);

                // if(Detector.webgl){
                //     renderer = new THREE.WebGLRenderer({antialias:true});
                // }else{
                //     renderer = new THREE.CanvasRenderer();
                // }


                renderer = new THREE.WebGLRenderer({ antialias: true });
                renderer.setClearColor(0xffffff);
                renderer.setSize(contW, contH)

                // element is provided by the angular directive
                element[0].appendChild(renderer.domElement);

                //add OrbitControls so that we can pan around with the mouse
                controls = new THREE.OrbitControls(camera, renderer.domElement);
                controls.target.set(20, 0, 10);

                //document.addEventListener('mousemove', scope.onDocumentMouseMove, false);
                element[0].addEventListener('mousedown', scope.onDocumentMouseDown, false);
                //element[0].addEventListener('mousedown', scope.onMouseDown, false);
                window.addEventListener('resize', scope.onWindowResize, false);
            };


            // -----------------------------------
            // Event listeners
            // -----------------------------------
            scope.onWindowResize = function () {
                scope.resizeCanvas();
            };

            scope.onDocumentMouseDown = function(event){    
                event.preventDefault();

                //Coordinates of top right corner of the WebGl Container
                var position = element[0].getBoundingClientRect();
                var x = position.left;
                var y = position.top;

                var mouse3D = new THREE.Vector3(((event.clientX-x)/contW)*2-1,   
                                        -((event.clientY-y)/contH)*2 + 1,  
                                        0.5);

                var raycaster =  new THREE.Raycaster();                                        
                raycaster.setFromCamera(mouse3D,camera);
                var intersects = raycaster.intersectObjects(objects);

                if ( intersects.length > 0 ) {
                    intersects[0].object.material.color.setHex( Math.random() * 0xffffff );
                    //alert(intersects[0].object.position.x);
                }
            };

            scope.animate = function () {
                requestAnimationFrame(scope.animate);

                for(var i=0; i<nodelabels.length; i++) {
                    nodelabels[i].updatePosition();
                }
                renderer.render(scene, camera);

                controls.update();
            };

            // -----------------------------------
            // Updates
            // -----------------------------------
            scope.resizeCanvas = function () {
                contW = (scope.fillcontainer) ?
                  element[0].clientWidth : scope.width;
                contH = scope.height;

                windowHalfX = contW / 2;
                windowHalfY = contH / 2;

                camera.aspect = contW / contH;
                camera.updateProjectionMatrix();

                renderer.setSize(contW, contH);
            };

            scope.resizeObject = function () {

                box.scale.set(scope.scale, scope.scale, scope.scale);
                icosahedron.scale.set(scope.scale, scope.scale, scope.scale);
                shadowMesh.scale.set(scope.scale, scope.scale, scope.scale);
                line.scale.set(scope.scale, scope.scale, scope.scale);
            };

            scope.changeMaterial = function () {

                box.material = materials[scope.materialType];
                icosahedron.material = materials[scope.materialType];

            };

            // scope.centerPositionCdG = function () {
            //     camera.position.x = 0;
            //     camera.position.y = 0;
            //     camera.position.z = 200;
            //     //camera.lookAt(scene.position);
            //     renderer.render(scene, camera);
            // };
            // -----------------------------------
            // Draw and Animate
            // -----------------------------------
            //scope.animate = function () {
            //    requestAnimationFrame(scope.animate);
            //    scope.render();
            //};


            //scope.render = function () {
            //    camera.position.x += (mouseX - camera.position.x) * 0.05;
            //    // camera.position.y += ( - mouseY - camera.position.y ) * 0.05;
            //    camera.lookAt(scene.position);
            //    renderer.render(scene, camera);
            //};

            // -----------------------------------
            // Watches: Interaction Rendu Graphiques/Commandes
            // -----------------------------------
            scope.$watch('fillcontainer + width + height', function () {

                scope.resizeCanvas();

            });
           
            scope.$watch('scale', function () {

                scope.resizeObject();

            });

            scope.$watch('materialType', function () {

                scope.changeMaterial();

            });

            scope.$watch('centerposition', function () {
                scope.centerPositionCdG();
            }, true);

            var nodes = [];
            scope.$watch('listnodes', function (newvalue, oldvalue) {
                if (newvalue != oldvalue) {
                    scope.nodesdrawing();
                    scope.membersdrawing();
                    scope.nodeloadsdrawing();
                }
            }, true);

            scope.cylinder = function (colorvalue) {
                var geometry = new THREE.CylinderGeometry(5, 5, 20, 4);
                //var material = new THREE.MeshBasicMaterial({ color: colorvalue });
                //var material = new THREE.MeshPhongMaterial({
                //    color: colorvalue,
                //    emissive: 0x072534,
                //    side: THREE.DoubleSide,
                //    shadding: THREE.FlatShading
                //});
                //var material = new THREE.LineBasicMaterial({
                //    color: colorvalue
                //});
                var material = new THREE.MeshBasicMaterial({ 
                    color: 0x000000, 
                    shading: THREE.FlatShading, 
                    wireframe: true, 
                    transparent: true });
                var cylinder = new THREE.Mesh(geometry, material);
                scene.add(cylinder);
            };

            scope.rectangledesign = function (A, L, colorvalue) {

                var geometry = new THREE.Geometry();

                geometry.vertices.push(new THREE.Vector3(A.X, A.Y, A.Z));
                geometry.vertices.push(new THREE.Vector3(A.X + L, A.Y, A.Z));
                geometry.vertices.push(new THREE.Vector3(A.X + L, A.Y + L, A.Z));
                geometry.vertices.push(new THREE.Vector3(A.X, A.Y + L, A.Z));

                geometry.faces.push(new THREE.Face3(0, 1, 2));
                geometry.faces.push(new THREE.Face3(0, 2, 3));

                var material = new THREE.MeshBasicMaterial({ color: colorvalue, side: THREE.FrontSide });
                var material1 = new THREE.MeshBasicMaterial({ color: 'blue', side: THREE.BackSide });
                var mesh = new THREE.Mesh(geometry, material);
                scene.add(mesh);
                var mesh1 = new THREE.Mesh(geometry, material1);
                scene.add(mesh1);
            };

            var mesh = [];
            var P = [];
            scope.sectionmemberdesign = function(meshindex, Ai, Aj,XG, YG, Xext, Yext, Xint, Yint, colorvalue) {

                var geometry = new THREE.Geometry();
                var Lij = Math.pow((Math.pow((Aj.X - Ai.X), 2) + Math.pow((Aj.Y - Ai.Y), 2) + Math.pow((Aj.Z - Ai.Z), 2)), 0.5);
                
                for (var i = 0; i < 3; i++) {
                    P[i]= [];
                    for (var j = 0; j < 3; j++) {
                        P[i][j] = 0;
                    }
                }

                //vecteur u1
                if (Lij != 0) {
                    P[0][0] = (Aj.X - Ai.X) / Lij;
                    P[0][1] = (Aj.Y - Ai.Y) / Lij;
                    P[0][2] = (Aj.Z - Ai.Z) / Lij;
                }

                ////vecteur u2
                if (Math.abs(P[0][2]) === 1) {
                    P[1][0] = 0;
                    P[1][1] = 1 * P[0][2];
                    P[1][2] = 0;
                }
                else {
                    P[1][0] = -P[0][1] / Math.pow((Math.pow(P[0][0], 2) + Math.pow(P[0][1], 2)), 0.5);
                    P[1][1] = P[0][0] / Math.pow((Math.pow(P[0][0], 2) + Math.pow(P[0][1], 2)), 0.5);
                    P[1][2] = 0;
                }

                ////vecteur u3
                var ProdVect;
                ProdVect = P[0][0] * P[1][0] + P[0][1] * P[1][1] + P[0][2] * P[1][2];
                P[2][0] = (P[0][1] * P[1][2] - P[0][2] * P[1][1]) / Math.pow((1 - Math.pow(ProdVect, 2)), 0.5);
                P[2][1] = (P[0][2] * P[1][0] - P[0][0] * P[1][2]) / Math.pow((1 - Math.pow(ProdVect, 2)), 0.5);
                P[2][2] = (P[0][0] * P[1][1] - P[0][1] * P[1][0]) / Math.pow((1 - Math.pow(ProdVect, 2)), 0.5);
                var Xi = 0;
                var Yi = 0;
                var Zi = 0;
                var messagei = [];
                var N = Xext.length;


                //Drawing line corresponding to members
                var linegeo = [];
                var linemat = new THREE.LineBasicMaterial({ color: 'black' });

                var Xi = [], Yi = [], Zi = [],
                Xj = [], Yj = [], Zj = [];

                for(i = 0; i < N; i++){

                        Xi[i] = Number(Ai.X) + P[1][0] * Number(Xext[i] - XG) + P[2][0] * Number(Yext[i] - YG);
                        Yi[i] = Number(Ai.Y) + P[1][1] * Number(Xext[i] - XG) + P[2][1] * Number(Yext[i] - YG);
                        Zi[i] = Number(Ai.Z) + P[1][2] * Number(Xext[i] - XG) + P[2][2] * Number(Yext[i] - YG);
                        messagei[i] ='N°' + i+ ' Xi = ' + Xi[i] + " Yi = " + Yi[i] + " Zi = " + Zi[i];
                        geometry.vertices.push(new THREE.Vector3(Xi[i], Yi[i], Zi[i]));
                        Xj[i] = P[0][0] * Lij + Xi[i];
                        Yj[i] = P[0][1] * Lij + Yi[i];
                        Zj[i] = P[0][2] * Lij + Zi[i];
                        geometry.vertices.push(new THREE.Vector3(Xj[i], Yj[i], Zj[i]));


                        // linegeo[i] = new THREE.Geometry();
                        // linegeo[i].vertices.push(new THREE.Vector3(Xi[i], Yi[i], Zi[i]));
                        // linegeo[i].vertices.push(new THREE.Vector3(Xj[i], Yj[i], Zj[i]));
                        // members[i] = new THREE.Line(linegeo[i], linemat);
                        // scene.add(members[i]);  
                }

                scope.message = messagei;
                var materials = [];
                for (i = 0; i < N - 1; i++) {
                    j = 2 * i;
                    geometry.faces.push(new THREE.Face3(j, j + 2, j + 1));
                    geometry.faces.push(new THREE.Face3(j + 1, j + 2, j + 3));
                }
                geometry.faces.push(new THREE.Face3(2 * N - 2, 0, 2 * N - 1));
                geometry.faces.push(new THREE.Face3(2 * N - 1, 0, 1));
                var material = new THREE.MeshBasicMaterial({
                    color: colorvalue,
                    side: THREE.DoubleSide,
                    shading: THREE.FlatShading,
                    wireframe: true,
                });

                mesh[meshindex] = new THREE.Mesh(geometry, material);
                scene.add(mesh[meshindex]);
            };

            scope.cylinderdesign = function (meshindex, Ai, Aj, R, N, colorvalue) {

                var geometry = new THREE.Geometry();

                var Lij = Math.pow((Math.pow((Aj.X - Ai.X), 2) + Math.pow((Aj.Y - Ai.Y), 2) + Math.pow((Aj.Z - Ai.Z), 2)), 0.5);

                for (var i = 0; i < 3; i++) {
                    P[i]= [];
                    for (var j = 0; j < 3; j++) {
                        P[i][j] = 0;
                    }
                }
                //vecteur u1
                if (Lij != 0) {
                    P[0][0] = (Aj.X - Ai.X) / Lij;
                    P[0][1] = (Aj.Y - Ai.Y) / Lij;
                    P[0][2] = (Aj.Z - Ai.Z) / Lij;
                }

                ////vecteur u2
                if (Math.abs(P[0][2]) === 1) {
                    P[1][0] = 0;
                    P[1][1] = 1 * P[0][2];
                    P[1][2] = 0;
                }
                else {
                    P[1][0] = -P[0][1] / Math.pow((Math.pow(P[0][0], 2) + Math.pow(P[0][1], 2)), 0.5);
                    P[1][1] = P[0][0] / Math.pow((Math.pow(P[0][0], 2) + Math.pow(P[0][1], 2)), 0.5);
                    P[1][2] = 0;
                }

                ////vecteur u3
                var ProdVect;
                ProdVect = P[0][0] * P[1][0] + P[0][1] * P[1][1] + P[0][2] * P[1][2];
                P[2][0] = (P[0][1] * P[1][2] - P[0][2] * P[1][1]) / Math.pow((1 - Math.pow(ProdVect, 2)), 0.5);
                P[2][1] = (P[0][2] * P[1][0] - P[0][0] * P[1][2]) / Math.pow((1 - Math.pow(ProdVect, 2)), 0.5);
                P[2][2] = (P[0][0] * P[1][1] - P[0][1] * P[1][0]) / Math.pow((1 - Math.pow(ProdVect, 2)), 0.5);
                var Xi = 0;
                var Yi = 0;
                var Zi = 0;
                var messagei = [];
                for (i = 0; i < N; i++) {
                        //var Xi = 0;
                        //var Yi = 0;
                        //var Zi = 0;
                        Xi = Number(Ai.X) + P[1][0] * R * Math.cos(2 * Math.PI * i / N) + P[2][0] * R * Math.sin(2 * Math.PI * i / N);
                        Yi = Number(Ai.Y) + P[1][1] * R * Math.cos(2 * Math.PI * i / N) + P[2][1] * R * Math.sin(2 * Math.PI * i / N);
                        Zi = Number(Ai.Z) + P[1][2] * R * Math.cos(2 * Math.PI * i / N) + P[2][2] * R * Math.sin(2 * Math.PI * i / N);
                        messagei[i] ='N°' + i+ ' Xi = ' + Xi + " Yi = " + Yi + " Zi = " + Zi;
                        //Xi = i;
                        //Yi = i;
                        //Zi = i;
                        geometry.vertices.push(new THREE.Vector3(Xi, Yi, Zi));
                        var Xj = P[0][0] * Lij + Xi;
                        var Yj = P[0][1] * Lij + Yi;
                        var Zj = P[0][2] * Lij + Zi;
                        //var Xj = Xi + Aj.X - Ai.X;
                        //var Yj = Yi + Aj.Y - Ai.Y;
                        //var Zj = Zi + Aj.Z - Ai.Z;
                        //var Xj = Aj.X;
                        //var Yj = Aj.Y+ R * Math.cos(2 * 3.14116 * i / N);
                        //var Zj = Aj.Z+ R * Math.sin(2 * 3.14116 * i / N);
                        geometry.vertices.push(new THREE.Vector3(Xj, Yj, Zj));
                        //messagei[i] = 'N°' + i + ' Xj = ' + Xj + " Yj = " + Yj + " Zj = " + Zj;

                }
                scope.message = messagei;
                var materials = [];
                for (i = 0; i < N - 1; i++) {
                    j = 2 * i;
                    geometry.faces.push(new THREE.Face3(j, j + 2, j + 1));
                    geometry.faces.push(new THREE.Face3(j + 1, j + 2, j + 3));
                }
                geometry.faces.push(new THREE.Face3(2 * N - 2, 0, 2 * N - 1));
                geometry.faces.push(new THREE.Face3(2 * N - 1, 0, 1));
                //var faceIndices = ['a', 'b', 'c', 'd'];
                //for (var k = 0; k < geometry.faces.length; k++) {
                //    f = geometry.faces[k];
                //    var n = ( f instanceof THREE.Face3 ) ? 3 : 4;
                //    for (var j = 0; j < n; j++) {

                //        var vertexIndex = f[faceIndices[j]];

                //        p = geometry.vertices[vertexIndex];

                //        color = new THREE.Color(0xffffff);
                //        color.setHSL(0.125 * vertexIndex / geometry.vertices.length, 1.0, 0.5);

                //        f.vertexColors[j] = color;
                //    }
                //}

                //var materials = [
                //    new THREE.MeshBasicMaterial({ color: 'blue', side: THREE.FrontSide }),
                //    new THREE.MeshBasicMaterial({ color: 'red', side: THREE.BackSide })

                //];

                var material = new THREE.MeshBasicMaterial({
                    color: colorvalue,
                    side: THREE.DoubleSide,
                    shading: THREE.FlatShading,
                    wireframe: true,
                });
                //var material = new THREE.MeshBasicMaterial({
                //    color: colorvalue,
                //    side: THREE.DoubleSide
                //});
                //var multimaterial = new THREE.MultiMaterial(materials);
                mesh[meshindex] = new THREE.Mesh(geometry, material);
                scene.add(mesh[meshindex]);
            }

            scope.$watch('listsupports', function (newvalue, oldvalue) {
                //Lines representing Members
                if (newvalue != oldvalue) {
                    scope.nodesdrawing();
                }
            }, true);

            var members = [];
            scope.$watch('listmembers', function (newvalue, oldvalue) {
                //Lines representing Members
                if (newvalue != oldvalue) {
                    scope.membersdrawing();
                }
            }, true);



            var nodeloadsFx = [], nodeloadsFy = [], nodeloadsFz = [];
            scope.$watch('listnodeloads', function (newvalue, oldvalue) {
                //Node loads are representing by Arrow
                if (newvalue != oldvalue) {
                    scope.nodeloadsdrawing();
                }
            }, true);


            scope.nodesdrawing = function () {
                //Deleting all nodes
                for (var i = nodes.length - 1; i >= 0; i--) {
                    scene.remove(nodes[i]);
                }

                //Deleting all nodes labels
                var elements = document.getElementsByClassName('node-label');
                while(elements.length > 0){
                    elements[0].parentNode.removeChild(elements[0]);
                }


                nodes.length = 0;
                //Drawing all nodes "Fixed" = SupportId = 1 = Box geometry, "Free" = Sphere geometry
                var spherege = new THREE.SphereGeometry(0.5, 32, 32);
                var boxge = new THREE.BoxGeometry(1, 1, 1);
                var materialsphere = new THREE.MeshBasicMaterial({ color: 'blue' });
                var materialbox = new THREE.MeshBasicMaterial({ color: 'skyblue' });


                // scene.add(nodename);
                var FixedID;
                for(var j = 0; j < scope.listsupports.length; j++){
                    if(scope.listsupports[j].DispX === 0 && scope.listsupports[j].DispY === 0
                    && scope.listsupports[j].DispZ === 0 && scope.listsupports[j].RotX === 0
                    && scope.listsupports[j].RotY === 0 && scope.listsupports[j].RotZ === 0){
                        FixedID=scope.listsupports[j]._id;
                    }
                }

                
                for (var i = 0; i < scope.listnodes.length; i++) {
                    if (scope.listnodes[i].SupportId === FixedID) {
                        nodes[i] = new THREE.Mesh(boxge, materialbox);
                        nodes[i].position.set(scope.listnodes[i].X, scope.listnodes[i].Y, scope.listnodes[i].Z);
                        scene.add(nodes[i]);
                    } else {
                        nodes[i] = new THREE.Mesh(spherege, materialsphere);
                        nodes[i].position.set(scope.listnodes[i].X, scope.listnodes[i].Y, scope.listnodes[i].Z);
                        scene.add(nodes[i]);
                    }
                    objects.push(nodes[i]);

                    var nodelabel = scope._createNodeLabel();
                    nodelabel.setHTML(scope.listnodes[i].Name);
                    nodelabel.setParent(nodes[i]);
                    nodelabels.push(nodelabel);
                    element[0].appendChild(nodelabel.element);
                }
            };

            scope.membersdrawing = function () {
                //Deleting all old members
                for (var i = members.length - 1; i >= 0; i--) {
                    scene.remove(members[i])
                }
                //Deleting all old 3D members
                for (var k = mesh.length - 1; k >= 0; k--) {
                    scene.remove(mesh[k]);
                }

                members.length = 0;
                //Drawing line corresponding to members
                var linegeo = [];
                var linemat = new THREE.LineBasicMaterial({ color: 'black' });
                for (var i = 0; i < scope.listmembers.length; i++) {
                    linegeo[i] = new THREE.Geometry();
                    for (var j = 0; j < scope.listnodes.length; j++) {
                        if (scope.listnodes[j]._id === scope.listmembers[i].NodeiId) {
                            linegeo[i].vertices.push(new THREE.Vector3(scope.listnodes[j].X, scope.listnodes[j].Y, scope.listnodes[j].Z));
                            var Ai = scope.listnodes[j];
                        }
                        if (scope.listnodes[j]._id === scope.listmembers[i].NodejId) {
                            linegeo[i].vertices.push(new THREE.Vector3(scope.listnodes[j].X, scope.listnodes[j].Y, scope.listnodes[j].Z));
                            var Aj = scope.listnodes[j];
                        }
                    }
                    if (scope.threedimensionsstructure === true) {
                        //scope.cylinderdesign(i, Ai, Aj, 2, 5, 'blue');
                        //scope.sectionmemberdesign(i, Ai, Aj, [0, 3, 3, 0], [0, 0, 2, 2], [0, 0, 0, 0], [0,0, 0, 0], 'blue');
                        for (var j = 0; j < scope.listsections.length; j++) {
                            if (scope.listsections[j]._id === scope.listmembers[i].SectionId) {
                                scope.sectionmemberdesign(i, Ai, Aj, scope.listsections[j].XG, scope.listsections[j].YG, scope.listsections[j].Xext, scope.listsections[j].Yext, scope.listsections[j].Xint, scope.listsections[j].Yint, 'blue');
                        
                    
                            }
                        }
                    } else {
                        members[i] = new THREE.Line(linegeo[i], linemat);
                        scene.add(members[i]);
                    }
                }
            };

            scope.$watch('threedimensionsstructure', function (newvalue, oldvalue) {
                if (newvalue != oldvalue) {
                    scope.membersdrawing();
                }
            }, true);

            var nodelabels= [];
            var coord2dlabel = [];
            scope._createNodeLabel = function() {
                var div = document.createElement('div');
                div.className = 'node-label';
                div.style.position = 'absolute';
                // div.style.width = 100;
                // div.style.height = 100;
                // div.innerHTML = "hi there!";
                // div.style.top = -1000;
                // div.style.left = -1000;
             
                return {
                    element: div,
                    parent: false,
                    position: new THREE.Vector3(0,0,0),
                    setHTML: function(html) {
                        this.element.innerHTML = html;
                    },
                    setParent: function(threejsobj) {
                        this.parent = threejsobj;
                    },
                    updatePosition: function() {
                        if(parent) {
                            this.position.copy(this.parent.position);
                        }
                        
                        var coords2d = this.get2DCoords(this.position,camera);
                        this.element.style.left = coords2d.x + 'px';
                        this.element.style.top = coords2d.y + 'px';
                        coord2dlabel.push(contW + 'px ;' +  contH + 'px');
                        //coord2dlabel.push(coords2d.x + 'px ;' +  coords2d.y + 'px');
                        scope.coord2dlabel = coord2dlabel;
                    },
                    get2DCoords: function(position, camera) {
                        var vector = position.project(camera);
                        vector.x = (vector.x + 1)/2 * contW;
                        vector.y = -(vector.y - 1)/2 * contH + 25;
                        return vector;
                    }
                };
            };

            scope.nodeloadsdrawing = function () {
                    //Deleting all old nodeloads
                    for (var i = nodeloadsFx.length - 1; i >= 0; i--) {
                        scene.remove(nodeloadsFx[i])
                    }
                    nodeloadsFx.length = 0;

                    for (var i = nodeloadsFy.length - 1; i >= 0; i--) {
                        scene.remove(nodeloadsFy[i])
                    }
                    nodeloadsFy.length = 0;

                    for (var i = nodeloadsFz.length - 1; i >= 0; i--) {
                        scene.remove(nodeloadsFz[i])
                    }
                    nodeloadsFz.length = 0;

                    var dirFx, dirFy, dirFz, dirMx, dirMy, dirMz;
                    var originFx, originFy, originFz, originMx, originMy, originMz;
                    var lengthFx, lengthFy, lengthFz, olengthMx, lengthMy, lengthMz;
                    

                    var ratio = 1;

                    var color = 'red';
                    for (var i = 0; i < scope.listnodeloads.length; i++) {
                        //ForceX
                        if (scope.listnodeloads[i].ForceX > 0) {
                            dirFx = new THREE.Vector3(1, 0, 0);
                            for (var j = 0; j < scope.listnodes.length; j++) {
                                if (scope.listnodes[j]._id === scope.listnodeloads[i].NodeId) {
                                    lengthFx = scope.listnodeloads[i].ForceX/ratio;
                                    originFx = new THREE.Vector3(scope.listnodes[j].X - lengthFx, scope.listnodes[j].Y, scope.listnodes[j].Z);
                                }
                            }
                        } else {
                            dirFx = new THREE.Vector3(-1, 0, 0);
                            for (var j = 0; j < scope.listnodes.length; j++) {
                                if (scope.listnodes[j]._id === scope.listnodeloads[i].NodeId) {
                                    lengthFx = -scope.listnodeloads[i].ForceX / ratio;
                                    originFx = new THREE.Vector3(scope.listnodes[j].X + lengthFx, scope.listnodes[j].Y, scope.listnodes[j].Z);
                                }
                            }
                        }
                        //ForceY
                        if (scope.listnodeloads[i].ForceY > 0) {
                            dirFy = new THREE.Vector3(0, 1, 0);
                            for (var j = 0; j < scope.listnodes.length; j++) {
                                if (scope.listnodes[j]._id === scope.listnodeloads[i].NodeId) {
                                    lengthFy = scope.listnodeloads[i].ForceY/ratio;
                                    originFy = new THREE.Vector3(scope.listnodes[j].X, scope.listnodes[j].Y - lengthFy, scope.listnodes[j].Z);
                                }
                            }
                        } else {
                            dirFy = new THREE.Vector3(0, -1, 0);
                            for (var j = 0; j < scope.listnodes.length; j++) {
                                if (scope.listnodes[j]._id === scope.listnodeloads[i].NodeId) {
                                    lengthFy = -scope.listnodeloads[i].ForceY / ratio;
                                    originFy = new THREE.Vector3(scope.listnodes[j].X, scope.listnodes[j].Y + lengthFy, scope.listnodes[j].Z);
                                }
                            }
                        }
                        //ForceZ
                        if (scope.listnodeloads[i].ForceZ > 0) {
                            dirFz = new THREE.Vector3(0, 0, 1);
                            for (var j = 0; j < scope.listnodes.length; j++) {
                                if (scope.listnodes[j]._id === scope.listnodeloads[i].NodeId) {
                                    lengthFz = scope.listnodeloads[i].ForceZ / ratio;
                                    originFz = new THREE.Vector3(scope.listnodes[j].X, scope.listnodes[j].Y, scope.listnodes[j].Z - lengthFz);
                                }
                            }
                        } else {
                            dirFz = new THREE.Vector3(0, 0, -1);
                            for (var j = 0; j < scope.listnodes.length; j++) {
                                if (scope.listnodes[j]._id === scope.listnodeloads[i].NodeId) {
                                    lengthFz = -scope.listnodeloads[i].ForceZ/ratio;
                                    originFz = new THREE.Vector3(scope.listnodes[j].X, scope.listnodes[j].Y, scope.listnodes[j].Z + lengthFz);
                                }
                            }
                        }

                        nodeloadsFx[i] = new THREE.ArrowHelper(dirFx, originFx, lengthFx, color);
                        scene.add(nodeloadsFx[i]);
                        nodeloadsFy[i] = new THREE.ArrowHelper(dirFy, originFy, lengthFy, color);
                        scene.add(nodeloadsFy[i]);
                        nodeloadsFz[i] = new THREE.ArrowHelper(dirFz, originFz, lengthFz, color);
                        scene.add(nodeloadsFz[i]);
                    }
            };
            // Begin
            scope.init();
            scope.animate();
        }
    };
});





