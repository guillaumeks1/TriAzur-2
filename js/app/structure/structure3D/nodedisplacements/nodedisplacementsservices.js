﻿var NodeDisplacementsServices = angular.module('NodeDisplacementsServices', [])

.factory('NodeDisplacementsService', function ($http, $q) {
    var fac = {};
    fac.GetListNodeDisplacements = function () {
        return $http.get('/NodeDisplacementsData/ListNodeDisplacements');
    }
    fac.GetDetailNodeDisplacements = function (data) {
        return $http.get('/NodeDisplacementsData/DetailsNodeDisplacements', { params: data });
    }
    return fac;
})

