﻿var NodeDisplacementsDirectives = angular.module('NodeDisplacementsDirectives', ['NodeDisplacementsServices'])

.directive('trNodedisplacementslist', function (NodeDisplacementsService) {
    return {
        restrict: 'A',
        templateUrl: '/Scripts/AngularJs/Structure/Structure3D/Templates/NodeDisplacements/ListNodeDisplacementTemplate.html',
        controller: ['$scope', function trNodeDisplacementslistCtrl($scope) {
            //nodedisplacements window control
            $('#windownodedisplacements').click(function () {
                $('#inputsectioncard').toggleClass("col-md-12");
                $('#outputsectioncard').toggle();
                $('#windownodes').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
                $('#windowmembers').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
                $('#windownodeloads').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
                $('#windownmemberloads').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
                $('#windownodedisplacements').toggleClass("glyphicon-circle-arrow-left glyphicon-circle-arrow-right");
            });

            //In order to control the visibility of tr-nodedisplacementslist
            $scope.nodedisplacementslistvisibility = true;

            //List of nodedisplacements
            $scope.listnodedisplacements = [];

            $scope.Listnodedisplacement = function () {
                NodeDisplacementsService.GetListNodeDisplacements().then(function (d) {
                    //$scope.listnodedisplacements = d.data;
                    //$scope.NodeDisplacement = $scope.listnodedisplacements[id];
                    $scope.ListNodeDisplacementsmessage = d.data;
                }, function (error) {
                    $scope.ListNodeDisplacementsmessage = "Error";
                });
            }
            $scope.postaddnodedisplacement = function () {
                $scope.Listnodedisplacement();
            }
        }]
    };
})

