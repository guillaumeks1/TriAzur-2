var Structure3DControllers = angular.module('Structure3DControllers', ['Structure3DDirectives', 'Structure3DServices'])

.controller('Structure3DCtrl', function ($scope, Structure3DService) {
    //-----------------------------------------------------------------------------------------------------------------------
    //Style
    //-----------------------------------------------------------------------------------------------------------------------
    $(document).ready(function () {
        if ($(this).width() < 970) {
            $('#menuinputhorizontalcard').show();
            $('#outputsectioncard').hide();
        } else {
            $('#menuinputhorizontalcard').show();
            $('#outputsectioncard').show();
        }
    });

    $(window).resize(function () {
        if ($(this).width() < 970) {
            $('#menuinputhorizontalcard').show();
             $('#outputsectioncard').hide();
        } else {
            $('#menuinputhorizontalcard').show();
            $('#outputsectioncard').show();
        }
    });

    //-----------------------------------------------------------------------------------------------------------------------
    //StructuralAnalysis: Controls NavBar
    //-----------------------------------------------------------------------------------------------------------------------
    $scope.tab = 1;


    $scope.setTab = function (newTab) {
        $scope.tab = newTab;
    };

    $scope.isSet = function (tabNum) {
        return $scope.tab === tabNum;
    };

    $scope.canvasWidth = 400;
    $scope.canvasHeight = 400;
    $scope.dofillcontainer = true;
    $scope.threedimensionsstructure = false;
    $scope.scale = 1;
    $scope.materialType = 'wireframe';
    $scope.centerPosition = true;
    $scope.message = [];

    $scope.UnitLength = "m";
    $scope.UnitTime = "s";
    $scope.UnitWeight = "kg";
    $scope.UnitLoad = "kN";
    $scope.UnitTemperature = "°C";

    //-----------------------------------------------------------------------------------------------------------------------
    //StructuralAnalysis: Calculation
    //-----------------------------------------------------------------------------------------------------------------------
    
    //structure3danalysis

    $scope.Listnode = function(){
        Structure3DService.GetListNodes().then(function (d) {
            $scope.listnodes = d.data;
            }, function (error) {
            $scope.ListNodesmessage = "Error";
        });
    };


    $scope.structure3danalysis = function () {

        $scope.Listnode();
       
        // var listsec = [];
        // Structure3DService.GetListSections().then(function (d) {
        //         listsec = d.data;
        //         $scope.ListSectionsmessage = listsec;
        //         SectionsService.GetDataStructures3d(listsec).then(function (d) {
        //             if (d.data == "Success") {
        //                 $scope.ListSectionsmessage = "";
        //                 $scope.Listsection(0);
        //             } else {
        //                 $scope.ListSectionsmessage = d.data;
        //             }   
        //         }, function (error) {
        //             $scope.ListSectionsmessage = "Error";
        //         });      
        // }, function (error) {
        //     $scope.ListSectionsmessage = "Error";
        // });

        Structure3DService.GetDataStructures3d($scope.listnodes).then(function (d) {
            if (d.data == "Success") {
                $scope.ListSectionsmessage = "";
                $scope.Listsection(0);
            } else {
                $scope.ListSectionsmessage = d.data;
            }   
        }, function (error) {
            $scope.ListSectionsmessage = "Error";
        }); 
    };


});

