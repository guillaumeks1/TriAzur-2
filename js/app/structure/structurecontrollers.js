var StructureControllers = angular.module('StructureControllers', ['StructureDirectives','StructureServices', 'TriAzurControllers'
,'RectSectionUnderBendingMomentEC2Controllers','RectSectionUnderBendingMomentControllers'
,'ConcreteSectionAnalysisControllers','Structure3DControllers'])

.controller('TriAzurStructureCtrl', function ($scope, UnitsService, ConversionService, TranslationService) {

    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //                                            UNITS & CONVERSION
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------

    $scope.conversionUnit = ConversionService.conversionUnit;

    $scope.UnitsOptions = UnitsService.getUnitOptions();

    $scope.UnitTimeOptions = UnitsService.UnitTimeOptions;

    $scope.UnitLengthOptions = UnitsService.UnitLengthOptions;

    $scope.UnitAreaOptions = UnitsService.UnitAreaOptions;

    $scope.UnitStressOptions = UnitsService.UnitStressOptions;

    $scope.UnitMomentOptions = UnitsService.UnitMomentOptions;

    $scope.UnitDensityOptions = UnitsService.UnitDensityOptions;

})

.filter('conversion', function (ConversionService) {
    var conversionUnit = ConversionService.conversionUnit;

    return function (input, from, to, pow) {
        var precision = 3;
        return Math.round((input * Math.pow(conversionUnit[from][to], pow)) * Math.pow(10, precision)) / Math.pow(10, precision);
    };
})


.filter('round', function () {
    return function (input, precision) {
        return Math.round(input * Math.pow(10, precision)) / Math.pow(10, precision);
    };
});


