﻿var ConcreteSectionAnalysisDirectives = angular.module('ConcreteSectionAnalysisDirectives', [])

//-----------------------------------------------------------------------------------------------------
//                                                  INPUTS
//-----------------------------------------------------------------------------------------------------

.directive('trConcretesectionanalysisoptions', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/reinforcedconcrete/concretesectionanalysis/templates/Inputs/ConcreteSectionAnalysisOptionsTemplate.html',
        //scope:{},
        controller: ['$scope', function trConcretesectionanalysisoptionsCtrl($scope) {

            $scope.UnitsOptions = [
            {
                name: 'm-kN-s',
                value: 'm-kN-s'
            },
            {
                name: 'm-MN-s',
                value: 'm-MN-s'
            },
            {
                name: 'cm-MN-s',
                value: 'cm-MN-s'
            },
            {
                name: 'inch-kips-s',
                value: 'inch-kips-s'
            }];

            $scope.Units = $scope.UnitsOptions[1];

            $scope.UnitsText = function () {
                switch ($scope.Units.value) {
                    case "m-kN-s":
                        $scope.UnitLength = "m";
                        $scope.UnitArea = "m²";
                        $scope.UnitTime = "s";
                        $scope.UnitWeight = "kg";
                        $scope.UnitForce = "kN";
                        $scope.UnitMoment = "kN.m";
                        $scope.UnitStress = "kPa";
                        $scope.UnitTemperature = "°C";
                        break;
                    case "m-MN-s":
                        $scope.UnitLength = "m";
                        $scope.UnitArea = "m²";
                        $scope.UnitTime = "s";
                        $scope.UnitWeight = "kg";
                        $scope.UnitForce = "MN";
                        $scope.UnitMoment = "MN.m";
                        $scope.UnitStress = "MPa";
                        $scope.UnitTemperature = "°C";
                        break;
                    case "cm-MN-s":
                        $scope.UnitLength = "cm";
                        $scope.UnitArea = "cm²";
                        $scope.UnitTime = "s";
                        $scope.UnitWeight = "kg";
                        $scope.UnitForce = "MN";
                        $scope.UnitMoment = "MN.cm";
                        $scope.UnitStress = "MPa";
                        $scope.UnitTemperature = "°C";
                        break;
                    case "inch-kips-s":
                        $scope.UnitLength = "inch";
                        $scope.UnitArea = "inch²";
                        $scope.UnitTime = "s";
                        $scope.UnitWeight = "kg";
                        $scope.UnitForce = "kips";
                        $scope.UnitMoment = "kips.inch";
                        $scope.UnitTemperature = "°C";
                        break;
                    default:
                        $scope.UnitLength = "m";
                        $scope.UnitArea = "m²";
                        $scope.UnitTime = "s";
                        $scope.UnitWeight = "kg";
                        $scope.UnitForce = "MN";
                        $scope.UnitMoment = "MN.m";
                        $scope.UnitStress = "MPa";
                        $scope.UnitTemperature = "°C";
                        break;
                }
            };

        }]
    };
})

.directive('trConcretesectionanalysisrectangularsection', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/reinforcedconcrete/concretesectionanalysis/templates/Inputs/ConcreteSectionGeometry/RecConcreteSectionTemplate.html',
        controller: ['$scope', function trConcretesectionanalysisrectangularsectionCtrl($scope) {

            //---------------------------------------------------------------------------------------------
            //Concrete Section Width
            //---------------------------------------------------------------------------------------------
            $scope.bAff = 1;
            $scope.bUnit = $scope.UnitLengthOptions[0];
            $scope.b = function () {
                return ($scope.bAff * $scope.conversionUnit[$scope.bUnit.value]['m']);
            };
            $scope.$watch('bUnit', function (newValue, oldValue) {
                $scope.bAff = $scope.bAff * $scope.conversionUnit[oldValue.value][newValue.value];
            });

            //---------------------------------------------------------------------------------------------
            //Concrete Section Height
            //---------------------------------------------------------------------------------------------
            $scope.hAff = 1;
            $scope.hUnit = $scope.UnitLengthOptions[0];
            $scope.h = function () {
                return ($scope.hAff * $scope.conversionUnit[$scope.hUnit.value]['m']);
            };
            $scope.$watch('hUnit', function (newValue, oldValue) {
                $scope.hAff = $scope.hAff * $scope.conversionUnit[oldValue.value][newValue.value];
            });

            $scope.$watch('bAff + hAff', function (newvalue, oldvalue) {
                if ($scope.isStrictPositive($scope.bAff) && $scope.isStrictPositive($scope.hAff)) {
                    $scope.ConcreteExt =[{
                        Yext: 0,
                        Zext: 0
                    }, {
                        Yext: $scope.b(),
                        Zext: 0
                    }, {
                        Yext: $scope.b(),
                        Zext: $scope.h()
                    }, {
                        Yext: 0,
                        Zext: $scope.h()
                    }, {
                        Yext: 0,
                        Zext: 0
                    }];
                    $scope.ConcreteInt = [{ Yint: 0, Zint: 0 }];
                }
            });
        }]
    }
})

.directive('trConcretesectionanalysistsection', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/reinforcedconcrete/concretesectionanalysis/templates/Inputs/ConcreteSectionGeometry/TConcreteSectionTemplate.html',
        controller: ['$scope', function trConcretesectionanalysisTsectionCtrl($scope) {

            //---------------------------------------------------------------------------------------------
            //Concrete section dimensions
            //---------------------------------------------------------------------------------------------
            $scope.bAfft = 1;
            $scope.bUnitt = $scope.UnitLengthOptions[0];
            $scope.bt = function () {
                return ($scope.bAfft * $scope.conversionUnit[$scope.bUnitt.value]['m']);
            };
            $scope.$watch('bUnitt', function (newValue, oldValue) {
                $scope.bAfft = $scope.bAfft * $scope.conversionUnit[oldValue.value][newValue.value];
            });

            $scope.b0Afft = 0.5;
            $scope.b0Unitt = $scope.UnitLengthOptions[0];
            $scope.b0t = function () {
                return ($scope.b0Afft * $scope.conversionUnit[$scope.b0Unitt.value]['m']);
            };
            $scope.$watch('b0Unitt', function (newValue, oldValue) {
                $scope.b0Afft = $scope.b0Afft * $scope.conversionUnit[oldValue.value][newValue.value];
            });

            $scope.hAfft = 2;
            $scope.hUnitt = $scope.UnitLengthOptions[0];
            $scope.ht = function () {
                return ($scope.hAfft * $scope.conversionUnit[$scope.hUnitt.value]['m']);
            };
            $scope.$watch('hUnitt', function (newValue, oldValue) {
                $scope.hAfft = $scope.hAfft * $scope.conversionUnit[oldValue.value][newValue.value];
            });

            $scope.h0Afft = 1;
            $scope.h0Unitt = $scope.UnitLengthOptions[0];
            $scope.h0t = function () {
                return ($scope.h0Afft * $scope.conversionUnit[$scope.h0Unitt.value]['m']);
            };
            $scope.$watch('h0Unitt', function (newValue, oldValue) {
                $scope.h0Afft = $scope.h0Afft * $scope.conversionUnit[oldValue.value][newValue.value];
            });

            //---------------------------------------------------------------------------------------------
            //Concrete section drawings
            //---------------------------------------------------------------------------------------------

            $scope.$watch('bAfft + hAfft + b0Afft + h0Afft', function (newvalue, oldvalue) {
                if ($scope.isStrictPositive($scope.bAfft) && $scope.isStrictPositive($scope.hAfft) && $scope.isStrictPositive($scope.b0Afft) && $scope.isStrictPositive($scope.h0Afft)) {
                    $scope.ConcreteExt = [{
                            Yext: Math.abs($scope.bt()/2-$scope.b0t()/2),
                            Zext: 0
                        }, {
                            Yext: Math.abs($scope.bt() / 2 + $scope.b0t() / 2),
                            Zext: 0
                        }, {
                            Yext: Math.abs($scope.bt() / 2 + $scope.b0t() / 2),
                            Zext: Math.abs($scope.ht() - $scope.h0t())
                        }, {
                            Yext: Math.abs($scope.bt()),
                            Zext: Math.abs($scope.ht() - $scope.h0t())
                        }, {
                            Yext: Math.abs($scope.bt()),
                            Zext: Math.abs($scope.ht())
                        }, {
                            Yext: 0,
                            Zext: Math.abs($scope.ht())
                        }, {
                            Yext: 0,
                            Zext: Math.abs($scope.ht() - $scope.h0t())
                        }, {
                            Yext: Math.abs($scope.bt() / 2 - $scope.b0t() / 2),
                            Zext: Math.abs($scope.ht() - $scope.h0t())
                        }, {
                            Yext: Math.abs($scope.bt() / 2 - $scope.b0t() / 2),
                            Zext: 0
                    }];
                }

                $scope.ConcreteInt = [{ Yint: 0, Zint: 0 }];
            });
        }]
    }
})

.directive('trConcretesectionanalysisgeometry', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/reinforcedconcrete/concretesectionanalysis/templates/Inputs/ConcreteSectionAnalysisGeometryTemplate.html',
        //scope:{},
        controller: ['$scope', function trConcretesectionanalysisgeometryCtrl($scope, $parse) {
            $scope.ConcreteExt = [
            {
                Yext: 0,
                Zext: 0
            }, {
                Yext: 0,
                Zext: 1
            }, {
                Yext: 1,
                Zext: 1
            }, {
                Yext: 1,
                Zext: 0
            }, {
                Yext: 0,
                Zext: 0
            }];

            $scope.ConcreteInt = [
            {
                Yint: 0.1,
                Zint: 0.1
            }, {
                Yint: 0.1,
                Zint: 0.9
            }, {
                Yint: 0.9,
                Zint: 0.9
            }, {
                Yint: 0.9,
                Zint: 0.1
            }, {
                Yint: 0.1,
                Zint: 0.1
            }];
            $scope.srcimageconcretesection = [
                "/Img/Structure/ConcreteStructure/ReinforcedStructure/rectangularconcretesection.svg",
                "/Img/Structure/ConcreteStructure/ReinforcedStructure/Tconcretesection.svg",
                "/Img/Structure/ConcreteStructure/ReinforcedStructure/Iconcretesection.svg",
                "/Img/Structure/ConcreteStructure/ReinforcedStructure/circleconcretesection.svg",
                "/Img/Structure/ConcreteStructure/ReinforcedStructure/hollowcircleconcretesection.svg",
                "/Img/Structure/ConcreteStructure/ReinforcedStructure/randomconcretesection.svg"
            ];

            $scope.rectangularconcretesectiongeometry = function () {
                $scope.concretesectiongeometryvisibility = false;
                $scope.rectangularconcretesectiongeometryvisibility = true;
            };
            // //---------------------------------------------------------------------------------------------
            // //Concrete section commands
            // //---------------------------------------------------------------------------------------------
            $scope.rectangularsectionback = function () {
                $scope.rectangularconcretesectiongeometryvisibility = false;
                $scope.concretesectiongeometryvisibility = true;

            };

            $scope.Tconcretesectiongeometry = function () {
                $scope.concretesectiongeometryvisibility = false;
                $scope.Tconcretesectiongeometryvisibility = true;
            };

            //---------------------------------------------------------------------------------------------
            //Concrete section commands
            //---------------------------------------------------------------------------------------------
            $scope.tsectionback = function () {
                $scope.Tconcretesectiongeometryvisibility = false;
                $scope.concretesectiongeometryvisibility = true;
            };


            $scope.columns = ['Yext', 'Zext'];

            $scope.val = function () {
                return $scope.ConcreteExt[1].Yext;
            };

            $scope.AddPointExt = function () {
                $scope.ConcreteExt.push({ Yext: 3, Zext: 3 });
            };


            ////---------------------------------------------------------------------------------------------
            ////Concrete Section Width
            ////---------------------------------------------------------------------------------------------
            //$scope.bwAff = 1;
            //$scope.bwUnit = $scope.UnitLengthOptions[0];
            //$scope.bw = function () {
            //    return ($scope.bwAff * $scope.conversionUnit[$scope.bwUnit.value]['m']);
            //};
            //$scope.$watch('bwUnit', function (newValue, oldValue) {
            //    $scope.bwAff = $scope.bwAff * $scope.conversionUnit[oldValue.value][newValue.value];
            //});

            ////---------------------------------------------------------------------------------------------
            ////Concrete Section Height
            ////---------------------------------------------------------------------------------------------
            //$scope.hAff = 1;
            //$scope.hUnit = $scope.UnitLengthOptions[0];
            //$scope.h = function () {
            //    return ($scope.hAff * $scope.conversionUnit[$scope.hUnit.value]['m']);
            //};
            //$scope.$watch('hUnit', function (newValue, oldValue) {
            //    $scope.hAff = $scope.hAff * $scope.conversionUnit[oldValue.value][newValue.value];
            //});


            //---------------------------------------------------------------------------------------------
            //Reinforcement Bottom Cover
            //---------------------------------------------------------------------------------------------
            $scope.cAff = 50;
            $scope.cUnit = $scope.UnitLengthOptions[2];
            $scope.c = function () {
                return ($scope.cAff * $scope.conversionUnit[$scope.cUnit.value]['m']);
            };
            $scope.$watch('cUnit', function (newValue, oldValue) {
                $scope.cAff = $scope.cAff * $scope.conversionUnit[oldValue.value][newValue.value];
            });

            //---------------------------------------------------------------------------------------------
            //Reinforcement Top Cover
            //---------------------------------------------------------------------------------------------
            $scope.cprimeAff = 50;
            $scope.cprimeUnit = $scope.UnitLengthOptions[2];
            $scope.cprime = function () {
                return ($scope.cprimeAff * $scope.conversionUnit[$scope.cprimeUnit.value]['m']);
            };
            $scope.$watch('cprimeUnit', function (newValue, oldValue) {
                $scope.cprimeAff = $scope.cprimeAff * $scope.conversionUnit[oldValue.value][newValue.value];
            });


            $scope.d = function () {
                return ($scope.h() - $scope.c());
            };

            $scope.dprime = function () {
                return ($scope.cprime());
            };

        }]
    };
})

.directive('trConcretesectionanalysisconcrete', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/reinforcedconcrete/concretesectionanalysis/templates/Inputs/ConcreteSectionAnalysisConcreteTemplate.html',
        //scope:{},
        controller: ['$scope', function trConcretesectionanalysisconcreteCtrl($scope) {
            //---------------------------------------------------------------------------------------------
            //Concrete Strength
            //---------------------------------------------------------------------------------------------
            $scope.fckAff = 17;
            $scope.fckUnit = $scope.UnitStressOptions[2];
            $scope.fck = function () {
                return ($scope.fckAff * Math.pow($scope.conversionUnit[$scope.fckUnit.value1]['MN'], $scope.fckUnit.pow1) / Math.pow($scope.conversionUnit[$scope.fckUnit.value2]['m'], $scope.fckUnit.pow2));
            };
            $scope.$watch('fckUnit', function (newValue, oldValue) {
                $scope.fckAff = $scope.fckAff * Math.pow($scope.conversionUnit[oldValue.value1][newValue.value1], newValue.pow1) / Math.pow($scope.conversionUnit[oldValue.value2][newValue.value2], newValue.pow2);
            });

            $scope.gammac = 1.5;

            $scope.alphacc = 1;

            $scope.eta = function () {
                if ($scope.fck() < 50) {
                    return (1);
                } else {
                    return (1 - ($scope.fck() - 50) / 200);
                }
            };


            $scope.fcd = function () {
                return ($scope.eta() * $scope.alphacc * $scope.fck() / $scope.gammac);
            };

            $scope.epsicu2 = function () {
                if ($scope.fck() < 50) {
                    return (3.5 / 1000);
                } else {
                    return ((2.6 + 35 * (Math.pow((90 - $scope.fck()) / 100, 4))) / 1000);
                }
            };

            $scope.lambda = function () {
                if ($scope.fck() < 50) {
                    return (0.8);
                } else {
                    return (0.8 - ($scope.fck() - 50) / 400);
                }
            };

            $scope.fctm = function () {
                return (0.3 * Math.pow($scope.fck(), 2 / 3));
            };

        }]
    };
})

.directive('trConcretesectionanalysissteel', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/reinforcedconcrete/concretesectionanalysis/templates/Inputs/ConcreteSectionAnalysisSteelTemplate.html',
        //scope:{},
        controller: ['$scope', function trConcretesectionanalysissteelCtrl($scope) {
            $scope.fykAff = 500;
            $scope.fykUnit = $scope.UnitStressOptions[2];
            $scope.fyk = function () {
                return ($scope.fykAff * Math.pow($scope.conversionUnit[$scope.fykUnit.value1]['MN'], $scope.fykUnit.pow1) / Math.pow($scope.conversionUnit[$scope.fykUnit.value2]['m'], $scope.fykUnit.pow2));
            };
            $scope.$watch('fykUnit', function (newValue, oldValue) {
                $scope.fykAff = $scope.fykAff * Math.pow($scope.conversionUnit[oldValue.value1][newValue.value1], newValue.pow1) / Math.pow($scope.conversionUnit[oldValue.value2][newValue.value2], newValue.pow2);
            });

            $scope.gammas = 1.15;

            $scope.EsAff = 200000;
            $scope.EsUnit = $scope.UnitStressOptions[2];
            $scope.Es = function () {
                return ($scope.EsAff * Math.pow($scope.conversionUnit[$scope.EsUnit.value1]['MN'], $scope.EsUnit.pow1) / Math.pow($scope.conversionUnit[$scope.EsUnit.value2]['m'], $scope.EsUnit.pow2));
            };
            $scope.$watch('EsUnit', function (newValue, oldValue) {
                $scope.EsAff = $scope.EsAff * Math.pow($scope.conversionUnit[oldValue.value1][newValue.value1], newValue.pow1) / Math.pow($scope.conversionUnit[oldValue.value2][newValue.value2], newValue.pow2);
            });

            $scope.fyd = function () {
                return ($scope.fyk() / $scope.gammas);
            };

            $scope.epsiyd = function () {
                return ($scope.fyd() / $scope.Es());
            };
            $scope.DuctilityClassOptions = [{
                name: 'A',
                value: 'A'
            }, {
                name: 'B',
                value: 'B'
            }, {
                name: 'C',
                value: 'C'
            }];

            $scope.DuctilityClass = $scope.DuctilityClassOptions[0];

            $scope.epsiuk = function () {
                var ductilityClass = $scope.DuctilityClass.value;
                if (ductilityClass === 'B') {
                    return (0.05);
                }
                if (ductilityClass === 'C') {
                    return (0.075);
                }
                if (ductilityClass === 'A') {
                    return (0.025);
                } else {
                    return (0.025);
                }
            };

            $scope.k = function () {
                var ductilityClass = $scope.DuctilityClass.value;
                if (ductilityClass === 'B') {
                    return (1.08);
                }
                if (ductilityClass === 'C') {
                    return (1.15);
                }
                if (ductilityClass === 'A') {
                    return (1.05);
                } else {
                    return (1.05);
                }
            };

            $scope.epsiud = function () {
                return (0.9 * $scope.epsiuk());
            };

            $scope.A = function () {
                return ($scope.fyd() * ($scope.k() * $scope.epsiyd() - $scope.epsiuk()) / ($scope.epsiyd() - $scope.epsiuk()));
            };

            $scope.B = function () {
                return (($scope.k() * $scope.fyd() - $scope.fyd()) / ($scope.epsiuk() - $scope.epsiyd()));
            };

        }]
    };
})

.directive('trConcretesectionanalysisloads', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/reinforcedconcrete/concretesectionanalysis/templates/Inputs/ConcreteSectionAnalysisLoadsTemplate.html',
        //scope:{},
        controller: ['$scope', function trConcretesectionanalysisloadsCtrl($scope) {
            $scope.MedAff = 1;
            $scope.MedUnit = $scope.UnitMomentOptions[2];
            $scope.Med = function () {
                return ($scope.MedAff * Math.pow($scope.conversionUnit[$scope.MedUnit.value1]['MN'], $scope.MedUnit.pow1) * Math.pow($scope.conversionUnit[$scope.MedUnit.value2]['m'], $scope.MedUnit.pow2));
            };
            $scope.$watch('MedUnit', function (newValue, oldValue) {
                $scope.MedAff = $scope.MedAff * Math.pow($scope.conversionUnit[oldValue.value1][newValue.value1], newValue.pow1) * Math.pow($scope.conversionUnit[oldValue.value2][newValue.value2], newValue.pow2);
            });

        }]
    };
})

//-----------------------------------------------------------------------------------------------------
//                                                  OUTPUTS
//-----------------------------------------------------------------------------------------------------


.directive('trConcretesectionanalysissectionchart', function () {
    return {
        restrict: 'A',
        scope: {
            title: '=',
            concreteext: '=',
            concreteint: '='
        },
        templateUrl: '/js/app/structure/reinforcedconcrete/concretesectionanalysis/templates/Outputs/ConcreteSectionAnalysisSectionChartTemplate.html',

    };
})

.directive('trConcretesectionanalysischart', function () {
    return {
        restrict: 'A',
        scope: {
            title: '=',
            unitx: '=',
            unity:'=',
            concreteext: '=',
            concreteint: '='
        },
        link: function link(scope, element, attrs) {

            google.charts.load('current', { packages: ['corechart'] });
            google.charts.setOnLoadCallback(drawChart);

            $(document).ready(function () {
                $(window).resize(function () {
                    drawChart();
                });
                $('.redraw').click(function () {
                    drawChart();
                });
                $('#menu-toggle').mouseleave(function () {
                    drawChart();
                });

                //$('.redraw').hover(function () {
                //    drawChart();
                //});
            });

            function drawChart() {
                var unitx = String(scope.unitx);
                var unity = String(scope.unity);

                chartdata = []

                var Header = ['Yext', 'Zext', 'Zint', 'Zint'];

                chartdata.push(Header);

                var Yextmax = 0;
                var Yextmin = 0;
                var Zextmax = 0;
                var Zextmin = 0;
                angular.forEach(scope.concreteext, function (value, key) {
                    Yextmax = Math.max(Yextmax, value.Yext);
                    Yextmin = Math.min(Yextmin, value.Yext);
                    Zextmax = Math.max(Zextmax, value.Zext);
                    Zextmin = Math.min(Zextmin, value.Zext);
                    chartdata.push([value.Yext, value.Zext, null, null]);
                });

                angular.forEach(scope.concreteint, function (value, key) {
                    chartdata.push([value.Yint, null, value.Zint, value.Zint]);
                });
                //chartdata.push([scope.concreteext[0].Yext,scope.concreteext[0].Zext]);

                var data = google.visualization.arrayToDataTable(chartdata);

                var options = {
                    title: scope.title,
                    isStacked: true,
                    hAxis: {
                        title: 'X(' + unitx + ")",
                        titleTextStyle: { color: '#333' },
                        viewWindow: {
                            min: 0,
                            //max: scope.sectionwidth()
                        },
                        ticks: [
                            Yextmax / 2,
                            { v: Yextmax, f: Yextmax },
                            { v: Math.max(Yextmax, Zextmax) + 0.2, f: '' }
                        ]
                    },
                    vAxis: {
                        title: 'Y(' + unity + ")",
                        viewWindow: {
                            min: 0,
                            //max: scope.sectionheight()
                        },
                        ticks: [
                            Zextmax / 2,
                            { v: Zextmax, f: Zextmax },
                            { v: Math.max(Yextmax, Zextmax), f: '' }
                        ]
                    },
                    series: {
                        0: { color: 'grey', visibleInLegend: false, lineWidth: 2 },
                        1: { color: 'white', visibleInLegend: false, lineWidth: 0, areaOpacity: 1 },
                        2: { color: 'grey', visibleInLegend: false, type: 'line' }
                    }
                };

                var chart = new google.visualization.AreaChart(element[0]);
                chart.draw(data, options);
            }

            scope.$watch(function () { return angular.toJson([scope.concreteext]) }, function (newVal) {
                drawChart();
            }, true);

            scope.$watch(function () { return angular.toJson([scope.concreteint]) }, function (newVal) {
                drawChart();
            }, true);

        }
    };
})

.directive('trConcretesectionanalysisreinforcement', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/reinforcedconcrete/concretesectionanalysis/templates/Outputs/ConcreteSectionAnalysisReinforcementTemplate.html',
        //scope:{},
        controller: ['$scope', function trConcretesectionanalysisreinforcementCtrl($scope) {
            $scope.mucu = function () {
                return ($scope.Med() / ($scope.bw() * Math.pow($scope.d(), 2) * $scope.fcd()));
            };

            $scope.alphals = function () {
                return ($scope.epsicu2() / ($scope.epsicu2() + $scope.epsiyd()));
            };

            $scope.muls = function () {
                return ($scope.lambda() * $scope.alphals() * (1 - $scope.lambda() * $scope.alphals() / 2));
            };

            $scope.alphau = function () {
                if ($scope.mucu() <= $scope.muls()) {
                    return (1 / $scope.lambda() * (1 - Math.pow((1 - 2 * $scope.mucu()), 0.5)));
                } else {
                    return (0);
                }
            };

            $scope.Mls = function () {
                return ($scope.muls() * $scope.bw() * Math.pow($scope.d(), 2) * $scope.fcd());
            };

            $scope.alphaAB = function () {
                return ($scope.epsicu2() / ($scope.epsicu2() + $scope.epsiud()));
            };

            $scope.muAB = function () {
                return ($scope.lambda() * $scope.alphaAB() * (1 - $scope.lambda() * $scope.alphaAB() / 2));
            };

            $scope.epsis1 = function () {
                if ($scope.mucu() <= $scope.muls()) {
                    if ($scope.mucu() <= $scope.muAB()) {
                        return ($scope.epsiud());
                    } else {
                        return ($scope.epsicu2() * (1 - $scope.alphau()) / $scope.alphau());
                    }
                } else {
                    return ($scope.epsiyd());
                }
            };

            $scope.epsis2 = function () {
                return ($scope.epsicu2() * ($scope.alphals() - $scope.dprime() / $scope.d()) / $scope.alphals());
            };

            $scope.sigmas1 = function () {
                if ($scope.mucu() <= $scope.muls()) {
                    return ($scope.A() + $scope.B() * $scope.epsis1());
                } else {
                    return ($scope.fyd());
                }
            };

            $scope.sigmas2 = function () {
                if ($scope.epsis2() <= $scope.epsiyd()) {
                    return ($scope.Es() * $scope.epsis2());
                } else {
                    return ($scope.A() + $scope.B() * $scope.epsis2());
                }
            };

            $scope.zc = function () {
                if ($scope.mucu() <= $scope.muls()) {
                    return ($scope.d() * (1 - $scope.lambda() * $scope.alphau() / 2));
                } else {
                    return ($scope.d() * (1 - $scope.lambda() * $scope.alphals() / 2));
                }
            };

            $scope.As2 = function () {
                if ($scope.mucu() <= $scope.muls()) {
                    return (0);
                } else {
                    return (($scope.Med() - $scope.Mls()) / (($scope.d() - $scope.dprime()) * $scope.sigmas2()));
                }
            };
            $scope.As2Unit = $scope.UnitAreaOptions[1];

            $scope.As1 = function () {
                if ($scope.mucu() <= $scope.muls()) {
                    return (($scope.Med() / ($scope.sigmas1() * $scope.zc())));
                } else {
                    return (($scope.Mls() / ($scope.sigmas1() * $scope.zc()) + $scope.As2() * $scope.sigmas2() / $scope.sigmas1()));
                }
            };
            $scope.As1Unit = $scope.UnitAreaOptions[1];
        }]
    };
})

.directive('trConcretesectionanalysisdata', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/reinforcedconcrete/concretesectionanalysis/templates/Outputs/ConcreteSectionAnalysisDataTemplate.html',
        //scope:{},
    };
})