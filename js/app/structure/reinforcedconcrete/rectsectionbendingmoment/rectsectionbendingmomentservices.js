﻿var RectSectionUnderBendingMomentServices = angular.module('RectSectionUnderBendingMomentServices', [])

.factory('RectSectionUnderBendingMomentTranslationService', function () {
    var factory = {
        TranslationLanguage: {
            En: {
                //-----------------------------------------------------------------------------------------
                //                              RECTANGULAR SECTION UNDER BENDING MOMENT EC2
                //-----------------------------------------------------------------------------------------
                //InputCard1
                TextOptions : "Options",
                TextOptionsUnits : "Units",
                InfoOptionsUnits : "Units",
                //InputCard2
                TextGeometry : "Geometry",
                TextGeometrySectionWidth : "b",
                InfoGeometrySectionWidth : "Concrete Section Width",
                TextGeometrySectionHeight : "h",
                InfoGeometrySectionHeight : "Concrete Section Height",
                TextGeometryLowerReinforcementCover : "c",
                InfoGeometryLowerReinforcementCover : "Lower Reinforcement Cover",
                TextGeometryUpperReinforcementCover : "c'",
                InfoGeometryUpperReinforcementCover : "Upper Reinforcement Cover",
                //InputCard3
                TextConcrete : "Concrete",
                TextConcreteStrength : "fck",
                InfoConcreteStrength : "Concrete Strength",
                TextConcretePartialFactor : "ϒc",
                InfoConcretePartialFactor : "Concrete Partial Factor",
                TextConcreteCoefficient : "αcc",
                InfoConcreteCoefficient : "Concrete Coefficient",
                //InputCard4
                TextSteel : "Steel",
                TextSteelYieldStrength : "fyk",
                InfoSteelYieldStrength : "Steel Yield Stress",
                TextSteelPartialFactorforReinforcingorPrestressingSteel : "ϒs",
                InfotSteelPartialFactorforReinforcingorPrestressingSteel : "Partial Factor for Reinforcing or PrestressingSteel",
                TextSteelModulusofElasticityofReinforcingSteel : "Es",
                InfoSteelModulusofElasticityofReinforcingSteel : "Steel Modulus of Elasticity of ReinforcingSteel",
                TextSteelDuctilityClass : "Ductility class",
                InfoSteelDuctilityClass : "Steel Ductility class",
                //InputCard5
                TextBendingMoment : "Bending Moment",
                TextMoment : "Med",
                InfoBendingMoment : "Bending Moment",
                //OutputCard1
                TextSectionChart : "Concrete Section",
                //OutputCard2
                TextReinforcement : "Reinforcement",
                TextReinforcementTopReinforcement : "As,inf",
                InfoReinforcementTopReinforcement : "Top Reinforcement",
                TextReinforcementBottomReinforcement : "As,sup",
                InfoReinforcementBottomReinforcement : "Bottom Reinforcement"
            },
            Fr: {
                //-----------------------------------------------------------------------------------------
                //                              SECTION RECTANGULAIRE SOUS MOMENT A L'EC2
                //-----------------------------------------------------------------------------------------
                //InputCard1
                TextOptions : "Options",
                TextOptionsUnits : "Unitées",
                InfoOptionsUnits : "Unitées",
                //InputCard2
                TextGeometry : "Geometrie",
                TextGeometrySectionWidth : "b",
                InfoGeometrySectionWidth : "Largeur de la section en béton",
                TextGeometrySectionHeight : "h",
                InfoGeometrySectionHeight : "Hauteur de la section en béton",
                TextGeometryLowerReinforcementCover : "c",
                InfoGeometryLowerReinforcementCover : "Enrobage des armatures inférieures",
                TextGeometryUpperReinforcementCover : "c'",
                InfoGeometryUpperReinforcementCover : "Enrobage des armatures supérieures",
                //InputCard3
                TextConcrete : "Béton",
                TextConcreteStrength : "fck",
                InfoConcreteStrength : "Résistance caractéristique en compression du béton, mesurée sur Cylindre à 28jours",
                TextConcretePartialFactor : "ϒc",
                InfoConcretePartialFactor : "Coefficient partiel relatif du béton",
                TextConcreteCoefficient : "αcc",
                InfoConcreteCoefficient : "Coefficient",
                //InputCard4
                TextSteel : "Acier",
                TextSteelYieldStrength : "fyk",
                InfoSteelYieldStrength : "Limite caractéristique d'élasticité de l'acier de béton armé",
                TextSteelPartialFactorforReinforcingorPrestressingSteel : "ϒs",
                InfotSteelPartialFactorforReinforcingorPrestressingSteel : "Coefficient partiel relatif à l'acier de béton armé ou de précontrainte",
                TextSteelModulusofElasticityofReinforcingSteel : "Es",
                InfoSteelModulusofElasticityofReinforcingSteel : "Valeur de calcul du module d'élasticité de l'acier de béton armé",
                TextSteelDuctilityClass : "Classe de Ductilité",
                InfoSteelDuctilityClass : "Classe de Ductilité de l'acier de béton armé",
                //InputCard5
                TextBendingMoment : "Moment Fléchissant",
                TextMoment : "Med",
                InfoBendingMoment : "Moment Fléchissant",
                //OutputCard1
                TextSectionChart : "Section en béton",

                //OutputCard2
                TextReinforcement : "Armatures Acier",
                TextReinforcementTopReinforcement : "As1",
                InfoReinforcementTopReinforcement : "Section d'armatures d'acier supérieur",
                TextReinforcementBottomReinforcement : "As2",
                InfoReinforcementBottomReinforcement : "Section d'armatures d'acier inférieur"
            }
        }
    }
    return factory;
})