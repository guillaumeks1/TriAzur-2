﻿var RectSectionUnderBendingMomentControllers = angular.module('RectSectionUnderBendingMomentControllers', ['RectSectionUnderBendingMomentDirectives','RectSectionUnderBendingMomentServices'])

.controller('RectSectionUnderBendingMomentCtrl', function ($scope, RectSectionUnderBendingMomentTranslationService) {
    $scope.isNumeric = function (n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    };
    $scope.isStrictPositive = function (n) {
        if ($scope.isNumeric(n)) {
            if (n > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    };

    //-----------------------------------------------------------------------------------------------------------------------
    //Style
    //-----------------------------------------------------------------------------------------------------------------------
    $(document).ready(function () {
        if ($(this).width() < 750) {
            $('#menuinputverticalcard').hide();
            $('#menuoutputverticalcard').hide();
            $('#menuinputhorizontalcard').show();
            $('#menuoutputhorizontalcard').show();
        } else {
            $('#menuinputhorizontalcard').hide();
            $('#menuoutputhorizontalcard').hide();
            $('#menuinputverticalcard').show();
            $('#menuoutputverticalcard').show();
        }
    });

    $(window).resize(function () {
        if ($(this).width() < 750) {
            $('#menuinputverticalcard').hide();
            $('#menuoutputverticalcard').hide();
            $('#menuinputhorizontalcard').show();
            $('#menuoutputhorizontalcard').show();
        } else {
            $('#menuinputhorizontalcard').hide();
            $('#menuoutputhorizontalcard').hide();
            $('#menuinputverticalcard').show();
            $('#menuoutputverticalcard').show();

        }
    });
    //-----------------------------------------------------------------------------------------------------------------------
    //Commands
    //-----------------------------------------------------------------------------------------------------------------------
    $scope.tab = 1;
    $scope.tab1 = 1;

    $scope.setTab = function (newTab) {
        $scope.tab = newTab;
    };

    $scope.isSet = function (tabNum) {
        return $scope.tab === tabNum;
    };

    $scope.setTab1 = function (newTab1) {
        $scope.tab1 = newTab1;
    };

    $scope.isSet1 = function (tabNum1) {
        return $scope.tab1 === tabNum1;
    };

})