﻿var RectSectionUnderBendingMomentDirectives = angular.module('RectSectionUnderBendingMomentDirectives', [])

.directive('trRectsectionunderbendingmomentsectionchart', function () {
    return {
        restrict: 'A',
        scope: {
            title: '@',
            width: '=',
            height: '=',
            c: '=',
            cprime: '=',
            unitl: '='
        },
        templateUrl: '/js/app/structure/reinforcedconcrete/rectsectionbendingmoment/templates/RectSectionUnderBendingMomentEC2SectionChartTemplate.html',

    };
})

.directive('trSectionchart1', function () {
    return {
        restrict: 'A',
        scope: {
            sectionwidth: '=',
            sectionheight: '=',
            unitx: '=',
            unity: '=',
            c: '=',
            cprime: '='
        },
        link: function link(scope, element, attrs) {

            google.charts.load('current', { packages: ['corechart'] });
            google.charts.setOnLoadCallback(drawChart);

            $(document).ready(function () {
                $(window).resize(function () {
                    drawChart();
                });
                $('.redraw').click(function () {
                    drawChart();
                });
                $('#menu-toggle').mouseleave(function () {
                    drawChart();
                });

                //$('.redraw').hover(function () {
                //    drawChart();
                //});
            });

            function drawChart() {
                var unitx = scope.unitx;
                var unity = scope.unity;

                var data = google.visualization.arrayToDataTable([
                  ['X', 'Concrete Section', 'Reinforcement Inf', 'Reinforcement Sup'],
                  [0, 0, null, null],
                  [scope.sectionwidth(), 0, null, null],
                  [scope.sectionwidth(), scope.sectionheight(), null, null],
                  [0, scope.sectionheight(), null, null],
                  [0, 0, null, null],
                  [scope.c(), null, scope.c() - 0.01, null],
                  [scope.c(), null, scope.c() + 0.01, null],
                  [scope.sectionwidth() - scope.c(), null, scope.c() + 0.01, null],
                  [scope.sectionwidth() - scope.c(), null, scope.c() - 0.01, null],
                  [scope.c(), null, scope.c() - 0.01, null],
                  [scope.cprime(), null, null, scope.sectionheight() - scope.cprime() - 0.01],
                  [scope.cprime(), null, null, scope.sectionheight() - scope.cprime() + 0.01],
                  [scope.sectionwidth() - scope.cprime(), null, null, scope.sectionheight() - scope.cprime() + 0.01],
                  [scope.sectionwidth() - scope.cprime(), null, null, scope.sectionheight() - scope.cprime() - 0.01],
                  [scope.cprime(), null, null, scope.sectionheight() - scope.cprime() - 0.01]
                ]);



                var options = {
                    title: 'Concrete Section',
                    isStacked: true,
                    hAxis: {
                        title: 'X(' + unitx + ")",
                        titleTextStyle: { color: '#333' },
                        viewWindow: {
                            min: 0,
                            //max: scope.sectionwidth()
                        },
                        ticks: [
                            scope.sectionwidth() / 2,
                            { v: scope.sectionwidth(), f: scope.sectionwidth() },
                            { v: Math.max(scope.sectionwidth(), scope.sectionheight()) + 0.2, f: '' }
                        ]
                    },
                    vAxis: {
                        title: 'Y(' + unity + ")",
                        viewWindow: {
                            min: 0,
                            //max: scope.sectionheight()
                        },
                        ticks: [
                            scope.sectionheight() / 2,
                            { v: scope.sectionheight(), f: scope.sectionheight() },
                            { v: Math.max(scope.sectionwidth(), scope.sectionheight()), f: '' }
                        ]
                    },
                    series: {
                        0: { color: 'grey', visibleInLegend: false },
                        1: { color: 'black', visibleInLegend: false },
                        2: { color: 'black', visibleInLegend: false }
                    }
                };

                var chart = new google.visualization.AreaChart(element[0]);
                chart.draw(data, options);
            }

            scope.$watch(function () {
                return scope.sectionwidth();
            }, function () {
                drawChart();
            });

            scope.$watch(function () {
                return scope.sectionheight();
            }, function () {
                drawChart();
            });

            scope.$watch(function () {
                return scope.c();
            }, function () {
                drawChart();
            });

            scope.$watch(function () {
                return scope.cprime();
            }, function () {
                drawChart();
            });

            scope.$watch('unitx', function () {
                drawChart();
            });

        }
    };
})

.directive('trRectsectionunderbendingmomentoptions', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/reinforcedconcrete/rectsectionbendingmoment/templates/RectSectionUnderBendingMomentEC2OptionsTemplate.html',
        //scope:{},
        controller: ['$scope', function trRectsectionunderbendingmomentoptionsCtrl($scope) {

            $scope.Units = $scope.UnitsOptions[1];

            $scope.$watch('Units', function (newvalue, oldvalue) {
                switch ($scope.Units.value) {
                    case "m-kN-s":
                        $scope.UnitLength = "m";
                        $scope.UnitArea = "m²";
                        $scope.UnitTime = "s";
                        $scope.UnitWeight = "kg";
                        $scope.UnitForce = "kN";
                        $scope.UnitMoment = "kN.m";
                        $scope.UnitStress = "kPa";
                        $scope.UnitTemperature = "°C";
                        break;
                    case "m-MN-s":
                        $scope.UnitLength = "m";
                        $scope.UnitArea = "m²";
                        $scope.UnitTime = "s";
                        $scope.UnitWeight = "kg";
                        $scope.UnitForce = "MN";
                        $scope.UnitMoment = "MN.m";
                        $scope.UnitStress = "MPa";
                        $scope.UnitTemperature = "°C";
                        break;
                    case "cm-MN-s":
                        $scope.UnitLength = "cm";
                        $scope.UnitArea = "cm²";
                        $scope.UnitTime = "s";
                        $scope.UnitWeight = "kg";
                        $scope.UnitForce = "MN";
                        $scope.UnitMoment = "MN.cm";
                        $scope.UnitStress = "MPa";
                        $scope.UnitTemperature = "°C";
                        break;
                    case "inch-kip-s":
                        $scope.UnitLength = "inch";
                        $scope.UnitArea = "inch²";
                        $scope.UnitTime = "s";
                        $scope.UnitWeight = "kg";
                        $scope.UnitForce = "kip";
                        $scope.UnitMoment = "kip.inch";
                        $scope.UnitStress = "psi";
                        $scope.UnitTemperature = "°C";
                        break;
                    default:
                        $scope.UnitLength = "m";
                        $scope.UnitArea = "m²";
                        $scope.UnitTime = "s";
                        $scope.UnitWeight = "kg";
                        $scope.UnitForce = "MN";
                        $scope.UnitMoment = "MN.m";
                        $scope.UnitStress = "MPa";
                        $scope.UnitTemperature = "°C";
                        break;
                }
            });



        }]
    };
})

.directive('trRectsectionunderbendingmomentgeometry', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/reinforcedconcrete/rectsectionbendingmoment/templates/RectSectionUnderBendingMomentEC2GeometryTemplate.html',
        //scope:{},
        controller: ['$scope', function trRectsectionunderbendingmomentgeometryCtrl($scope) {
           //---------------------------------------------------------------------------------------------
           //Units Control
            //---------------------------------------------------------------------------------------------
            $scope.bwUnit = $scope.UnitLengthOptions[0];
            $scope.hUnit = $scope.UnitLengthOptions[0];
            $scope.cUnit = $scope.UnitLengthOptions[0];
            $scope.cprimeUnit = $scope.UnitLengthOptions[0];
            $scope.$watch('Units', function (newvalue, oldvalue) {
                for (var j = 0; j < $scope.UnitLengthOptions.length; j++) {
                    if ($scope.UnitLengthOptions[j].name === $scope.UnitLength) {
                        $scope.bwUnit = $scope.UnitLengthOptions[j];
                        $scope.hUnit = $scope.UnitLengthOptions[j];
                        $scope.cUnit = $scope.UnitLengthOptions[j];
                        $scope.cprimeUnit = $scope.UnitLengthOptions[j];
                    }
                }
            });
            //---------------------------------------------------------------------------------------------
            //Concrete Section Width
            //---------------------------------------------------------------------------------------------
            $scope.bwAff = 1;
         
            $scope.bw = function () {
                return ($scope.bwAff * $scope.conversionUnit[$scope.bwUnit.value]['m']);
            };
            $scope.$watch('bwUnit', function (newValue, oldValue) {
                $scope.bwAff = Math.round($scope.bwAff * $scope.conversionUnit[oldValue.value][newValue.value]*1000)/1000;
            });

            //---------------------------------------------------------------------------------------------
            //Concrete Section Height
            //---------------------------------------------------------------------------------------------
            $scope.hAff = 1;

            $scope.h = function () {
                return ($scope.hAff * $scope.conversionUnit[$scope.hUnit.value]['m']);
            };
            $scope.$watch('hUnit', function (newValue, oldValue) {
                $scope.hAff = Math.round($scope.hAff * $scope.conversionUnit[oldValue.value][newValue.value] * 1000) / 1000;
            });


            //---------------------------------------------------------------------------------------------
            //Reinforcement Bottom Cover
            //---------------------------------------------------------------------------------------------
            $scope.cAff = 0.05;

            $scope.c = function () {
                return ($scope.cAff * $scope.conversionUnit[$scope.cUnit.value]['m']);
            };
            $scope.$watch('cUnit', function (newValue, oldValue) {
                $scope.cAff = Math.round($scope.cAff * $scope.conversionUnit[oldValue.value][newValue.value] * 1000) / 1000;
            });

            //---------------------------------------------------------------------------------------------
            //Reinforcement Top Cover
            //---------------------------------------------------------------------------------------------
            $scope.cprimeAff = 0.050;

            $scope.cprime = function () {
                return ($scope.cprimeAff * $scope.conversionUnit[$scope.cprimeUnit.value]['m']);
            };
            $scope.$watch('cprimeUnit', function (newValue, oldValue) {
                $scope.cprimeAff = Math.round($scope.cprimeAff * $scope.conversionUnit[oldValue.value][newValue.value] * 1000) / 1000;
            });


            $scope.d = function () {
                return ($scope.h() - $scope.c());
            };

            $scope.dprime = function () {
                return ($scope.cprime());
            };

        }]
    };
})

.directive('trRectsectionunderbendingmomentconcrete', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/reinforcedconcrete/rectsectionbendingmoment/templates/RectSectionUnderBendingMomentEC2ConcreteTemplate.html',
        //scope:{},
        controller: ['$scope', function trRectsectionunderbendingmomentconcreteCtrl($scope) {
            //---------------------------------------------------------------------------------------------
            //Units Control
            //---------------------------------------------------------------------------------------------
            $scope.fckUnit = $scope.UnitStressOptions[0];
            $scope.$watch('Units', function (newvalue, oldvalue) {
                for (var j = 0; j < $scope.UnitStressOptions.length; j++) {
                    if ($scope.UnitStressOptions[j].name === $scope.UnitStress) {
                        $scope.fckUnit = $scope.UnitStressOptions[j];
                    }
                }
            });
            //---------------------------------------------------------------------------------------------
            //Concrete Strength
            //---------------------------------------------------------------------------------------------
            $scope.fckAff = 17;
            $scope.fck = function () {
                return ($scope.fckAff * Math.pow($scope.conversionUnit[$scope.fckUnit.value1]['MN'], $scope.fckUnit.pow1) / Math.pow($scope.conversionUnit[$scope.fckUnit.value2]['m'], $scope.fckUnit.pow2));
            };
            $scope.$watch('fckUnit', function (newValue, oldValue) {
                $scope.fckAff = Math.round($scope.fckAff * Math.pow($scope.conversionUnit[oldValue.value1][newValue.value1], newValue.pow1) / Math.pow($scope.conversionUnit[oldValue.value2][newValue.value2], newValue.pow2)*1000)/1000;
            });

            $scope.gammac = 1.5;

            $scope.alphacc = 1;

            $scope.eta = function () {
                if ($scope.fck() < 50) {
                    return (1);
                } else {
                    return (1 - ($scope.fck() - 50) / 200);
                };
            };

            $scope.fcd = function () {
                return ($scope.eta() * $scope.alphacc * $scope.fck() / $scope.gammac);
            };

            $scope.epsicu2 = function () {
                if ($scope.fck() < 50) {
                    return (3.5 / 1000);
                } else {
                    return ((2.6 + 35 * (Math.pow((90 - $scope.fck()) / 100, 4))) / 1000);
                };
            };

            $scope.lambda = function () {
                if ($scope.fck() < 50) {
                    return (0.8);
                } else {
                    return (0.8 - ($scope.fck() - 50) / 400);
                };
            };

            $scope.fctm = function () {
                return (0.3 * Math.pow($scope.fck(), 2 / 3));
            };

        }]
    };
})

.directive('trRectsectionunderbendingmomentsteel', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/reinforcedconcrete/rectsectionbendingmoment/templates/RectSectionUnderBendingMomentEC2SteelTemplate.html',
        //scope:{},
        controller: ['$scope', function trRectsectionunderbendingmomentsteelCtrl($scope) {
            //---------------------------------------------------------------------------------------------
            //Units Control
            //---------------------------------------------------------------------------------------------
            $scope.fykUnit = $scope.UnitStressOptions[0];
            $scope.EsUnit = $scope.UnitStressOptions[0];
            $scope.$watch('Units', function (newvalue, oldvalue) {
                for (var j = 0; j < $scope.UnitStressOptions.length; j++) {
                    if ($scope.UnitStressOptions[j].name === $scope.UnitStress) {
                        $scope.fykUnit = $scope.UnitStressOptions[j];
                        $scope.EsUnit = $scope.UnitStressOptions[j];
                    }
                }
            });
            //---------------------------------------------------------------------------------------------
            //Steel Stress
            //---------------------------------------------------------------------------------------------
            $scope.fykAff = 500;

            $scope.fyk = function () {
                return ($scope.fykAff * Math.pow($scope.conversionUnit[$scope.fykUnit.value1]['MN'], $scope.fykUnit.pow1) / Math.pow($scope.conversionUnit[$scope.fykUnit.value2]['m'], $scope.fykUnit.pow2));
            };
            $scope.$watch('fykUnit', function (newValue, oldValue) {
                $scope.fykAff = Math.round($scope.fykAff * Math.pow($scope.conversionUnit[oldValue.value1][newValue.value1], newValue.pow1) / Math.pow($scope.conversionUnit[oldValue.value2][newValue.value2], newValue.pow2)*1000)/1000;
            });

            $scope.gammas = 1.15;

            $scope.EsAff = 200000;
            $scope.Es = function () {
                return ($scope.EsAff * Math.pow($scope.conversionUnit[$scope.EsUnit.value1]['MN'], $scope.EsUnit.pow1) / Math.pow($scope.conversionUnit[$scope.EsUnit.value2]['m'], $scope.EsUnit.pow2));
            };
            $scope.$watch('EsUnit', function (newValue, oldValue) {
                $scope.EsAff = Math.round($scope.EsAff * Math.pow($scope.conversionUnit[oldValue.value1][newValue.value1], newValue.pow1) / Math.pow($scope.conversionUnit[oldValue.value2][newValue.value2], newValue.pow2)*1000)/1000;
            });

            $scope.fyd = function () {
                return ($scope.fyk() / $scope.gammas);
            };

            $scope.epsiyd = function () {
                return ($scope.fyd() / $scope.Es());
            };
            $scope.DuctilityClassOptions = [{
                name: 'A',
                value: 'A'
            }, {
                name: 'B',
                value: 'B'
            }, {
                name: 'C',
                value: 'C'
            }];

            $scope.DuctilityClass = $scope.DuctilityClassOptions[0];

            $scope.epsiuk = function () {
                var ductilityClass = $scope.DuctilityClass.value;
                if (ductilityClass === 'B') {
                    return (0.05);
                };
                if (ductilityClass === 'C') {
                    return (0.075);
                };
                if (ductilityClass === 'A') {
                    return (0.025);
                } else {
                    return (0.025);
                };
            };

            $scope.k = function () {
                var ductilityClass = $scope.DuctilityClass.value;
                if (ductilityClass === 'B') {
                    return (1.08);
                };
                if (ductilityClass === 'C') {
                    return (1.15);
                };
                if (ductilityClass === 'A') {
                    return (1.05);
                } else {
                    return (1.05);
                };
            };

            $scope.epsiud = function () {
                return (0.9 * $scope.epsiuk());
            };

            $scope.A = function () {
                return ($scope.fyd() * ($scope.k() * $scope.epsiyd() - $scope.epsiuk()) / ($scope.epsiyd() - $scope.epsiuk()));
            };

            $scope.B = function () {
                return (($scope.k() * $scope.fyd() - $scope.fyd()) / ($scope.epsiuk() - $scope.epsiyd()));
            };

        }]
    };
})

.directive('trRectsectionunderbendingmomentbendingmoment', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/reinforcedconcrete/rectsectionbendingmoment/templates/RectSectionUnderBendingMomentEC2BendingMomentTemplate.html',
        //scope:{},
        controller: ['$scope', function trRectsectionunderbendingmomentbendingmomentCtrl($scope) {
            //---------------------------------------------------------------------------------------------
            //Units Control
            //---------------------------------------------------------------------------------------------
            $scope.MedUnit = $scope.UnitMomentOptions[0];
            $scope.$watch('Units', function (newvalue, oldvalue) {
                for (var j = 0; j < $scope.UnitMomentOptions.length; j++) {
                    if ($scope.UnitMomentOptions[j].name === $scope.UnitMoment) {
                        $scope.MedUnit = $scope.UnitMomentOptions[j];
                    }
                }
            });
            //---------------------------------------------------------------------------------------------
            //Bending Moment
            //---------------------------------------------------------------------------------------------
            $scope.MedAff = 1;
            $scope.Med = function () {
                return ($scope.MedAff * Math.pow($scope.conversionUnit[$scope.MedUnit.value1]['MN'], $scope.MedUnit.pow1) * Math.pow($scope.conversionUnit[$scope.MedUnit.value2]['m'], $scope.MedUnit.pow2));
            };
            $scope.$watch('MedUnit', function (newValue, oldValue) {
                $scope.MedAff = Math.round($scope.MedAff * Math.pow($scope.conversionUnit[oldValue.value1][newValue.value1], newValue.pow1) * Math.pow($scope.conversionUnit[oldValue.value2][newValue.value2], newValue.pow2)*1000)/1000;
            });

        }]
    };
})

.directive('trRectsectionunderbendingmomentreinforcement', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/reinforcedconcrete/rectsectionbendingmoment/templates/RectSectionUnderBendingMomentEC2ReinforcementTemplate.html',
        //scope:{},
        controller: ['$scope', function trRectsectionunderbendingmomentreinforcementCtrl($scope) {
            //---------------------------------------------------------------------------------------------
            //Units Control
            //---------------------------------------------------------------------------------------------
            $scope.As2Unit = $scope.UnitAreaOptions[0];
            $scope.As1Unit = $scope.UnitAreaOptions[0];
            $scope.$watch('Units', function (newvalue, oldvalue) {
                for (var j = 0; j < $scope.UnitAreaOptions.length; j++) {
                    if ($scope.UnitAreaOptions[j].name === $scope.UnitArea) {
                        $scope.As2Unit = $scope.UnitAreaOptions[j];
                        $scope.As1Unit = $scope.UnitAreaOptions[j];
                    }
                }
            });
            //---------------------------------------------------------------------------------------------
            //Reinforcement Calculation as per EC2
            //---------------------------------------------------------------------------------------------


            $scope.mucu = function () {
                return ($scope.Med() / ($scope.bw() * Math.pow($scope.d(), 2) * $scope.fcd()));
            };

            $scope.alphals = function () {
                return ($scope.epsicu2() / ($scope.epsicu2() + $scope.epsiyd()));
            };

            $scope.muls = function () {
                return ($scope.lambda() * $scope.alphals() * (1 - $scope.lambda() * $scope.alphals() / 2));
            };

            $scope.alphau = function () {
                if ($scope.mucu() <= $scope.muls()) {
                    return (1 / $scope.lambda() * (1 - Math.pow((1 - 2 * $scope.mucu()), 0.5)));
                } else {
                    return (0);
                };
            };

            $scope.Mls = function () {
                return ($scope.muls() * $scope.bw() * Math.pow($scope.d(), 2) * $scope.fcd());
            };

            $scope.alphaAB = function () {
                return ($scope.epsicu2() / ($scope.epsicu2() + $scope.epsiud()));
            };

            $scope.muAB = function () {
                return ($scope.lambda() * $scope.alphaAB() * (1 - $scope.lambda() * $scope.alphaAB() / 2));
            };

            $scope.epsis1 = function () {
                if ($scope.mucu() <= $scope.muls()) {
                    if ($scope.mucu() <= $scope.muAB()) {
                        return ($scope.epsiud());
                    } else {
                        return ($scope.epsicu2() * (1 - $scope.alphau()) / $scope.alphau());
                    };
                } else {
                    return ($scope.epsiyd());
                };
            };

            $scope.epsis2 = function () {
                return ($scope.epsicu2() * ($scope.alphals() - $scope.dprime() / $scope.d()) / $scope.alphals());
            };

            $scope.sigmas1 = function () {
                if ($scope.mucu() <= $scope.muls()) {
                    return ($scope.A() + $scope.B() * $scope.epsis1());
                } else {
                    return ($scope.fyd());
                };
            };

            $scope.sigmas2 = function () {
                if ($scope.epsis2() <= $scope.epsiyd()) {
                    return ($scope.Es() * $scope.epsis2());
                } else {
                    return ($scope.A() + $scope.B() * $scope.epsis2());
                };
            };

            $scope.zc = function () {
                if ($scope.mucu() <= $scope.muls()) {
                    return ($scope.d() * (1 - $scope.lambda() * $scope.alphau() / 2));
                } else {
                    return ($scope.d() * (1 - $scope.lambda() * $scope.alphals() / 2));
                };
            };

            $scope.As2 = function () {
                if ($scope.mucu() <= $scope.muls()) {
                    return (0);
                } else {
                    return (($scope.Med() - $scope.Mls()) / (($scope.d() - $scope.dprime()) * $scope.sigmas2()));
                };
            };

            $scope.As1 = function () {
                if ($scope.mucu() <= $scope.muls()) {
                    return (($scope.Med() / ($scope.sigmas1() * $scope.zc())));
                } else {
                    return (($scope.Mls() / ($scope.sigmas1() * $scope.zc()) + $scope.As2() * $scope.sigmas2() / $scope.sigmas1()));
                };
            };
        }]
    };
})

.directive('trRectsectionunderbendingmomentdata', function () {
    return {
        restrict: 'A',
        templateUrl: '/js/app/structure/reinforcedconcrete/rectsectionbendingmoment/templates/RectSectionUnderBendingMomentEC2DataTemplate.html',
        //scope:{},
    };
})