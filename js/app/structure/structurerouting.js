var TriAzurApp = angular.module('TriAzurApp', ['ngRoute', 'StructureControllers'])

.config(function ($routeProvider, $locationProvider, $translateProvider) {
    $routeProvider
    .when('/', {
        templateUrl: '/structure/structure3d',
        controller: 'Structure3DCtrl'
    })
    .when('/steelsection', {
        templateUrl: '/structure/steelsection',
    })
    .when('/reinforcedconcrete', {
        templateUrl: '/structure/reinforcedconcrete',
    })
    .when('/prestressedconcrete', {
        templateUrl: '/structure/prestressedconcrete',
    })
    .when('/rectsectionunderbendingmomentEC2', {
        templateUrl: '/structure/rectsectionunderbendingmomentEC2',
        controller: 'RectSectionUnderBendingMomentEC2Ctrl'
    })
    .when('/rectsectionunderbendingmoment', {
        templateUrl: '/structure/rectsectionunderbendingmoment',
        controller: 'RectSectionUnderBendingMomentCtrl'
    })
    .when('/concretesectionanalysis', {
        templateUrl: '/structure/concretesectionanalysis',
        controller: 'ConcreteSectionAnalysisCtrl'
    })
    .when('/structure3d',{
        templateUrl: '/structure/structure3d',
        controller: 'Structure3DCtrl'
    })
    
    .otherwise({redirectTo:'/'});

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });

    $translateProvider.useSanitizeValueStrategy('escape'); // gestion des caractères d’échappement
    $translateProvider.useStaticFilesLoader({ prefix: '/languages/', suffix: '.json' }); // chargement des fichiers de langues
    $translateProvider.registerAvailableLanguageKeys(['en', 'fr'], { 'en_US': 'en', 'en_UK': 'en', 'fr_FR': 'fr', 'fr_BE': 'fr' }) // définition des langues disponibles
    .preferredLanguage('fr'); // sélection de la langue du système
    $translateProvider.use();
});