var TriAzurDirectives = angular.module('TriAzurDirectives', [])

.directive('trInput1', function () {
    return {
        restrict: 'A',
        scope: {
            text: '@',
            description: '@',
            input: '=',
            unit: '=',
            unitoptions: '='
        },
        templateUrl: '/js/app/templates/Input1Template.html',
        controller: ['$scope', function trInput1Ctrl($scope) {
            
        }]
    }
})

.directive('trTooltip', function () {
    return {
        restrict: 'A',
        scope: {
            description: '='
        },
        link: function (scope, element, attrs) {
            scope.$watch('description', function () {
                attrs.description;
            }, true);

            $(element)
                .attr('title', scope.$eval(attrs.description))
                .tooltip({ 
                    selector: '',
                    placement: 'top',
                    container:'body'
                });
        }
        //controller: ['$scope', function tooltipCtrl($scope) {
        //    $scope
        //}]
    }
})

.directive('trInput', function () {
    return {
        restrict: 'A',
        scope: {
            text: '@',
            description: '@',
            unit: '=',
            input: '='
        },
        templateUrl: '/js/app/templates/InputTemplate.html'
    }
})

.directive('trSelect', function () {
    return {
        restrict: 'A',
        scope: {
            text: '@',
            description: '@',
            input: '=',
            inputoptions: '=',
            inputfunction: '='
        },
        templateUrl: '/js/app/templates/SelectTemplate.html'
    }
})

.directive('trOutput', function () {
    return {
        restrict: 'A',
        scope: {
            text: '@',
            description: '@',
            unit: '@',
            output: '='
        },
        templateUrl: '/js/app/templates/OutputTemplate.html'
    }
})

.directive('trOutput1', function () {
    return {
        restrict: 'A',
        scope: {
            text: '@',
            description: '@',
            output: '=',
            unit: '=',
            unitoptions: '='
        },
        templateUrl: '/js/app/templates/Output1Template.html',
    }
})


