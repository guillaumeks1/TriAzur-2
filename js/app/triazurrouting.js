var TriAzurApp = angular.module('TriAzurApp', ['TriAzurControllers'])

.config(function ($translateProvider) {
    $translateProvider.useSanitizeValueStrategy('escape'); // gestion des caractères d’échappement
    $translateProvider.useStaticFilesLoader({ prefix: '/languages/', suffix: '.json' }); // chargement des fichiers de langues
    $translateProvider.registerAvailableLanguageKeys(['en', 'fr'], { 'en_US': 'en', 'en_UK': 'en', 'fr_FR': 'fr', 'fr_BE': 'fr' }) // définition des langues disponibles
    .preferredLanguage('fr'); // sélection de la langue du système
    $translateProvider.use();
});