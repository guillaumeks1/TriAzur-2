var TriAzurServices = angular.module('TriAzurServices', ['pascalprecht.translate'])

.factory('ConversionService', function () {
    var factory = {
        conversionUnit: {
            s: {
                s: 1,
                h: 0.000277778
            },
            h: {
                s: 3600,
                h: 1
            },
            m: {
                m: 1,
                cm: 100,
                mm: 1000,
                inch: 39.37007874,
                ft: 3.28084
            },
            cm: {
                m: 0.01,
                cm: 1,
                mm: 10,
                inch: 0.393701,
                ft: 0.0328084
            },
            mm: {
                m: 0.001,
                cm: 0.1,
                mm: 1,
                inch: 0.0393701,
                ft: 0.00328084
            },
            inch: {
                m: 0.0254,
                cm: 2.54,
                mm: 25.4,
                inch: 1,
                ft: 0.0833333
            },
            ft: {
                m: 0.3048,
                cm: 30.48,
                mm: 304.8,
                inch: 12,
                ft: 1
            },
            N: {
                N: 1,
                kN: 0.001,
                MN: 0.000001,
                lbf: 0.22482,
                kip: 0.00022482
            },
            kN: {
                N: 1000,
                kN: 1,
                MN: 0.001,
                lbf: 224.82,
                kip: 0.22482
            },
            MN: {
                N: 1000000,
                kN: 1000,
                MN: 1,
                lbf: 224820,
                kip: 224.82
            },
            lbf: {
                N: 4.4482216153,
                kN: 0.0044482216153,
                MN: 0.0000044482216153,
                lbf: 1,
                kip: 0.001
            },
            kip: {
                N: 44482.216153,
                kN: 4.4482216153,
                MN: 0.0044482216153,
                lbf: 1000,
                kip: 1
            },
            lb: {
                lb: 1,
                oz: 16,
                st: 0.071429,
                g: 453.59702440351987,
                kg: 0.4535970244035199,
                ton: 0.0004535970244035199
            },
            oz: {
                lb: 0.0625,
                oz: 1,
                st: 0.004464313,
                g: 28.34981403,
                kg: 0.028349814,
                ton: 0.000028349814
            },
            st: {
                lb: 14,
                oz: 224,
                st: 1,
                g: 6350.415952,
                kg: 6.350415952,
                ton: 0.006350415952
            },
            g: {
                lb: 0.0022046,
                oz: 0.035274,
                st: 0.00015747,
                g: 1,
                kg: 0.001,
                ton: 0.000001
            },
            kg: {
                lb: 2.2046,
                oz: 35.274,
                st: 0.15747,
                g: 1000,
                kg: 1,
                ton: 0.001
            },
            ton: {
                lb: 2204.6,
                oz: 35274,
                st: 157.47,
                g: 1000000,
                kg: 1000,
                ton: 1
            }
        }
    }
    return factory;
})

.factory('UnitsService', function () {
    var factory = {
        UnitOptions: [{
            name: 'm-kN-s',
            value: 'm-kN-s'
        },
        {
            name: 'm-MN-s',
            value: 'm-MN-s'
        },
        {
            name: 'cm-MN-s',
            value: 'cm-MN-s'
        },
        {
            name: 'inch-kip-s',
            value: 'inch-kip-s'
        }],

        UnitTimeOptions: [{
            name: 's',
            value: 's'
        }, {
            name: 'h',
            value: 'h'
        }],

        UnitLengthOptions: [{
            name: 'm',
            value: 'm'
        }, {
            name: 'cm',
            value: 'cm'
        }, {
            name: 'mm',
            value: 'mm'
        }, {
            name: 'inch',
            value: 'inch'
        }, {
            name: 'ft',
            value: 'ft'
        }],

        UnitAreaOptions: [{
            name: 'm²',
            value: 'm',
            pow: 2
        }, {
            name: 'cm²',
            value: 'cm',
            pow: 2
        }, {
            name: 'mm²',
            value: 'mm',
            pow: 2
        }, {
            name: 'inch²',
            value: 'inch',
            pow: 2
        }, {
            name: 'ft²',
            value: 'ft',
            pow: 2
        }],

        UnitVolumeOptions: [{
            name: 'm3',
            value: 'm',
            pow: 3
        }, {
            name: 'cm3',
            value: 'cm',
            pow: 3
        }, {
            name: 'mm3',
            value: 'mm',
            pow: 3
        }, {
            name: 'inch3',
            value: 'inch',
            pow: 3
        }, {
            name: 'ft3',
            value: 'ft',
            pow: 3
        }],

        UnitStressOptions: [{
            name: 'Pa',
            value1: 'N',
            pow1: 1,
            value2: 'm',
            pow2: 2,
        }, {
            name: 'kPa',
            value1: 'kN',
            pow1: 1,
            value2: 'm',
            pow2: 2,
        }, {
            name: 'MPa',
            value1: 'MN',
            pow1: 1,
            value2: 'm',
            pow2: 2,
        }, {
            name: 'psi',
            value1: 'lbf',
            pow1: 1,
            value2: 'inch',
            pow2: 2,
        }],

        UnitDensityOptions: [{
            name: 'g/m3',
            value1: 'g',
            pow1: 1,
            value2: 'm',
            pow2: 3,
        }, {
            name: 'kg/m3',
            value1: 'kg',
            pow1: 1,
            value2: 'm',
            pow2: 3,
        }, {
            name: 't/m3',
            value1: 'ton',
            pow1: 1,
            value2: 'm',
            pow2: 3,
        }],

        UnitMomentOptions: [{
            name: 'N.m',
            value1: 'N',
            pow1: 1,
            value2: 'm',
            pow2: 1,
        }, {
            name: 'kN.m',
            value1: 'kN',
            pow1: 1,
            value2: 'm',
            pow2: 1,
        }, {
            name: 'MN.m',
            value1: 'MN',
            pow1: 1,
            value2: 'm',
            pow2: 1,
        }, {
            name: 'MN.cm',
            value1: 'MN',
            pow1: 1,
            value2: 'cm',
            pow2: 1,
        },{
            name: 'kip.inch',
            value1: 'kip',
            pow1: 1,
            value2: 'inch',
            pow2: 1,
        }],

        getUnitOptions: function () {
            return factory.UnitOptions;
        }
    }
    return factory;
})

.factory('TranslationService', function () {
    var factory = {
        LanguageOptions:[{
            name: 'En',
            value: 'En'
        }, {
            name: 'Fr',
            value: 'Fr'
        }],

        TranslationLanguage: {
            En: {
                //-----------------------------------------------------------------------------------------
                //                                            NAVBAR
                //-----------------------------------------------------------------------------------------
                NavBar : {
                    Structure: "Structure",
                    Geotechnic: "Geotechnic",
                    Hydraulic: "Hydraulic",
                    Logout: "Log Out",
                    Login: "Login",
                    Register: "Register"
                },
                Structure : {
                    //-----------------------------------------------------------------------------------------
                    //                                            MENU STRUCTURE
                    //-----------------------------------------------------------------------------------------
                    StructureMenu: {
                        TextStructure: "Structure",
                        TextSteelStructure: "Steel Structure",
                        TextSteelPileAPI: "Steel Pile API",
                        TextSteelPileEC3: "Steel Pile EC3",
                        TextConcreteStructure: "Concrete Structure",
                        TextReinforcedConcrete: "Reinforced Concrete",
                        TextPrestressedConcrete: "Prestressed Concrete",
                        TextStructure3D: "Structure 3D"
                    }, 
                }
            },
            Fr: {
                //-----------------------------------------------------------------------------------------
                //                                            NAVBAR
                //-----------------------------------------------------------------------------------------
                NavBar: {
                    Structure : "Structure",
                    Geotechnic : "Géotechnique",
                    Hydraulic : "Hydraulique",
                    Logout : "Déconnection",
                    Login : "Connexion",
                    Register : "Inscription"
                },
                Structure: {
                    //-----------------------------------------------------------------------------------------
                    //                                            MENU STRUCTURE
                    //-----------------------------------------------------------------------------------------
                    StructureMenu: {
                        TextStructure: "Structure",
                        TextSteelStructure: "Structure Acier",
                        TextSteelPileAPI: "Pieux Acier API",
                        TextSteelPileEC3: "Pieux Acier EC3",
                        TextConcreteStructure: "Structure Béton",
                        TextReinforcedConcrete: "Béton Armé",
                        TextPrestressedConcrete: "Béton Précontraint",
                        TextStructure3D: "Structure 3D"
                    }
                }
            }
        }
    }
    return factory;
})