var cloudant = require('./db');
var nodesdb = require('./nodesdb');
var nodeload = require('./../models/structure/structure3d/nodeload');

var listnod = [];
nodesdb.list({ include_docs: true }, function(er, data) {
    if (er) {
        return;
    }else{
        data.rows.forEach(function(row){
            if(row.doc.Name){
                listnod.push(row.doc);
                return listnod;
            }
        });
    }
});

//Creating or Using nodeloadsdb
cloudant.db.list(function(err, allDbs){
    if(err){
        console.log("Error in Node Loads db");
        return;
    }
    var noldb = false;
    for(var i = 0; i < allDbs.length;i++){
        if(allDbs[i]=='nodeloadsdb'){
            noldb = true;
        }
    }

    if(noldb == true){
        noldb  = cloudant.db.use('nodeloadsdb');
    }else{
        cloudant.db.create('nodeloadsdb', function() {
            nodeloadsdb  = cloudant.db.use('nodeloadsdb');

            var NodeIdA = listnod[0]._id;
            var nodeloadA = new nodeload('NL', NodeIdA, 10, 0, 0, 0, 0, 0);
            nodeloadsdb.insert(nodeloadA, function(err, data){
                if (err) {
                    console.log("Error in Node Insertion");
                    return;
                }
                console.log("NL node load added");
            });  
         });
    }
});

var nodeloadsdb = cloudant.db.use('nodeloadsdb');

module.exports = nodeloadsdb;