var cloudant = require('./db');
var materialsdb = require('./materialsdb');
var section = require('./../models/structure/structure3d/section');

var listmat = [];
materialsdb.list({ include_docs: true }, function(er, data) {
    if (er) {
        return;
    }else{
        data.rows.forEach(function(row){
            if(row.doc.Name){
                listmat.push(row.doc);
                return listmat;
            }
        });
    }
});

//Creating or Using supportsdb
cloudant.db.list(function(err, allDbs) {
    if (err) {
        console.log("Error in Section db");
        return;
    }
    var secdb = false;
    for(var i = 0; i<allDbs.length;i++){
        if(allDbs[i]=='sectionsdb'){
            secdb = true;
        }
    }
    if(secdb == true){
        sectionsdb  = cloudant.db.use('sectionsdb');
    }else{
        cloudant.db.create('sectionsdb', function() {
            sectionsdb  = cloudant.db.use('sectionsdb');
            var MaterialIdA=listmat[0]._id;
            for(var i=0; i< listmat.length;i++){
                if(listmat[i].Name==='Steel'){
                    var MaterialIdA = listmat[i]._id;
                }
            }
            var Pile = new section('Pile', MaterialIdA, 2, 10, 10, 10, 0.5, 0.5, 
            [0, 1,  1, 0, 0],
            [0, 0, 1, 1, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0]);
            sectionsdb.insert(Pile, function(err, data){
                if (err) {
                    console.log("Error in Section Insertion");
                    return;
                }
                console.log("Pile section added");
            });  
        });
    }
});
var sectionsdb = cloudant .db.use('sectionsdb');

module.exports = sectionsdb;
