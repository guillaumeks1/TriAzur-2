var cloudant = require('./db');
var support = require('./../models/structure/structure3d/support');


//Creating or Using supportsdb
cloudant.db.list(function(err, allDbs) {
        if (err) {
            console.log("Error in Support db");
            return;
        }
        var supdb = false;
        for(var i = 0; i<allDbs.length;i++){
            if(allDbs[i]=='supportsdb'){
                supdb = true;
            }
        }
        if(supdb == true){
            supportsdb  = cloudant.db.use('supportsdb');
        }else{
            cloudant.db.create('supportsdb', function() {
                supportsdb  = cloudant.db.use('supportsdb');
                var fixed = new support('Fixed', 0, 0, 0, 0, 0, 0);

                supportsdb.insert(fixed, function(err, data){
                    if (err) {
                        console.log("Error in Support Insertion");
                        return;
                    }
                    console.log("Fixed support added");
                });  

                var free = new support('Free', 1, 1, 1, 1, 1, 1);  
                supportsdb.insert(free, function(err, data){
                    if (err) {
                        console.log("Error in Support Insertion");
                        return;
                    }
                    console.log("Free support added");
                });  
            });
        }
    });
var supportsdb = cloudant .db.use('supportsdb');

module.exports = supportsdb;
