var cloudant = require('./db');
var supportsdb = require('./supportsdb');
var node = require('./../models/structure/structure3d/node');

var listsup = [];
supportsdb.list({ include_docs: true }, function(er, data) {
    if (er) {
        return;
    }else{
        data.rows.forEach(function(row){
            if(row.doc.Name){
                listsup.push(row.doc);
                return listsup;
            }
        });
    }

});


//Creating or Using nodesdb
cloudant.db.list(function(err, allDbs) {
    if (err) {
        console.log("Error in Node db");
        return;
    }
    var noddb = false;
    for(var i = 0; i < allDbs.length;i++){
        if(allDbs[i]=='nodesdb'){
            noddb = true;
        }
    }
    if(noddb == true){
        nodesdb  = cloudant.db.use('nodesdb');
    }else{
        cloudant.db.create('nodesdb', function() {
            nodesdb  = cloudant.db.use('nodesdb');

            var SupportIdA = listsup[0]._id;
            var pointA = new node('A', SupportIdA, 0, 0, 0);
            nodesdb.insert(pointA, function(err, data){
                if (err) {
                    console.log("Error in Node Insertion");
                    return;
                }
                console.log("PointA node added");
            });  

            var SupportIdB = listsup[1]._id;
            var pointB = new node('B', SupportIdB, 10, 10, 10);  
            nodesdb.insert(pointB, function(err, data){
                if (err) {
                    console.log("Error in Node Insertion");
                    return;
                }
                console.log("PointB node added");
            });  
        });
    }
});
var nodesdb = cloudant.db.use('nodesdb');

module.exports = nodesdb;
