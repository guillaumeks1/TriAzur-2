var cloudant = require('./db');
var nodesdb = require('./nodesdb');
var sectionsdb = require('./sectionsdb');
var member = require('./../models/structure/structure3d/member');

var listsec = [];
sectionsdb.list({ include_docs: true }, function(er, data) {
    if (er) {
        return;
    }else{
        data.rows.forEach(function(row){
            if(row.doc.Name){
                listsec.push(row.doc);
                return listsec;
            }
        });
    }
});

var listnod = [];
nodesdb.list({ include_docs: true }, function(er, data) {
    if (er) {
        return;
    }else{
        data.rows.forEach(function(row){
            if(row.doc.Name){
                listnod.push(row.doc);
                return listnod;
            }
        });
    }
});

//Creating or Using membersdb
cloudant.db.list(function(err, allDbs) {
    if (err) {
        console.log("Error in Member db");
        return;
    }
    var memdb = false;
    for(var i = 0; i < allDbs.length;i++){
        if(allDbs[i]=='membersdb'){
            memdb = true;
        }
    }
    if(memdb == true){
        membersdb  = cloudant.db.use('membersdb');
    }else{
        cloudant.db.create('membersdb', function() {
            membersdb  = cloudant.db.use('membersdb');

            var NodeiIdAA = listnod[0]._id;
            var NodejIdAA = listnod[0]._id;
            var SectionIdAA = listsec[0]._id;

            var MemberAA = new member('AA', NodeiIdAA, NodejIdAA, SectionIdAA);

            membersdb.insert(MemberAA, function(err, data){
                if (err) {
                    console.log("Error in Member Insertion");
                    return;
                }
                console.log("MemberAA member added");
            });  
        });
    }
});

var membersdb = cloudant .db.use('membersdb');

module.exports = membersdb;