var cloudant = require('./db');
var material = require('./../models/structure/structure3d/material');


//Creating or Using supportsdb
cloudant.db.list(function(err, allDbs) {
        if (err) {
            console.log("Error in Material db: " + err.message);
            return;
        }
        var matdb = false;
        for(var i = 0; i<allDbs.length;i++){
            if(allDbs[i]=='materialsdb'){
                matdb = true;
            }
        }
        if(matdb == true){
            materialsdb  = cloudant.db.use('materialsdb');
        }else{
            cloudant.db.create('materialsdb', function() {
                materialsdb  = cloudant.db.use('materialsdb');

                var steel = new material('Steel', 10, 10, 10, 10, 10);
                materialsdb.insert(steel, function(err, data){
                    if (err) {
                        console.log("Error in Material Insertion");
                        return;
                    }
                    console.log("Steel added");
                });  

                var concrete = new material('Concrete', 10, 10, 10, 10, 10);  
                materialsdb.insert(concrete, function(err, data){
                    if (err) {
                        console.log("Error in Material Insertion");
                        return;
                    }
                    console.log("Concrete added");
                });  
            });
        }
    });
var materialsdb = cloudant .db.use('materialsdb');

module.exports = materialsdb;

