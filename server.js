var express = require('express');
var bodyParser = require('body-parser');
var Cloudant = require('cloudant');
var urlencodedParser = bodyParser.urlencoded({ extended: false });

var index = require('./routes/index');

//-----------------------------------------------------------------------------------------------------------------------
// Routes Structure
//-----------------------------------------------------------------------------------------------------------------------
var structureroutes = require('./routes/structure/structureroutes');
var materialsdatacontrollers = require('./routes/structure/structure3d/materialsdatacontrollers');
var sectionsdatacontrollers = require('./routes/structure/structure3d/sectionsdatacontrollers');
var supportsdatacontrollers = require('./routes/structure/structure3d/supportsdatacontrollers');
var nodesdatacontrollers = require('./routes/structure/structure3d/nodesdatacontrollers');
var membersdatacontrollers = require('./routes/structure/structure3d/membersdatacontrollers');
var nodeloadsdatacontrollers = require('./routes/structure/structure3d/nodeloadsdatacontrollers');
var structure3danalysisdatacontrollers = require('./routes/structure/structure3d/structure3danalysisdatacontrollers');
//-----------------------------------------------------------------------------------------------------------------------
// Routes Watson
//-----------------------------------------------------------------------------------------------------------------------
var watsonroutes = require('./routes/watson/watsonroutes');

var app = express();

app.set('view engine','ejs');

// Définition de l'hôte et du port
var host = process.env.VCAP_APP_HOST || process.env.HOST || 'localhost';
var port = process.env.VCAP_APP_PORT || process.env.PORT || 8080;


// Middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/js',express.static(__dirname + "/js"));
app.use('/css',express.static(__dirname + "/css"));
app.use('/img',express.static(__dirname + "/img"));
app.use('/fonts',express.static(__dirname + "/fonts"));
app.use('/languages',express.static(__dirname + "/languages"));
app.use('/views',express.static(__dirname + "/views"));

app.get('/backindex', function(req, res){
    res.render('index.ejs');
});

app.use('/', index);

//-----------------------------------------------------------------------------------------------------------------------
// Middlewares Structure
//-----------------------------------------------------------------------------------------------------------------------
app.use('/structure', structureroutes);
//structure 3d data
app.use('/materialsdata', materialsdatacontrollers);
app.use('/sectionsdata', sectionsdatacontrollers);
app.use('/supportsdata', supportsdatacontrollers);
app.use('/nodesdata', nodesdatacontrollers);
app.use('/membersdata', membersdatacontrollers);
app.use('/nodeloadsdata', nodeloadsdatacontrollers);
app.use('/structure3danalysisdata', structure3danalysisdatacontrollers);
//-----------------------------------------------------------------------------------------------------------------------
// Middlewares Watson
//-----------------------------------------------------------------------------------------------------------------------
app.use('/watson', watsonroutes);

//-----------------------------------------------------------------------------------------------------------------------
// Middlewares Geotechnic
//-----------------------------------------------------------------------------------------------------------------------
app.get('/geotechnic', function(req, res){
    res.render('geotechnic/triazurgeotechnic.ejs',{variable:'Hello TriAzurGeotechnic'});
});
//-----------------------------------------------------------------------------------------------------------------------
// Middlewares Hydraulic
//-----------------------------------------------------------------------------------------------------------------------
app.get('/hydraulic', function(req, res){
    res.render('hydraulic/triazurhydraulic.ejs',{variable:'Hello TriAzurHydraulic'});
});



app.all('/*', function(req, res){
    res.render('structure/triazurstructure.ejs');
});

app.listen(port, host);