var express = require('express');
var router = express.Router();

/* GET Landing Page */
router.get('/', function(req, res){
    res.render('index.ejs');    
});

router.get('/backindex', function(req, res){
    res.render('index.ejs');    
});

module.exports = router;
