var express = require('express');
var router = express.Router();

router.get('/', function(req, res){
    res.render('structure/triazurstructure.ejs');
});

router.get('/introduction', function(req, res){
    res.render('structure/structureintroduction.ejs');
});

router.get('/steelsection',function(req,res){
     res.render('structure/steelsection.ejs');
});

router.get('/reinforcedconcrete',function(req,res){
     res.render('structure/reinforcedconcrete.ejs');
});

router.get('/prestressedconcrete',function(req,res){
     res.render('structure/prestressedconcrete.ejs');
});

router.get('/rectsectionunderbendingmomentEC2',function(req,res){
     res.render('structure/rectsectionunderbendingmomentEC2.ejs');
});

router.get('/rectsectionunderbendingmoment',function(req,res){
     res.render('structure/rectsectionunderbendingmoment.ejs');
});

router.get('/concretesectionanalysis',function(req,res){
     res.render('structure/concretesectionanalysis.ejs');
});

router.get('/structure3d',function(req,res){
     res.render('structure/structure3d.ejs');
});

module.exports = router;