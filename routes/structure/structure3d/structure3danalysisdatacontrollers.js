var express = require('express');
var router = express.Router();
var cloudant = require('./../../../config/db');
var material = require('./../../../models/structure/structure3d/material');
var section = require('./../../../models/structure/structure3d/section');
var support = require('./../../../models/structure/structure3d/support');
var node = require('./../../../models/structure/structure3d/node');
var member = require('./../../../models/structure/structure3d/member');
var nodeload = require('./../../../models/structure/structure3d/nodeload');

var bodyParser = require('body-parser');

var structure3danalysis = require('./../../../models/calculation/structure3danalysis');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

var materialsdb = require('./../../../config/materialsdb');
var sectionsdb = require('./../../../config/sectionsdb');
var supportsdb = require('./../../../config/supportsdb');
var nodesdb = require('./../../../config/nodesdb');
var membersdb = require('./../../../config/membersdb');
var nodeloadsdb = require('./../../../config/nodeloadsdb');

router.get('/datainputs', function(req, res, next){
    sectionsdb.list({ include_docs: true }, function(err, data) {
		if (err) {
			res.json({err:err});
			return;
		}else{
            var listsec = [];

            data.rows.forEach(function(row){
                if(row.doc.Name){
                    listsec.push(row.doc);                    
                }
            });
            res.json(listsec);
        }
    });
});

router.get('/datastructure3d', function(req, res, next){
    if(!sectionsdb){
        res.json('No access to database');
        return;
    }
    materialsdb.list({ include_docs: true }, function(err, data) {
        if (err) {
            res.json({err:err});
            return;
        }else{
            var listmat = [];
            data.rows.forEach(function(row){
                if(row.doc.Name){
                    listmat.push(row.doc);                    
                }
            });
            sectionsdb.list({ include_docs: true }, function(err, data) {
                if (err) {
                    res.json({err:err});
                    return;
                }else{
                    var listsec = [];
                    data.rows.forEach(function(row){
                        if(row.doc.Name){
                            listsec.push(row.doc);                    
                        }
                    });
                    supportsdb.list({ include_docs: true }, function(err, data) {
                        if (err) {
                            res.json({err:err});
                            return;
                        }else{
                            var listsup = [];
                            data.rows.forEach(function(row){
                                if(row.doc.Name){
                                    listsup.push(row.doc);                    
                                }
                            });
                            nodesdb.list({ include_docs: true }, function(err, data) {
                                if (err) {
                                    res.json({err:err});
                                    return;
                                }else{
                                    var listnod = [];
                                    data.rows.forEach(function(row){
                                        if(row.doc.Name){
                                            listnod.push(row.doc);                    
                                        }
                                    });
                                    membersdb.list({ include_docs: true }, function(err, data) {
                                        if (err) {
                                            res.json({err:err});
                                            return;
                                        }else{
                                            var listmem = [];
                                            data.rows.forEach(function(row){
                                                if(row.doc.Name){
                                                    listmem.push(row.doc);                    
                                                }
                                            });
                                            nodeloadsdb.list({ include_docs: true }, function(err, data) {
                                                if (err) {
                                                    res.json({err:err});
                                                    return;
                                                }else{
                                                    var listnol = [];
                                                    data.rows.forEach(function(row){
                                                        if(row.doc.Name){
                                                            listnol.push(row.doc);                    
                                                        }
                                                    });
                                                    var analysisdata = structure3danalysis(listmat, listsec, listsup, listnod, listmem, listnol);
                                                    console.log(analysisdata);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });









    // console.log(req.query);

    // res.json(req.query); 
});


module.exports = router;