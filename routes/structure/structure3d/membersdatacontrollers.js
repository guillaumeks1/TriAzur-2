var express = require('express');
var router = express.Router();
var cloudant = require('./../../../config/db');
var member = require('./../../../models/structure/structure3d/member');
var bodyParser = require('body-parser');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

var sectionsdb = require('./../../../config/sectionsdb');
var nodesdb = require('./../../../config/nodesdb');
var membersdb = require('./../../../config/membersdb');

router.get('/listmembers', function(req, res, next){
    if(!membersdb){        
        res.json('No access to database');
        return;
    }
    membersdb.list({ include_docs: true }, function(err, data) {
		if (err) {
			res.json({err:err});
			return;
		}else{
            var listmem = [];
            data.rows.forEach(function(row){
                if(row.doc.Name){
                    listmem.push(row.doc);
                }
            });
            res.json(listmem);
        }
    });
});

router.post('/editlistmembers', function(req, res, next){
    if(!membersdb){
        res.json('No access to database');
        return;
    }

    var EditListMembersmessage = '';

    if(req.body.length === 0){
        var EditListMembersmessage = '';
        return;
    }else{
        var editlistmemvalid = [];

        for(var i=0; i<req.body.length;i++){
            var editmem = new member(req.body[i].Name, req.body[i].NodeiId, req.body[i].NodejId, req.body[i].SectionId);
            
            if(i>0){
                for(var j=0; j<i; j++){
                    if(req.body[j].Name==req.body[i].Name){
                        EditListMembersmessage = "Member " + req.body[i].Name + " already exists";
                        res.json(EditListMembersmessage);
                        return;
                    }
                    if(req.body[j].NodeiId==req.body[i].NodeiId && req.body[j].NodejId==req.body[i].NodejId){
                        EditListMembersmessage = "Member " + req.body[j].Name + " has same nodes than " +  req.body[i].Name;
                        res.json(EditListMembersmessage);
                        return;
                    }
                    if(req.body[j].NodeiId==req.body[i].NodejId && req.body[j].NodejId==req.body[i].NodeiId){
                        EditListMembersmessage = "Member " + req.body[j].Name + " has same nodes than " +  req.body[i].Name;
                        res.json(EditListMembersmessage);
                        return;
                    }
                    if(req.body[j].NodeiId==req.body[j].NodejId){
                        EditListMembersmessage = "Member " + req.body[j].Name + " has a length equal to 0";
                        res.json(EditListMembersmessage);
                        return;
                    }
                }
            }

            if(editmem.isvalid()){
                var id = req.body[i]._id || req.body[i].id || "";
                var rev = req.body[i]._rev || req.body[i].rev || "";

                var membedited = {
                    _id : id,
                    _rev : rev,
                    Name: editmem.Name,
                    NodeiId: editmem.NodeiId, 
                    NodejId: editmem.NodejId,
                    SectionId: editmem.SectionId
                }
                editlistmemvalid.push(membedited);
            }else{
                EditListMembersmessage = "Data not valid";
                res.json(EditListMembersmessage);
                return;
            }
        }

        var listmem = {
            docs: editlistmemvalid
        };
        membersdb.bulk(listmem, function(er, data){
            if(er){
                throw er;
            }
            for(var i=0; i<listmem.docs.length;i++){
                console.log(listmem.docs[i].Name);
            }
            EditListMembersmessage = "Success";
            res.json(EditListMembersmessage);
        });
    }
  
});

router.post('/editmembers', function(req, res, next){
    if(!membersdb){
        res.json('No access to database');
        return;
    }

    let mem = req.body;
    let EditMembersmessage = '';

    let editmem = new member(mem.Name, mem.NodeiId, mem.NodejId, mem.SectionId);

    if(editmem.isvalid()){
        var memnameindex = {name:'memnameindex', type:'json', index:{fields:['Name']}};
         membersdb.index(memnameindex,function(er, result){
            if (er) {
                EditMembersmessage = "Error in Index Query Creation";
                return;
            }
        });

        var id = req.query._id || req.query.id || req.body._id || req.body.id || "";
        var rev = req.query._rev || req.query.rev || req.body._rev || req.body.rev || "";

        if (id != "") {
            membersdb.get(id, function(err, data){
                if(err){
                    EditMembersmessage = "Member not founded in the database";
					return;
                }

                var old_doc = data;

                var memberedited = {
                    _id : id,
                    _rev : rev,
                    Name: editmem.Name,
                    NodeiId: editmem.NodeiId,
                    NodejId: editmem.NodejId,
                    SectionId: editmem.SectionId
                }

                var query = { selector:{Name:editmem.Name}};
                membersdb.find(query,function(err, result){
                    if(result.docs.length==0){
                        membersdb.insert(memberedited, function(err, data){
                            if (err) {
                                EditMembersmessage = "Error in Member Insertion";
                                res.json(EditMembersmessage);
                                return;
                            }
                            EditMembersmessage = 'Success';
                            res.json(EditMembersmessage);
                        });
                    }else{
                        if(result.docs.length==1 && result.docs[0]._id == old_doc._id){
                            membersdb.insert(memberedited, function(err, data){
                                if (err) {
                                    EditMembersmessage = "Error in Member Insertion";
                                    res.json(EditMembersmessage);
                                    return;
                                }
                                EditMembersmessage = 'Success';
                                res.json(EditMembersmessage);
                            });
                        }else{
                            EditMembersmessage = 'Another member with the same name already exists';
                            res.json(EditMembersmessage);
                        }
                    }
                });
            })
        }else{
            EditMembersmessage = 'This member does not exist in the database';
            res.json(EditMembersmessage);
        }
    }else{
        EditMembersmessage = req.body;
        res.json(EditMembersmessage);
    }
});


router.post('/addmembers', function(req, res, next){
    if(!membersdb){
        res.json('No access to database');
        return;
    }

    var AddMembersmessage = '';

    var query = { selector:{Name:'Pile'}};

    sectionsdb.find(query,function(err, result){
        if(err){
            console.log('Error section query');
            return;
        }
        nodesdb.list({ include_docs: true }, function(err, data) {
            if (err) {
                res.json({err:err});
                return;
            }else{
                var listnod = [];
                data.rows.forEach(function(row){
                    if(row.doc.Name){
                        listnod.push(row.doc);                    
                    }
                });
                var newmem = new member("MID#" + Math.floor(Math.random()*10000), listnod[0]._id, listnod[0]._id, result.docs[0]._id);
                if(newmem.isvalid()){
                    var memnameindex = {name:'nodnameindex', type:'json', index:{fields:['Name']}};
                    membersdb.index(memnameindex,function(er, result){
                        if (er) {
                            AddMembersmessage = "Error in Index Query Creation";
                            return;
                        }
                    });

                    var query = { selector:{Name:newmem.Name}};
                    membersdb.find(query,function(err, result){
                        if(result.docs.length==0){
                            membersdb.insert(newmem, function(err, data){
                                if (err) {
                                    AddMembersmessage = "Error in Member Insertion";
                                    res.json(AddMembersmessage);
                                    return;
                                }
                                AddMembersmessage = "Success";
                                res.json(AddMembersmessage);
                            });
                        }else{
                            AddMembersmessage = 'Member already exists';
                            res.json(AddMembersmessage);
                        }
                    });
                }else{
                    AddMembersmessage = newmem;
                    res.json(AddMembersmessage);
                }
            }
        });
    });
});


router.post('/deletemembers', function(req, res, next){
    let deletemem = req.body;
    let DeleteMembermessage = "Ok";

    var memnameindex = {name:'memnameindex', type:'json', index:{fields:['Name']}};
    membersdb.index(memnameindex,function(er, result){
        if (er) {
            DeleteMembermessage = "Error in Index Query Creation";
            return;
        }
    });

    var id = deletemem.id || deletemem._id;
    var rev = deletemem.rev || deletemem._rev;
    
 
    if(id !='')
    {
        var query = { selector:{Name:deletemem.Name}};
        membersdb.find(query,function(err, result){
            if(result.docs.length==0){
                DeleteMembermessage = 'Database Error: This member does not exist';
                res.json(DeleteMembermessage);
            }else{
                membersdb.destroy(id, rev, function(err, data){
                    if (err) {
                        DeleteMembermessage = "Member not deleted";
                        res.json(DeleteMembermessage);
                        return;
                    }
                    DeleteMembermessage = "Success";
                    res.json(DeleteMembermessage);
                });
            }
        });
    }else{
        DeleteMembermessage = 'This member does not exist';
        res.json(DeleteMembermessage);
    }
});


module.exports = router;
