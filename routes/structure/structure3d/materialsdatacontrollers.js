var express = require('express');
var router = express.Router();
var cloudant = require('./../../../config/db');
var material = require('./../../../models/structure/structure3d/material');
var bodyParser = require('body-parser');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

var materialsdb = require('./../../../config/materialsdb');

router.get('/listmaterials', function(req, res, next){
    if(!materialsdb){        
        res.json('No access to database');
        return;
    }
    materialsdb.list({ include_docs: true }, function(err, data) {
		if (err) {
			res.json({err:err});
			return;
		}else{
            var listmat = [];

            data.rows.forEach(function(row){
                if(row.doc.Name){
                    listmat.push(row.doc);
                }
            });
            res.json(listmat);
        }
    });
});

router.post('/editmaterials', function(req, res, next){
    if(!materialsdb){
        res.json('No access to database');
        return;
    }

    let mat = req.body;
    let EditMaterialsmessage = '';

    let editmat = new material(mat.Name, mat.Density, mat.Eyoung, mat.Poisson, mat.Thermal,mat.Gmodulus);

    if(editmat.isvalid()){
        if(editmat.Name === 'Steel'){
            EditMaterialsmessage = 'Steel Material';
            res.json(EditMaterialsmessage);
        }else{
            var matnameindex = {name:'matnameindex', type:'json', index:{fields:['Name']}};
            materialsdb.index(matnameindex,function(er, result){
                if (er) {
                    EditMaterialsmessage = "Error in Index Query Creation";
                    return;
                }
            });

            var id = req.query._id || req.query.id || req.body._id || req.body.id || "";

            if (id != "") {
                materialsdb.get(id, function(err, data){
                    if(err){
                        EditMaterialsmessage = "Material not founded in the database";
                        return;
                    }

                    var old_doc = data;

                    var query = { selector:{Name:editmat.Name}};
                    materialsdb.find(query,function(err, result){
                        if(result.docs.length==0){
                            materialsdb.insert(mat, function(err, data){
                                if (err) {
                                    EditMaterialsmessage = "Error in Material Insertion";
                                    res.json(EditMaterialsmessage);
                                    return;
                                }
                                EditMaterialsmessage = "Success";
                                res.json(EditMaterialsmessage);
                            });
                        }else{
                            if(result.docs.length==1 && result.docs[0]._id == old_doc._id){
                                materialsdb.insert(mat, function(err, data){
                                    if (err) {
                                        EditMaterialsmessage = "Error in Material Insertion";
                                        res.json(EditMaterialsmessage);
                                        return;
                                    }
                                    EditMaterialsmessage = "Success";
                                    res.json(EditMaterialsmessage);
                                });
                            }else{
                                EditMaterialsmessage = 'Another material with the same name already exists';
                                res.json(EditMaterialsmessage);
                            }
                        }
                    });
                })
            }else{
                EditMaterialsmessage = 'This material does not exist in the database';
                res.json(EditMaterialsmessage);
            }
        }
    }else{
        EditMaterialsmessage = "Input no valid";
        res.json(EditMaterialsmessage);
    }
});

router.post('/addmaterials', function(req, res, next){
    if(!materialsdb){
        res.json('No access to database');
        return;
    }

    let mat = req.body;
    let AddMaterialsmessage = '';
    
    let newmat = new material(mat.Name, mat.Density, mat.Eyoung, mat.Poisson, mat.Thermal,mat.Gmodulus);  

    if(newmat.isvalid()){
        //Look for a Material with the same name
        var matnameindex = {name:'matnameindex', type:'json', index:{fields:['Name']}};
        materialsdb.index(matnameindex,function(er, result){
            if (er) {
                AddMaterialsmessage = "Error in Index Query Creation";
                return;
            }
        });

        var query = { selector:{Name:newmat.Name}};
        materialsdb.find(query,function(err, result){
            if(result.docs.length==0){
                materialsdb.insert(newmat, function(err, data){
                    if (err) {
                        AddMaterialsmessage = "Error in Material Insertion";
                        res.json(AddMaterialsmessage);
                        return;
                    }
                    AddMaterialsmessage = "Success";
                    res.json(AddMaterialsmessage);
                });
            }else{
                AddMaterialsmessage = 'Material already exists';
                res.json(AddMaterialsmessage);
            }
        });
    }else{
        AddMaterialsmessage = "Input no valid";
        res.json(AddMaterialsmessage);
    }
});


router.post('/deletematerials', function(req, res, next){
    var deletemat = req.body;
    var DeleteMaterialmessage = "Ok";

    var id = deletemat.id || deletemat._id;
    var rev = deletemat.rev || deletemat._rev;

    var matidindex = {name:'matidindex', type:'json', index:{fields:['_id']}};
    materialsdb.index(matidindex,function(er, result){
        if (er) {
            DeleteMaterialmessage = "Error in Index Query Creation";
            return;
        }
    });

    if(deletemat.Name === 'Steel'){
        DeleteMaterialmessage = 'Steel material can not be deleted';
        res.json(DeleteMaterialmessage);
    }else{
        if(id !='')
        {
            var query = { selector:{_id:deletemat._id}};
            materialsdb.find(query,function(err, result){
                if(result.docs.length==0){
                    DeleteMaterialmessage = 'Database Error: This material does not exist';
                    res.json(DeleteMaterialmessage);
                }else{
                    materialsdb.destroy(id, rev, function(err, data){
                        if (err) {
                            DeleteMaterialmessage = "Material not deleted";
                            res.json(DeleteMaterialmessage);
                            return;
                        }
                        DeleteMaterialmessage = "Success";
                        res.json(DeleteMaterialmessage);
                    });
                }
            });
        }else{
            DeleteMaterialmessage = 'This material does not exist';
            res.json(DeleteMaterialmessage);
        }
    }
});


module.exports = router;
