var express = require('express');
var router = express.Router();
var cloudant = require('./../../../config/db');
var nodeload = require('./../../../models/structure/structure3d/nodeload');
var bodyParser = require('body-parser');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

var nodesdb = require('./../../../config/nodesdb');
var nodeloadsdb = require('./../../../config/nodeloadsdb');

router.get('/listnodeloads', function(req, res, next){
    if(!nodeloadsdb){        
        res.json('No access to database');
        return;
    }
    nodeloadsdb.list({ include_docs: true }, function(err, data) {
		if (err) {
			res.json({err:err});
			return;
		}else{
            var listnol = [];

            data.rows.forEach(function(row){
                if(row.doc.Name){
                    listnol.push(row.doc);
                }
            });
            res.json(listnol);
        }
    });
});

router.post('/editlistnodeloads', function(req, res, next){
    if(!nodeloadsdb){
        res.json('No access to database');
        return;
    }

   var EditListNodeLoadsmessage = '';

   if(req.body.length===0){
        EditListNodeLoadsmessage = "No node load exist";
        return;
   }else{
        var editlistnolvalid = [];

        for(var i=0; i<req.body.length;i++){
            var editnol = new nodeload(req.body[i].Name, req.body[i].NodeId, req.body[i].ForceX, req.body[i].ForceY, req.body[i].ForceZ, req.body[i].MomentX, req.body[i].MomentY, req.body[i].MomentZ);
            
            // if(i>0){
            //     for(var j=0; j<i; j++){
            //         if(req.body[j].Name==req.body[i].Name){
            //             EditListNodesmessage = "Node Load" + req.body[i].Name + " already exists";
            //             res.json(EditListNodeLoadsmessage);
            //             return;
            //         }
            //     }
            // }

            if(editnol.isvalid()){
                var id = req.body[i]._id || req.body[i].id || "";
                var rev = req.body[i]._rev || req.body[i].rev || "";

                var nodeloadedited = {
                    _id : id,
                    _rev : rev,
                    Name: editnol.Name,
                    NodeId: editnol.NodeId, 
                    ForceX: editnol.ForceX,
                    ForceY: editnol.ForceY,
                    ForceZ: editnol.ForceZ,                    
                    MomentX: editnol.MomentX,
                    MomentY: editnol.MomentY,
                    MomentZ: editnol.MomentZ,
                }
                editlistnolvalid.push(nodeloadedited);
            }else{
                EditListNodesmessage = "Data not valid";
                res.json(EditListNodesmessage);
                return;
            }
        }           
                
        var listnol = {
            docs: editlistnolvalid
        };
        nodeloadsdb.bulk(listnol, function(er, data){
            if(er){
                throw er;
            }
            for(var i=0; i<listnol.docs.length;i++){
                console.log(listnol.docs[i].Name);
            }
            EditListNodeLoadsmessage = "Success";
            res.json(EditListNodeLoadsmessage);
        });
       
   }
});

router.post('/editnodeloads', function(req, res, next){
    if(!nodeloadsdb){
        res.json('No access to database');
        return;
    }

    let nol = req.body;
    let EditNodeLoadsmessage = '';

    let editnol = new nodeload(nol.Name, nol.NodeName, nol.ForceX, nol.ForceY, nol.ForceZ, nol.MomentX, nol.MomentY, nol.MomentZ);

    if(editnol.isvalid()){
        var nolnameindex = {name:'nolnameindex', type:'json', index:{fields:['Name']}};
         nodeloadsdb.index(nolnameindex,function(er, result){
            if (er) {
                EditNodeLoadsmessage = "Error in Index Query Creation";
                return;
            }
        });

        var id = req.query._id || req.query.id || req.body._id || req.body.id || "";

        if (id != "") {
            nodeloadsdb.get(id, function(err, data){
                if(err){
                    EditNodeLoadsmessage = "NodeLoad not founded in the database";
					return;
                }

                var old_doc = data;

                var query = { selector:{Name:editnol.Name}};
                nodeloadsdb.find(query,function(err, result){
                    if(result.docs.length==0){
                        nodeloadsdb.insert(nol, function(err, data){
                            if (err) {
                                EditNodeLoadsmessage = "Error in NodeLoad Insertion";
                                res.json(EditNodeLoadsmessage);
                                return;
                            }
                            EditNodeLoadsmessage = 'Success';
                            res.json(EditNodeLoadsmessage);
                        });
                    }else{
                        if(result.docs.length==1 && result.docs[0]._id == old_doc._id){
                            nodeloadsdb.insert(nol, function(err, data){
                                if (err) {
                                    EditNodeLoadsmessage = "Error in NodeLoad Insertion";
                                    res.json(EditNodeLoadsmessage);
                                    return;
                                }
                                EditNodeLoadsmessage = 'Success';
                                res.json(EditNodeLoadsmessage);
                            });
                        }else{
                            EditNodeLoadsmessage = 'Another nodeload with the same name already exists';
                            res.json(EditNodeLoadsmessage);
                        }
                    }
                });
            })
        }else{
            EditNodeLoadsmessage = 'This nodeload does not exist in the database';
            res.json(EditNodeLoadsmessage);
        }
    }else{
        EditNodeLoadsmessage = req.body;
        res.json(EditNodeLoadsmessage);
    }
});


router.post('/addnodeloads', function(req, res, next){
    if(!nodeloadsdb){
        res.json('No access to database');
        return;
    }

    var AddNodeLoadsmessage = '';
    
    nodesdb.list({ include_docs: true }, function(err, data) {
		if (err) {
			res.json({err:err});
			return;
		}else{
            var listnod = [];
            data.rows.forEach(function(row){
                if(row.doc.Name)
                    listnod.push(row.doc);
            });

            if(listnod.length === 0){
                AddNodeLoadsmessage = "No node exist";
                res.json(AddNodeLoadsmessage);
                return;
            }else{
                var newnol = new nodeload("NLID#" + Math.floor(Math.random()*10000), listnod[0]._id, 0, 0, 0, 0,0,0);
                
                if(newnol.isvalid()){
                    nodeloadsdb.insert(newnol, function(err, data){
                        if (err) {
                            AddNodeLoadsmessage = "Error in Node Load Insertion";
                            res.json(AddNodeLoadsmessage);
                            return;
                        }
                        AddNodeLoadsmessage = "Success";
                        res.json(AddNodeLoadsmessage);
                    });
                }
      
            }
        }
    });
});


router.post('/deletenodeloads', function(req, res, next){
    let deletenol = req.body;
    let DeleteNodeLoadmessage = "Ok";

    var id = deletenol.id || deletenol._id;
    var rev = deletenol.rev || deletenol._rev;

    var nolidindex = {name:'nolidindex', type:'json', index:{fields:['_id']}};
    nodeloadsdb.index(nolidindex,function(er, result){
        if (er) {
            DeleteNodeLoadmessage = "Error in Index Query Creation";
            return;
        }
    });

    if(id !='')
    {
        var query = { selector:{_id:deletenol._id}};
        nodeloadsdb.find(query,function(err, result){
            if(result.docs.length==0){
                DeleteNodeLoadmessage = 'Database Error: This nodeload does not exist';
                res.json(DeleteNodeLoadmessage);
            }else{
                nodeloadsdb.destroy(id, rev, function(err, data){
                    if (err) {
                        DeleteNodeLoadmessage = "NodeLoad not deleted";
                        res.json(DeleteNodeLoadmessage);
                        return;
                    }
                    DeleteNodeLoadmessage = "Success";
                    res.json(DeleteNodeLoadmessage);
                });
            }
        });
    }else{
        DeleteNodeLoadmessage = 'This node load does not exist';
        res.json(DeleteNodeLoadmessage);
    }
});


module.exports = router;
