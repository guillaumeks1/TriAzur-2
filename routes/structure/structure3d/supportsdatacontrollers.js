var express = require('express');
var router = express.Router();
var cloudant = require('./../../../config/db');
var support = require('./../../../models/structure/structure3d/support');
var bodyParser = require('body-parser');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

var supportsdb = require('./../../../config/supportsdb')

router.get('/listsupports', function(req, res, next){
    if(!supportsdb){        
        res.json('No access to database');
        return;
    }
    supportsdb.list({ include_docs: true }, function(err, data) {
		if (err) {
			res.json({err:err});
			return;
		}else{
            var listsup = [];

            data.rows.forEach(function(row){
                if(row.doc.Name){
                    listsup.push(row.doc);
                }
            });
            res.json(listsup);
        }
    });
});

router.post('/editsupports', function(req, res, next){
    if(!supportsdb){
        res.json('No access to database');
        return;
    }

    let sup = req.body;
    let EditSupportsmessage = '';

    let editsup = new support(sup.Name, sup.DispX, sup.DispY, sup.DispZ, sup.RotX, sup.RotY, sup.RotZ);

    if(editsup.isvalid()){
        if(editsup.Name==='Free' || editsup.Name==='Fixed'){
            EditSupportsmessage = 'This support can not be edited';
            res.json(EditSupportsmessage);
        }else{
            var supnameindex = {name:'supnameindex', type:'json', index:{fields:['Name']}};
            supportsdb.index(supnameindex,function(er, result){
                if (er) {
                    EditSupportsmessage = "Error in Index Query Creation";
                    return;
                }
            });

            var id = req.query._id || req.query.id || req.body._id || req.body.id || "";

            if (id != "") {
                supportsdb.get(id, function(err, data){
                    if(err){
                        EditSupportsmessage = "Support not founded in the database";
                        return;
                    }

                    var old_doc = data;

                    var query = { selector:{Name:editsup.Name}};
                    supportsdb.find(query,function(err, result){
                        if(result.docs.length==0){
                            supportsdb.insert(sup, function(err, data){
                                if (err) {
                                    EditSupportsmessage = "Error in Support Insertion";
                                    res.json(EditSupportsmessage);
                                    return;
                                }
                                EditSupportsmessage = "Success";
                                res.json(EditSupportsmessage);
                            });
                        }else{
                            if(result.docs.length==1 && result.docs[0]._id == old_doc._id){
                                supportsdb.insert(sup, function(err, data){
                                    if (err) {
                                        EditSupportsmessage = "Error in Support Insertion";
                                        res.json(EditSupportsmessage);
                                        return;
                                    }
                                    EditSupportsmessage = "Success";
                                    res.json(EditSupportsmessage);
                                });
                            }else{
                                EditSupportsmessage = 'Another support with the same name already exists';
                                res.json(EditSupportsmessage);
                            }
                        }
                    });
                })
            }else{
                EditSupportsmessage = 'This support does not exist in the database';
                res.json(EditSupportsmessage);
            }
        }
    }else{
        EditSupportsmessage = 'Inputs Not Valid';
        res.json(EditSupportsmessage);
    }
});

router.post('/addsupports', function(req, res, next){
    if(!supportsdb){
        res.json('No access to database');
        return;
    }

    let sup = req.body;
    let AddSupportsmessage = '';
    
    let newsup = new support(sup.Name, sup.DispX, sup.DispY, sup.DispZ, sup.RotX, sup.RotY, sup.RotZ);  

    if(newsup.isvalid()){
        //Look for a Support with the same name
        var supnameindex = {name:'supnameindex', type:'json', index:{fields:['Name']}};
        supportsdb.index(supnameindex,function(er, result){
            if (er) {
                AddSupportsmessage = "Error in Index Query Creation";
                return;
            }
        });

        var query = { selector:{Name:newsup.Name}};
        supportsdb.find(query,function(err, result){
            if(result.docs.length==0){
                supportsdb.insert(newsup, function(err, data){
                    if (err) {
                        AddSupportsmessage = "Error in Support Insertion";
                        res.json(AddSupportsmessage);
                        return;
                    }
                    AddSupportsmessage = "Success";
                    res.json(AddSupportsmessage);
                });
            }else{
                AddSupportsmessage = 'Support already exists';
                res.json(AddSupportsmessage);
            }
        });
    }else{
        AddSupportsmessage = "Input no valid";
        res.json(AddSupportsmessage);
    }
});

router.post('/deletesupports', function(req, res, next){
    let deletesup = req.body;
    let DeleteSupportmessage = "Ok";

    var supnameindex = {name:'supnameindex', type:'json', index:{fields:['Name']}};
    supportsdb.index(supnameindex,function(er, result){
        if (er) {
            DeleteSupportmessage = "Error in Index Query Creation";
            return;
        }
    });

    var id = deletesup.id || deletesup._id;
    var rev = deletesup.rev || deletesup._rev;
    
    if(deletesup.Name === 'Free' || deletesup.Name === 'Fixed'){
        DeleteSupportmessage = 'This support can not be deleted';
        res.json(DeleteSupportmessage);
    }else{
        if(id !='')
        {
            var query = { selector:{Name:deletesup.Name}};
            supportsdb.find(query,function(err, result){
                if(result.docs.length==0){
                    DeleteSupportmessage = 'Database Error: This support does not exist';
                    res.json(DeleteSupportmessage);
                }else{
                    supportsdb.destroy(id, rev, function(err, data){
                        if (err) {
                            DeleteSupportmessage = "Support not deleted";
                            res.json(DeleteSupportmessage);
                            return;
                        }
                        DeleteSupportmessage = "Success";
                        res.json(DeleteSupportmessage);
                    });
                }
            });
        }else{
            DeleteSupportmessage = 'This support does not exist';
            res.json(DeleteSupportmessage);
        }
    }

});


module.exports = router;
