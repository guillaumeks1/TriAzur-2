var express = require('express');
var router = express.Router();
var cloudant = require('./../../../config/db');
var node = require('./../../../models/structure/structure3d/node');
var bodyParser = require('body-parser');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

var supportsdb = require('./../../../config/supportsdb');
var nodesdb = require('./../../../config/nodesdb');


router.get('/listnodes', function(req, res, next){
    if(!nodesdb){        
        res.json('No access to database');
        return;
    }
    nodesdb.list({ include_docs: true }, function(err, data) {
		if (err) {
			res.json({err:err});
			return;
		}else{
            var listnod = [];
            data.rows.forEach(function(row){
                if(row.doc.Name)
                    listnod.push(row.doc);
            });
            res.json(listnod);
        }
    });
});


router.post('/editlistnodes', function(req, res, next){
    if(!nodesdb){
        res.json('No access to database');
        return;
    }

   var EditListNodesmessage = '';

   if(req.body.length===0){
        EditListNodesmessage = "No node exist";
        return;
   }else{

        var editlistnodvalid = [];

        for(var i=0; i<req.body.length;i++){
            var editnod = new node(req.body[i].Name, req.body[i].SupportId, req.body[i].X, req.body[i].Y, req.body[i].Z);
            
            if(i>0){
                for(var j=0; j<i; j++){
                    if(req.body[j].Name==req.body[i].Name){
                        EditListNodesmessage = "Node " + req.body[i].Name + " already exists";
                        res.json(EditListNodesmessage);
                        return;
                    }
                    if(req.body[j].X==req.body[i].X && req.body[j].Y==req.body[i].Y && req.body[j].Z==req.body[i].Z){
                        EditListNodesmessage = "Node " + req.body[j].Name + " has same coordinates than " +  req.body[i].Name;
                        res.json(EditListNodesmessage);
                        return;
                    }
                }
            }

            if(editnod.isvalid()){
                var id = req.body[i]._id || req.body[i].id || "";
                var rev = req.body[i]._rev || req.body[i].rev || "";

                var nodeedited = {
                    _id : id,
                    _rev : rev,
                    Name: editnod.Name,
                    SupportId: editnod.SupportId, 
                    X: editnod.X,
                    Y: editnod.Y,
                    Z: editnod.Z,
                }
                editlistnodvalid.push(nodeedited);
            }else{
                EditListNodesmessage = "Data not valid";
                res.json(EditListNodesmessage);
                return;
            }
        }           
                
        var listnod = {
            docs: editlistnodvalid
        };
        nodesdb.bulk(listnod, function(er, data){
            if(er){
                throw er;
            }
            for(var i=0; i<listnod.docs.length;i++){
                console.log(listnod.docs[i].Name);
            }
            EditListNodesmessage = "Success";
            res.json(EditListNodesmessage);
        });
       
   }
});



router.post('/editnodes', function(req, res, next){
    if(!nodesdb){
        res.json('No access to database');
        return;
    }

    var EditNodesmessage = '';

    var editnod = new node(req.body.Name, req.body.SupportId, req.body.X, req.body.Y, req.body.Z);

    if(editnod.isvalid()){
        var nodnameindex = {name:'nodnameindex', type:'json', index:{fields:['Name']}};
         nodesdb.index(nodnameindex,function(er, result){
            if (er) {
                EditNodesmessage = "Error in Index Query Creation";
                return;
            }
        });

        var id = req.query._id || req.query.id || req.body._id || req.body.id || "";
        var rev = req.query._rev || req.query.rev || req.body._rev || req.body.rev || "";

        if (id != "") {
            nodesdb.get(id, function(err, data){
                if(err){
                    EditNodesmessage = "Node not founded in the database";
					return;
                }
                var old_doc = data;

                var nodeedited = {
                    _id : id,
                    _rev : rev,
                    Name: editnod.Name,
                    SupportId: editnod.SupportId, 
                    X: editnod.X,
                    Y: editnod.Y,
                    Z: editnod.Z,
                }

                var query = { selector:{Name:editnod.Name}};
                nodesdb.find(query,function(err, result){
                    if(result.docs.length==0){
                        nodesdb.insert(nodeedited, function(err, data){
                            if (err) {
                                EditNodesmessage = "Error in Node Insertion";
                                res.json(EditNodesmessage);
                                return;
                            }
                            EditNodesmessage = "Success";
                            res.json(EditNodesmessage);
                        });
                    }else{
                        if(result.docs.length==1 && result.docs[0]._id == old_doc._id){
                            nodesdb.insert(nodeedited, function(err, data){
                                if (err) {
                                    EditNodesmessage = "Error in Node Insertion";
                                    res.json(EditNodesmessage);
                                    return;
                                }
                                EditNodesmessage = "Success";
                                res.json(EditNodesmessage);
                            });
                        }else{
                            EditNodesmessage = 'Another node with the same name already exists';
                            res.json(EditNodesmessage);
                        }
                    }
                });
            })
        }else{
            EditNodesmessage = 'This node does not exist in the database';
            res.json(EditNodesmessage);
        }
    }else{
        EditNodesmessage = req.body;
        res.json(EditNodesmessage);
    }
});


router.post('/addnodes', function(req, res, next){
    if(!nodesdb){
        res.json('No access to database');
        return;
    }

    var AddNodesmessage = '';

    var query = { selector:{Name:'Free'}};

    supportsdb.find(query,function(err, result){
        if(err){
            console.log('Error support query');
            return;
        }

        var newnod = new node("NodeID#" + Math.floor(Math.random()*10000), result.docs[0]._id, 0, 0, 0);

        if(newnod.isvalid()){
            var nodnameindex = {name:'nodnameindex', type:'json', index:{fields:['Name']}};
            nodesdb.index(nodnameindex,function(er, result){
                if (er) {
                    AddNodesmessage = "Error in Index Query Creation";
                    return;
                }
            });

            var query = { selector:{Name:newnod.Name}};
            nodesdb.find(query,function(err, result){
                if(result.docs.length==0){
                    nodesdb.insert(newnod, function(err, data){
                        if (err) {
                            AddNodesmessage = "Error in Node Insertion";
                            res.json(AddNodesmessage);
                            return;
                        }
                        AddNodesmessage = "Success";
                        res.json(AddNodesmessage);
                    });
                }else{
                    AddNodesmessage = 'Node already exists';
                    res.json(AddNodesmessage);
                }
            });
        }else{
            AddNodesmessage = newnod;
            res.json(AddNodesmessage);
        }
    });
});


router.post('/deletenodes', function(req, res, next){
    let deletenod = req.body;
    let DeleteNodemessage = "Ok";

    var id = deletenod.id || deletenod._id;
    var rev = deletenod.rev || deletenod._rev;

    var nodidindex = {name:'nodidindex', type:'json', index:{fields:['_id']}};
    nodesdb.index(nodidindex,function(er, result){
        if (er) {
            DeleteNodemessage = "Error in Index Query Creation";
            return;
        }
    });

    if(id !='')
    {
        var query = { selector:{_id:deletenod._id}};
        nodesdb.find(query,function(err, result){
            if(result.docs.length==0){
                DeleteNodemessage = 'Database Error: This node does not exist';
                res.json(DeleteNodemessage);
            }else{
                nodesdb.destroy(id, rev, function(err, data){
                    if (err) {
                        DeleteNodemessage = "Node not deleted";
                        res.json(DeleteNodemessage);
                        return;
                    }
                    DeleteNodemessage = "Success";
                    res.json(DeleteNodemessage);
                });
            }
        });
    }else{
        DeleteNodemessage = 'This node does not exist';
        res.json(DeleteNodemessage);
    }
});


module.exports = router;
