var express = require('express');
var router = express.Router();
var cloudant = require('./../../../config/db');
var section = require('./../../../models/structure/structure3d/section');
var bodyParser = require('body-parser');

var sectionanalysis = require('./../../../models/calculation/sectionanalysis');
var validation = require('./../../../models/validation/validation');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

var materialsdb = require('./../../../config/materialsdb');
var sectionsdb = require('./../../../config/sectionsdb');
var supportsdb = require('./../../../config/supportsdb');
var nodesdb = require('./../../../config/nodesdb');
var membersdb = require('./../../../config/membersdb');
var nodeloadsdb = require('./../../../config/nodeloadsdb');

router.get('/listsections', function(req, res, next){
    if(!sectionsdb){
        res.json('No access to database');
        return;
    }
    sectionsdb.list({ include_docs: true }, function(err, data) {
		if (err) {
			res.json({err:err});
			return;
		}else{
            var listsec = [];

            data.rows.forEach(function(row){
                if(row.doc.Name){
                    listsec.push(row.doc);                    
                }
            });
            res.json(listsec);
        }
    });
});


router.get('/datasections', function(req, res, next){
    if(!sectionsdb){
        res.json('No access to database');
        return;
    }

    if(validation.isArrayNaN(req.query.Xext) || 
        validation.isArrayNaN(req.query.Yext) ||
        validation.isArrayNaN(req.query.Xint) ||
        validation.isArrayNaN(req.query.Yint)){
        res.json({message:true});
    }else{
        var Xext = req.query.Xext.map(Number), 
        Yext = req.query.Yext.map(Number),
        Xint = req.query.Xint.map(Number),
        Yint = req.query.Yint.map(Number);

        var section = sectionanalysis(Xext, Yext, Xint, Yint);
        res.json({section, message:false});
    }
});


router.post('/editsections', function(req, res, next){
    if(!sectionsdb){
        res.json('No access to database');
        return;
    }

    var EditSectionsmessage = '';

    var editsec = new section(req.body.Name, req.body.MaterialId, req.body.Area,req.body.I2, 
    req.body.I3, req.body.J, req.body.XG, req.body.YG, req.body.Xext, req.body.Yext, req.body.Xint, req.body.Yint);

    if(editsec.isvalid()){
        if(editsec.Name === 'Pile'){
            EditSectionsmessage = 'Pile Section';
            res.json(EditSectionsmessage);
        }else{
            var secnameindex = {name:'secnameindex', type:'json', index:{fields:['Name']}};
            sectionsdb.index(secnameindex,function(er, result){
                if (er) {
                    EditSectionsmessage = "Error in Index Query Creation";
                    return;
                }
            });

            var id = req.query._id || req.query.id || req.body._id || req.body.id || "";
            var rev = req.query._rev || req.query.rev || req.body._rev || req.body.rev || "";

            if (id != "") {
                sectionsdb.get(id, function(err, data){
                    if(err){
                        EditSectionsmessage = "Section not founded in the database";
                        return;
                    }
                    var old_doc = data;

                    var sectionedited = {
                        _id : id,
                        _rev : rev,
                        Name: editsec.Name,
                        MaterialId: editsec.MaterialId, 
                        Area: editsec.Area,
                        I2: editsec.I2,
                        I3: editsec.I3,
                        J: editsec.J,
                        XG: editsec.XG,
                        YG: editsec.YG,
                        Xext: editsec.Xext,
                        Yext: editsec.Yext,
                        Xint: editsec.Xint,
                        Yint: editsec.Yint
                    }

                    var query = { selector:{Name:editsec.Name}};
                    sectionsdb.find(query,function(err, result){
                        if(result.docs.length==0){
                            sectionsdb.insert(sectionedited, function(err, data){
                                if (err) {
                                    EditSectionsmessage = "Error in Section Insertion";
                                    res.json(EditSectionsmessage);
                                    return;
                                }
                                EditSectionsmessage = 'Success';
                                res.json(EditSectionsmessage);
                            });
                        }else{
                            if(result.docs.length==1 && result.docs[0]._id == old_doc._id){
                                sectionsdb.insert(sectionedited, function(err, data){
                                    if (err) {
                                        EditSectionsmessage = "Error in Section Insertion";
                                        res.json(EditSectionsmessage);
                                        return;
                                    }
                                    EditSectionsmessage = 'Success';
                                    res.json(EditSectionsmessage);
                                });
                            }else{
                                EditSectionsmessage = 'Another section with the same name already exists';
                                res.json(EditSectionsmessage);
                            }
                        }
                    });
                })
            }else{
                EditSectionsmessage = 'This section does not exist in the database';
                res.json(EditSectionsmessage);
            }
        }
    }else{
        EditSectionsmessage = "Input not valid";
        res.json(EditSectionsmessage);
    }
});

router.post('/addsections', function(req, res, next){
    if(!sectionsdb){
        res.json('No access to database');
        return;
    }

    var sec = req.body;
    var AddSectionsmessage = '';

    var newsec = new section(req.body.Name, req.body.MaterialId, req.body.Area,req.body.I2, req.body.I3, req.body.J, 
    req.body.XG, req.body.YG, req.body.Xext, req.body.Yext, req.body.Xint, req.body.Yint);  

    if(newsec.isvalid()){
        var secnameindex = {name:'secnameindex', type:'json', index:{fields:['Name']}};
        sectionsdb.index(secnameindex,function(er, result){
            if (er) {
                AddSectionsmessage = "Error in Index Query Creation";
                return;
            }
        });

        var query = { selector:{Name:newsec.Name}};
        sectionsdb.find(query,function(err, result){
            if(result.docs.length==0){
                sectionsdb.insert(newsec, function(err, data){
                    if (err) {
                        AddSectionsmessage = "Error in Section Insertion";
                        res.json(AddSectionsmessage);
                        return;
                    }
                    AddSectionsmessage = "Success";
                    res.json(AddSectionsmessage);
                });
            }else{
                AddSectionsmessage = 'Section already exists';
                res.json(AddSectionsmessage);
            }
        });
    }else{
        AddSectionsmessage = "Input not valid";
        res.json(AddSectionsmessage);
    }
});

router.post('/deletesections', function(req, res, next){
    let deletesec = req.body;
    let DeleteSectionmessage = "Ok";

    var secidindex = {name:'secidindex', type:'json', index:{fields:['_id']}};
    sectionsdb.index(secidindex,function(er, result){
        if (er) {
            DeleteSectionmessage = "Error in Index Query Creation";
            return;
        }
    });

    var id = deletesec.id || deletesec._id;
    var rev = deletesec.rev || deletesec._rev;

    if(deletesec.Name === 'Pile'){
        DeleteSectionmessage = 'Pile section can not be deleted';
        res.json(DeleteSectionmessage);
    }else{
    if(id !='')
        {
            var query = { selector:{_id:deletesec._id}};
            sectionsdb.find(query,function(err, result){
                if(result.docs.length==0){
                    DeleteSectionmessage = 'Database Error: This section does not exist';
                    res.json(DeleteSectionmessage);
                }else{
                    sectionsdb.destroy(id, rev, function(err, data){
                        if (err) {
                            DeleteSectionmessage = "Section not deleted";
                            res.json(DeleteSectionmessage);
                            return;
                        }
                        DeleteSectionmessage = "Success";
                        res.json(DeleteSectionmessage);
                    });
                }
            });
        }else{
            DeleteSectionmessage = 'This section does not exist';
            res.json(DeleteSectionmessage);
        }
    }
});


module.exports = router;
